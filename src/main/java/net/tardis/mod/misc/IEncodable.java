package net.tardis.mod.misc;

import net.minecraft.network.PacketBuffer;

public interface IEncodable {

    void encode(PacketBuffer buf);
    void decode(PacketBuffer buf);
}
