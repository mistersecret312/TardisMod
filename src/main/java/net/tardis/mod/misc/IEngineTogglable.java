package net.tardis.mod.misc;

public interface IEngineTogglable {
    void setActivated(boolean active);
    boolean isActivated();
}
