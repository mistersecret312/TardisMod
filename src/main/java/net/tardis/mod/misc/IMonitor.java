package net.tardis.mod.misc;

import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorView;

public interface IMonitor {

    MonitorView getView();

    void setView(MonitorView view);
}
