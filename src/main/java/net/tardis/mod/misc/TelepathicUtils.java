package net.tardis.mod.misc;

import net.minecraft.util.text.ITextComponent;

public class TelepathicUtils {
	
	public enum SearchType {
        BIOME,
        STRUCTURE,
        SPAWN;
        
        public static SearchType[] values = values(); //Yes this is dumb but Eclipse screamed about values() not existing...
    }

    public static class Search {

        public String key;
        public ITextComponent trans;
        public SearchType type;

        public Search(ITextComponent trans, String key, SearchType type) {
            this.key = key;
            this.trans = trans;
            this.type = type;
        }
    }

}
