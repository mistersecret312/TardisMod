package net.tardis.mod.misc;

public class CrashType {

    private float componentDamage;
    private int inaccuracy;
    private boolean makeExteriorEffects;

    public CrashType(int inaccuracy, float damage, boolean exteriorEffects){
        this.inaccuracy = inaccuracy;
        this.componentDamage = damage;
        this.makeExteriorEffects = exteriorEffects;
    }

    public float getComponentDamage(){
        return this.componentDamage;
    }

    public int getInaccuracy() {
        return this.inaccuracy;
    }

    public boolean shouldDoExteriorEffects(){
        return this.makeExteriorEffects;
    }
}
