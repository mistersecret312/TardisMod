package net.tardis.mod.entity.mission;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.World;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.entity.humanoid.AbstractHumanoidEntity;
import net.tardis.mod.entity.humanoid.HumanoidEmotionalState;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.ObjectWrapper;
import net.tardis.mod.missions.MiniMission;
import net.tardis.mod.missions.misc.Dialog;
/** Generic humanoid entity that is involved in a Mission. To make this entity a host of the mission*/
public class HumanoidMissionEntity extends AbstractHumanoidEntity{
	
	public ITextComponent cachedDisplayName;
	
    public static final DataParameter<Integer> VARIANT = EntityDataManager.createKey(HumanoidMissionEntity.class, DataSerializers.VARINT);
    public static final DataParameter<Integer> EMOTIONAL_STATE = EntityDataManager.createKey(HumanoidMissionEntity.class, DataSerializers.VARINT);

	public static final ResourceLocation[] HUMAN_TEXTURES = {
			Helper.createRL("textures/entity/crew/1.png"),
			Helper.createRL("textures/entity/crew/2.png"),
			Helper.createRL("textures/entity/crew/3.png"),
			Helper.createRL("textures/entity/crew/4.png"),
			Helper.createRL("textures/entity/crew/5.png"),
			Helper.createRL("textures/entity/crew/6.png"),
			Helper.createRL("textures/entity/crew/7.png")
	};

	public HumanoidMissionEntity(EntityType<? extends CreatureEntity> type, World worldIn) {
		super(type, worldIn);
	}
	
	@Override
	public Dialog getCurrentDialog(PlayerEntity player) {
		return this.setupDialog(player);
	}
	/** Sets up the dialog based on the mission and the mission's objective*/
	public Dialog setupDialog(PlayerEntity player) {
		ObjectWrapper<Dialog> dialog = new ObjectWrapper<>(null);
		world.getCapability(Capabilities.MISSION).ifPresent(miss -> {
			MiniMission mission = miss.getMissionForPos(getPosition());
			if(mission != null)
				dialog.setValue(mission.getDialogForObjective(this, player, mission.getCurrentObjective()));
		});
		return dialog.getValue();
	}

	@Override
	public ResourceLocation getSkin() {
		return HUMAN_TEXTURES[this.getDataManager().get(VARIANT)];
	}
	
	@Override
	protected void registerData() {
		super.registerData();
		this.getDataManager().register(VARIANT, 0);
		this.getDataManager().register(EMOTIONAL_STATE, 0);
	}
	
	@Override
	public ILivingEntityData onInitialSpawn(IServerWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, ILivingEntityData spawnDataIn, CompoundNBT dataTag) {
		if(!worldIn.isRemote())
			this.getDataManager().set(VARIANT, this.rand.nextInt(HUMAN_TEXTURES.length));
		return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}
	
	public static AttributeModifierMap.MutableAttribute createAttributes() {
    	return CreatureEntity.func_233666_p_().createMutableAttribute(Attributes.MAX_HEALTH, 20D)
    			.createMutableAttribute(Attributes.ARMOR_TOUGHNESS, 10D);
    }
	
	@Override
    public void writeAdditional(CompoundNBT compound) {
		super.writeAdditional(compound);
		compound.putInt("texture", this.getDataManager().get(VARIANT));
		compound.putInt("emotional_state", this.getDataManager().get(EMOTIONAL_STATE));
	}

	@Override
	public void readAdditional(CompoundNBT compound) {
		super.readAdditional(compound);
		this.getDataManager().set(VARIANT, compound.getInt("texture"));
		this.getDataManager().set(EMOTIONAL_STATE, compound.getInt("emotional_state"));
	}
	
	public HumanoidEmotionalState getEmotionalState() {
	    return HumanoidEmotionalState.values()[this.getDataManager().get(EMOTIONAL_STATE)];
	}
	
	public void setEmotionalState(HumanoidEmotionalState state) {
	    this.getDataManager().set(EMOTIONAL_STATE, state.ordinal());
	}

}
