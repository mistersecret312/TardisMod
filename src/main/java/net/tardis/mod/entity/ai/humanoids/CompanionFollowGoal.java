package net.tardis.mod.entity.ai.humanoids;

import java.util.EnumSet;

import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.player.PlayerEntity;
import net.tardis.mod.entity.humanoid.CompanionEntity;

public class CompanionFollowGoal extends Goal{

//	private static double MAX_DIST = 32 * 32;
	
	private PlayerEntity owner;
	private CompanionEntity companion;
	private double speed;
	private int recalculatePath;
	
	
	public CompanionFollowGoal(CompanionEntity companion, double speed) {
		this.companion = companion;
		this.speed = speed;
		this.setMutexFlags(EnumSet.of(Flag.JUMP, Flag.MOVE));
	}
	
	@Override
	public boolean shouldExecute() {
		if(this.companion.getOwnerEntity() == null) 
			return false;
		else this.owner = this.companion.getOwnerEntity();
		
		if(this.companion.isSitting())
			return false;
		return true;
	}

	@Override
	public void startExecuting() {
		super.startExecuting();
	}

	@Override
	public boolean shouldContinueExecuting() {
		return super.shouldContinueExecuting() && !this.companion.isSitting() && !this.companion.getNavigator().noPath();
	}

	@Override
	public void resetTask() {
		super.resetTask();
	}

	@Override
	public void tick() {
		super.tick();
		
		++this.recalculatePath;
		if(this.recalculatePath % 20 == 0)
			this.companion.getNavigator().tryMoveToEntityLiving(this.owner, speed);
		
	}

}
