package net.tardis.mod.contexts.gui;

import net.tardis.mod.controls.AbstractControl;
import net.tardis.mod.misc.GuiContext;

public class ControlContext extends GuiContext {

	public AbstractControl control;
	
	public ControlContext(AbstractControl control) {
		this.control = control;
	}
}
