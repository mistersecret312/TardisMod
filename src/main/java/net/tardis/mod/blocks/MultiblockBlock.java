package net.tardis.mod.blocks;

import org.apache.logging.log4j.Level;

import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.template.NotSolidTileBlock;
import net.tardis.mod.tileentities.IMultiblock;
import net.tardis.mod.tileentities.MultiblockMasterTile;

/**
 * See {@link MultiblockMasterTile} for use
 *
 * @author spectre
 */

public class MultiblockBlock extends NotSolidTileBlock {

    public MultiblockBlock(Properties prop) {
        super(prop);
        this.setDefaultState(this.getDefaultState().with(BlockStateProperties.WATERLOGGED, false));
    }

    @Override
    public int getOpacity(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return 0;
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        return BlockRenderType.INVISIBLE;
    }

    @Override
    public float getPlayerRelativeBlockHardness(BlockState state, PlayerEntity player, IBlockReader worldIn, BlockPos pos) {
        if (state != this.getMasterState(worldIn, pos))
            return this.getMasterState(worldIn, pos).getPlayerRelativeBlockHardness(player, worldIn, pos);
        return super.getPlayerRelativeBlockHardness(state, player, worldIn, pos);
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (this.getMasterState(worldIn, pos) != state) {
        	if (this.getMasterState(worldIn, pos).hasTileEntity()) {
        		//Handle cases where the master block has a Tile Entity.
        		//Often the master block will use its tile entity for things like container guis, so we must use the master block pos to ensure that the correct tile entity type is used
        		//E.g. Right clicking a multiblock whose master block has a container gui, means we will open the container at the master position.
        		if (worldIn.getTileEntity(pos) instanceof IMultiblock) { 
        			IMultiblock multiblock = (IMultiblock)worldIn.getTileEntity(pos);
                	BlockPos masterPos = multiblock.getMaster();
                	//Constract a new block trace result so the block pos is correct
                	BlockRayTraceResult masterResultHit = new BlockRayTraceResult(hit.getHitVec(), hit.getFace(), masterPos, false);
                    return this.getMasterState(worldIn, pos).onBlockActivated(worldIn, player, handIn, masterResultHit);
        		}
        	}
        	else {
                return this.getMasterState(worldIn, pos).onBlockActivated(worldIn, player, handIn, hit);
        	}
        }
        return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
    }

    @Override
    public boolean canSpawnInBlock() {
        return false;
    }

    @Override
    public float getSlipperiness(BlockState state, IWorldReader world, BlockPos pos, Entity entity) {
        return this.getMasterState(world, pos) == state ? super.getSlipperiness(state, world, pos, entity) : this.getMasterState(world, pos).getSlipperiness(world, pos, entity);
    }

    public BlockState getMasterState(IBlockReader world, BlockPos pos) {
        TileEntity te = world.getTileEntity(pos);
        if (te instanceof IMultiblock)
            return ((IMultiblock) te).getMasterState();
        return world.getBlockState(pos);
    }

    @Override
    public void onBlockHarvested(World worldIn, BlockPos pos, BlockState state, PlayerEntity player) {
        super.onBlockHarvested(worldIn, pos, state, player);
        if (!worldIn.isRemote() && !player.abilities.isCreativeMode) {
            BlockState masterState = getMasterState(worldIn, pos);
            if (state != masterState)
                Block.spawnDrops(masterState, worldIn, pos);
        }
    }

    @Override
    public ItemStack getPickBlock(BlockState state, RayTraceResult target, IBlockReader world, BlockPos pos,
                                  PlayerEntity player) {
        BlockState masterState = getMasterState(world, pos);
        ItemStack masterBlockStack = new ItemStack(masterState.getBlock().asItem());
        return masterBlockStack;
    }

    @Override
    public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
        try {
            if (newState.getBlock() != state.getBlock()) {
                if (worldIn.getTileEntity(pos) instanceof MultiblockMasterTile) {
                    for (BlockPos other : ((MultiblockMasterTile) worldIn.getTileEntity(pos)).getSlavePositions()) {
                        worldIn.setBlockState(other, Blocks.AIR.getDefaultState());
                    }
                } else {
                    worldIn.setBlockState(((IMultiblock) worldIn.getTileEntity(pos)).getMaster(), Blocks.AIR.getDefaultState());
                }
            }
        } catch (ClassCastException e) {
            Tardis.LOGGER.catching(Level.DEBUG, e);
        }
        super.onReplaced(state, worldIn, pos, newState, isMoving);
    }
    
    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        FluidState fluid = context.getWorld().getFluidState(context.getPos());
        return this.getDefaultState().with(BlockStateProperties.WATERLOGGED, fluid.getFluidState().isTagged(FluidTags.WATER));
    }

    @Override
    protected void fillStateContainer(Builder<Block, BlockState> builder) {
        builder.add(BlockStateProperties.WATERLOGGED);
    }

    @Override
    public FluidState getFluidState(BlockState state) {
        return state.get(BlockStateProperties.WATERLOGGED) ? Fluids.WATER.getStillFluidState(false) : Fluids.EMPTY.getDefaultState();
    }

    @Override
    public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos) {
        BlockState state = stateIn;
        if (state.get(BlockStateProperties.WATERLOGGED)) {
            worldIn.getPendingFluidTicks().scheduleTick(currentPos, Fluids.WATER, Fluids.WATER.getTickRate(worldIn));
        }
        return state;
    }

}
