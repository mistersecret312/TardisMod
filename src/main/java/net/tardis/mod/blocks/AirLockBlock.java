package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.DoorBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.pathfinding.PathType;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class AirLockBlock extends Block{

    public static BooleanProperty TOP = BooleanProperty.create("top");
    public static BooleanProperty OPEN = BooleanProperty.create("open");

    public static VoxelShape TOP_SHAPE = VoxelShapes.empty(); //Block.makeCuboidShape(0, 0, 0, 16, 1, 16);
    public static VoxelShape BOTTOM_SHAPE = Block.makeCuboidShape(0, 15, 0, 16, 16, 16);

    public AirLockBlock(Properties properties) {
        super(properties);
        this.setDefaultState(this.getDefaultState().with(OPEN, false).with(TOP, false));
    }

    @Override
    protected void fillStateContainer(Builder<Block, BlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(TOP, OPEN);
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        if (!state.get(OPEN))
            return VoxelShapes.fullCube();

        if (state.get(TOP))
            return BOTTOM_SHAPE;

        return TOP_SHAPE;
    }

    @Override
    public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
        

        if (!worldIn.isRemote  && state.getBlock() != newState.getBlock()) {
           BlockPos other = null;
           if (state.get(TOP))
               other = pos.down();
           else other = pos.up();
           worldIn.setBlockState(other, Blocks.AIR.getDefaultState());
        }
        super.onReplaced(state, worldIn, pos, newState, isMoving);

    }

    @Override
    public void neighborChanged(BlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) {


        boolean flag = worldIn.isBlockPowered(pos) || worldIn.isBlockPowered(pos.offset(state.get(TOP) == false ? Direction.UP : Direction.DOWN));

        if (blockIn != this && flag != state.get(OPEN)) {
        	worldIn.setBlockState(pos, state.with(TOP, state.get(TOP)).with(OPEN, Boolean.valueOf(flag)), 2);
        	if (worldIn.getBlockState(pos.up()).hasProperty(TOP))
        	   worldIn.setBlockState(pos.up(), state.with(TOP, true).with(OPEN, Boolean.valueOf(flag)), 2);
        	if (worldIn.getBlockState(pos.down()).hasProperty(TOP)) {
        		worldIn.setBlockState(pos.down(), state.with(TOP, false).with(OPEN, Boolean.valueOf(flag)), 2);
        	}
        	worldIn.playSound(null, pos, state.get(OPEN) ? SoundEvents.BLOCK_PISTON_EXTEND : SoundEvents.BLOCK_PISTON_CONTRACT, SoundCategory.BLOCKS, 0.7F, 1F);
        }
    }
    
    @Override
    public boolean isValidPosition(BlockState state, IWorldReader worldIn, BlockPos pos) {
    	 BlockPos blockpos = pos.down();
         BlockState blockstate = worldIn.getBlockState(blockpos);
         return state.get(TOP) == false ? blockstate.isSolidSide(worldIn, blockpos, Direction.UP) : blockstate.matchesBlock(this);
    }

    @Override
    public boolean allowsMovement(BlockState state, IBlockReader worldIn, BlockPos pos, PathType type) {
        switch(type) {
        case LAND:
           return state.get(OPEN);
        case WATER:
           return false;
        case AIR:
           return state.get(OPEN);
        default:
           return false;
        }
    }

    public void setDoorState(World world, BlockPos pos, boolean open) {
        BlockState state = world.getBlockState(pos);

        world.setBlockState(pos, state.with(OPEN, open));
        if (state.get(TOP))
            world.setBlockState(pos.down(), world.getBlockState(pos.down()).with(OPEN, open), 2);
        else
        	world.setBlockState(pos.up(), world.getBlockState(pos.up()).with(OPEN, open), 2);
        world.playSound(null, pos, state.get(OPEN) ? SoundEvents.BLOCK_PISTON_EXTEND : SoundEvents.BLOCK_PISTON_CONTRACT, SoundCategory.BLOCKS, 0.7F, 1F);
    }

    public static class AirLockBlockItem extends BlockItem {

        public AirLockBlockItem(Block blockIn, Properties builder) {
            super(blockIn, builder);
        }

        @Override
        public ActionResultType tryPlace(BlockItemUseContext context) {
            return context.getWorld().getBlockState(context.getPos().up()).isReplaceable(context) ? super.tryPlace(context) : ActionResultType.FAIL;
        }

        @Override
        protected boolean onBlockPlaced(BlockPos pos, World worldIn, PlayerEntity player, ItemStack stack, BlockState state) {
            worldIn.setBlockState(pos.up(), state.with(TOP, true));
            return super.onBlockPlaced(pos, worldIn, player, stack, state);
        }

    }

}
