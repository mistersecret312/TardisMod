package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;
import net.tardis.mod.ars.IARS;
import net.tardis.mod.helper.VoxelShapeUtils;
import net.tardis.mod.properties.Prop;

public class TechStrutBlock extends Block implements IARS {

    public static final IntegerProperty TYPE = IntegerProperty.create("type", 0, 2);
    public static final VoxelShape NORTH = createVoxelShape();
    public static final VoxelShape EAST = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.CLOCKWISE_90);
    public static final VoxelShape SOUTH = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.CLOCKWISE_180);
    public static final VoxelShape WEST = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.COUNTERCLOCKWISE_90);

    public TechStrutBlock() {
        super(Prop.Blocks.BASIC_TECH.get().notSolid());
    }

    public static VoxelShape createVoxelShape() {
    	VoxelShape shape = VoxelShapes.create(0.4375, 0.0, 0.85625, 0.5625, 1.0, 1.0);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.359375, 0.0, 0.575, 0.640625, 1.0, 0.85625), IBooleanFunction.OR);
        return shape;
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.calcState(context.getWorld(), context.getPos()).with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing().getOpposite());
    }

    @Override
	public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld worldIn, BlockPos pos, BlockPos facingPos) {
		if (!worldIn.isRemote()) {
			
			BlockPos upPos = pos.offset(Direction.UP);
			BlockPos downPos = pos.offset(Direction.DOWN);
			
            //up
            int type = this.calcState(worldIn, upPos).get(TYPE);
            if (worldIn.getBlockState(upPos).hasProperty(TYPE) && worldIn.getBlockState(upPos).get(TYPE) != type) {
                worldIn.setBlockState(upPos, stateIn.with(TYPE, type), 3);
            }

            //Down
            type = this.calcState(worldIn, downPos).get(TYPE);
            if (worldIn.getBlockState(downPos).hasProperty(TYPE) && worldIn.getBlockState(downPos).get(TYPE) != type) {
                worldIn.setBlockState(downPos, stateIn.with(TYPE, type), 3);
            }
            
            //Middle
            type = this.calcState(worldIn, pos).get(TYPE);
            if (worldIn.getBlockState(pos).hasProperty(TYPE) && worldIn.getBlockState(pos).get(TYPE) != type) {
                worldIn.setBlockState(pos, stateIn.with(TYPE, type), 3);
            }
        }
		return super.updatePostPlacement(stateIn, facing, facingState, worldIn, pos, facingPos);
	}

	@Override
    public FluidState getFluidState(BlockState state) {
        return Fluids.EMPTY.getDefaultState();
    }

    @Override
    protected void fillStateContainer(Builder<Block, BlockState> builder) {
        builder.add(BlockStateProperties.HORIZONTAL_FACING);
        builder.add(TYPE);
    }

    public BlockState calcState(IWorldReader world, BlockPos pos) {
        if (world.getBlockState(pos.down()).getBlock() != TBlocks.tech_strut.get() && world.getBlockState(pos.up()).getMaterial() == Material.AIR) {
            return this.getDefaultState().with(TYPE, 0);
        }
        if (world.getBlockState(pos.up()).getBlock() != TBlocks.tech_strut.get()) {
        	if (world.getBlockState(pos.down()).getBlock() == TBlocks.tech_strut.get() || world.getBlockState(pos.down()).getMaterial() == Material.AIR)
                return this.getDefaultState().with(TYPE, 2);
        }
        if (world.getBlockState(pos.up()).getBlock() == TBlocks.tech_strut.get() && world.getBlockState(pos.down()).getBlock() == TBlocks.tech_strut.get())
        	return this.getDefaultState().with(TYPE, 1);

        return this.getDefaultState().with(TYPE, 0);
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        switch (state.get(BlockStateProperties.HORIZONTAL_FACING)) {
            case EAST:
                return EAST;
            case SOUTH:
                return SOUTH;
            case WEST:
                return WEST;
            default:
                return NORTH;
        }
    }

    @Override
    public BlockState rotate(BlockState state, Rotation rot) {
        return state.with(BlockStateProperties.HORIZONTAL_FACING, rot.rotate(state.get(BlockStateProperties.HORIZONTAL_FACING)));
    }

    /**
     * Returns the blockstate with the given mirror of the passed blockstate. If inapplicable, returns the passed
     * blockstate.
     *
     * @deprecated call via {@link IBlockState#withMirror(Mirror)} whenever possible. Implementing/overriding is fine.
     */
    @Override
    public BlockState mirror(BlockState state, Mirror mirrorIn) {
        return state.rotate(mirrorIn.toRotation(state.get(BlockStateProperties.HORIZONTAL_FACING)));
    }


}
