package net.tardis.mod.registries;

import java.util.function.Supplier;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.animation.ClassicExteriorAnimation;
import net.tardis.mod.client.animation.ExteriorAnimationEntry;
import net.tardis.mod.client.animation.NewWhoExteriorAnimation;

public class ExteriorAnimationRegistry {

	public static final DeferredRegister<ExteriorAnimationEntry> EXTERIOR_ANIMATIONS = DeferredRegister.create(ExteriorAnimationEntry.class, Tardis.MODID);
    
    public static Supplier<IForgeRegistry<ExteriorAnimationEntry>> EXTERIOR_ANIMATION_REGISTRY = EXTERIOR_ANIMATIONS.makeRegistry("exterior_animation", () -> new RegistryBuilder<ExteriorAnimationEntry>().setMaxID(Integer.MAX_VALUE - 1));
	
	
	public static final RegistryObject<ExteriorAnimationEntry> CLASSIC = EXTERIOR_ANIMATIONS.register("classic", () -> new ExteriorAnimationEntry(ClassicExteriorAnimation::new));
	public static final RegistryObject<ExteriorAnimationEntry> NEW_WHO = EXTERIOR_ANIMATIONS.register("new_who", () -> new ExteriorAnimationEntry(NewWhoExteriorAnimation::new));

}
