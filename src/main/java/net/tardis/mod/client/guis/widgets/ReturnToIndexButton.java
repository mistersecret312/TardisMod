package net.tardis.mod.client.guis.widgets;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.tardis.mod.client.guis.manual.ManualScreen;

public class ReturnToIndexButton extends ChangeChapterButton{

	public ReturnToIndexButton(int x, int y, boolean isForward, IPressable onPress, boolean playTurnSound) {
		super(x, y, 12, 16, isForward, onPress, playTurnSound);
	}

	@Override
	public void renderWidget(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        Minecraft.getInstance().getTextureManager().bindTexture(ManualScreen.TEXTURE);
	    int u = 52;
	    int v = 200;
	    if (this.isHovered()) {
	       u += 22;
	    }

	    if (!this.isForward) {
	       v += 19;
	    }
	    this.blit(matrixStack, this.x, this.y, u, v, this.width, this.height);
	}

}
