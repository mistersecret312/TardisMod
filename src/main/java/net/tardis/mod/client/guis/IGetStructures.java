package net.tardis.mod.client.guis;

import java.util.List;

import net.tardis.mod.misc.TelepathicUtils.Search;
/** Interface to allow a client side {@linkplain Screen} to receive Configured Structure Names
 * <br> Used for Telepathic Screen*/
public interface IGetStructures {

	abstract void setTelepathicStructureNamesFromServer(List<Search> searches);
}
