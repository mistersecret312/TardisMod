package net.tardis.mod.client.guis.vm;

import java.util.ArrayList;
import java.util.List;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.button.LockIconButton;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.client.guis.INeedWorlds;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.VMTeleportMessage;

public class VortexMTeleportGui extends VortexMFunctionScreen implements INeedWorlds{

    private final TranslationTextComponent title = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "teleport.title");
    private final TranslationTextComponent tp_type = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "teleport.tp_type");
    private final TranslationTextComponent warning = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "teleport.precise_warning");
    private final TranslationTextComponent TP_TOP = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "teleport.setting.top");
    private final TranslationTextComponent TP_PRECISE = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "teleport.setting.precise");
    private final TranslationTextComponent TP_BUTTON = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "teleport.tp");
    private TextFieldWidget xCoord;
    private TextFieldWidget yCoord;
    private TextFieldWidget zCoord;
    private Button selectedDim;
    private LockIconButton dimensionToggle;
    private Button teleport;
    private Button back;
    private Button teleportSetting;
    private boolean preciseTeleport = false;
    private boolean useCurrentDim = true;
    private int settingId = 0;
    private List<RegistryKey<World>> worldKeys = new ArrayList<>();
    private RegistryKey<World> selectedWorld;
    private int index = 0;


    public VortexMTeleportGui(ITextComponent title) {
        super(title);
    }

    public VortexMTeleportGui() {
    }


    @Override
    public void init() {
        super.init();
        this.selectedWorld = this.minecraft	.player.world.getDimensionKey();
        final int btnH = 20;

        xCoord = new TextFieldWidget(this.font, this.getMinX() + 35, this.getMaxY() + 40, 50, this.font.FONT_HEIGHT + 2, new TranslationTextComponent(""));
        yCoord = new TextFieldWidget(this.font, this.getMinX() + 105, this.getMaxY() + 40, 50, this.font.FONT_HEIGHT + 2, new TranslationTextComponent(""));
        zCoord = new TextFieldWidget(this.font, this.getMinX() + 175, this.getMaxY() + 40, 50, this.font.FONT_HEIGHT + 2, new TranslationTextComponent(""));
        teleport = new Button(this.getMinX() + 95, this.getMaxY() + 125, this.font.getStringWidth(TP_BUTTON.getString()) + 10, btnH, TP_BUTTON, new Button.IPressable() {
            @Override
            public void onPress(Button button) {
                checkPosInput();
            }
        });
        back = new TextButton(this.getMaxX() + 5, this.getMaxY() + 5, TardisConstants.Translations.GUI_BACK, new Button.IPressable() {

            @Override
            public void onPress(Button button) {
                ClientHelper.openGUI(TardisConstants.Gui.VORTEX_MAIN, null);
            }
        });
        teleportSetting = new Button(this.getMinX() + 20, this.getMaxY() + 85, 55, this.font.FONT_HEIGHT + 11, TP_TOP, new Button.IPressable() {

            @Override
            public void onPress(Button button) {
                incrementId();
                switch (settingId) {
                    case 0:
                        teleportSetting.setMessage(TP_TOP);
                        setTeleportType(!preciseTeleport);
                        break;
                    case 1:
                        teleportSetting.setMessage(TP_PRECISE);
                        setTeleportType(!preciseTeleport); //reset flag
                        break;
                }

            }
        });
        this.dimensionToggle = new LockIconButton(this.getMinX() + 95, this.getMaxY() + 85, new Button.IPressable() {
            @Override
            public void onPress(Button button) {
            	boolean currentSetting = useCurrentDim;
                useCurrentDim = !currentSetting;
                dimensionToggle.setLocked(useCurrentDim);
                if (!useCurrentDim) {
                    selectedDim.active = true;
                }
                else {
                	selectedDim.active = false;
                	selectedWorld = minecraft.player.world.getDimensionKey();
                	selectedDim.setMessage(new StringTextComponent(selectedWorld.getLocation().toString()));
                }
            }
        });
        this.selectedDim = new Button(this.getMinX() + 120, this.getMaxY() + 85, 120, this.font.FONT_HEIGHT + 11, new StringTextComponent(""), new Button.IPressable() {
            @Override
            public void onPress(Button button) {
                modIndex(1);
            }
        });
        
        this.buttons.clear();
        this.addButton(teleport);
        this.addButton(xCoord); //addButton also adds other widget types, mapping name can be misleading
        this.addButton(yCoord);
        this.addButton(zCoord);
        this.addButton(back);
        this.addButton(teleportSetting);
        this.addButton(this.dimensionToggle);
        this.addButton(this.selectedDim);
        xCoord.setFocused2(true);
        dimensionToggle.setLocked(this.useCurrentDim);
        selectedDim.active = !dimensionToggle.isLocked();
        this.selectedDim.setMessage(new StringTextComponent(this.selectedWorld.getLocation().toString()));
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
    	super.render(matrixStack, mouseX, mouseY, partialTicks);
        
        drawCenteredString(matrixStack, this.font, title.getString(), this.getMinX() + 135, this.getMaxY() + 20, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, "X:", this.getMinX() + 28, this.getMaxY() + 41, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, "Y:", this.getMinX() + 98, this.getMaxY() + 41, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, "Z:", this.getMinX() + 168, this.getMaxY() + 41, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, tp_type.getString(), this.getMinX() + 50, this.getMaxY() + 70, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, TardisConstants.Translations.TARGET_DIM.getString(), this.getMinX() + 150, this.getMaxY() + 70, 0xFFFFFF);
//        if (this.preciseTeleport) {
//        	this.font.func_238418_a_(this.warning, this.getMinX() + 162, this.getMaxY() + 75, 80, 0xffcc00);
//        }
        displayTextBoxCoords(this.xCoord, COORD_TYPE.X);
        displayTextBoxCoords(this.yCoord, COORD_TYPE.Y);
        displayTextBoxCoords(this.zCoord, COORD_TYPE.Z);
    }

    @Override
    public boolean shouldCloseOnEsc() {
        return true;
    }

    @Override
    public void closeScreen() {
        super.closeScreen();
    }

    @Override
    public boolean isPauseScreen() {
        return true;
    }

    private void checkPosInput() {
        boolean isValid = isStringNumeric(xCoord.getText(), COORD_TYPE.X) && isStringNumeric(yCoord.getText(), COORD_TYPE.Y) && isStringNumeric(zCoord.getText(), COORD_TYPE.Z);
        boolean withinRange = isWithinConfigRange(xCoord.getText(), COORD_TYPE.X) && isWithinConfigRange(yCoord.getText(), COORD_TYPE.Y) && isWithinConfigRange(zCoord.getText(), COORD_TYPE.Z);
        if (isValid && withinRange) {
            BlockPos tpPos = new BlockPos(getInt(xCoord.getText(), COORD_TYPE.X), getInt(yCoord.getText(), COORD_TYPE.Y), getInt(zCoord.getText(), COORD_TYPE.Z));
            Network.sendToServer(new VMTeleportMessage(tpPos, this.selectedWorld, this.preciseTeleport, !this.useCurrentDim));
        }
    }

    private boolean isStringNumeric(String input, COORD_TYPE type) {
        if (input != null && !input.isEmpty()) {
            try {
                Integer.parseInt(input);
                return true;
            } catch (NumberFormatException nfe) {
            	this.minecraft.player.sendStatusMessage(TextHelper.createVortexManipMessage(new TranslationTextComponent("message.vm.invalidInput", type.toString())), false);
                return false;
            }
        } else if (input.isEmpty()) {
            return true;
        }
        return false;
    }

    private int getInt(String num, COORD_TYPE type) {
        if (isStringNumeric(num, type)) {
            if (num.isEmpty()) { //Make it so that if a textbox is empty, it will use player current coords
                return getDefaultCoordInt(type);
            } else {
                return Integer.parseInt(num);
            }
        }
        return getDefaultCoordInt(type);
    }

    private void displayTextBoxCoords(TextFieldWidget widget, COORD_TYPE type) {
        if (widget.isFocused()) {
            widget.setSuggestion("");
        } else if (widget.getText().isEmpty()) {
            widget.setSuggestion(Integer.toString(this.getDefaultCoordInt(type)));
        }
    }

    private int getDefaultCoordInt(COORD_TYPE type) {
        switch (type) { //Teleports user to current position if they try to parse in non-integers
            case X:
                return (int) this.minecraft.player.getPosX();
            case Y:
                return (int) this.minecraft.player.getPosY();
            case Z:
                return (int) this.minecraft.player.getPosZ();
            default:
                return 0;
        }
    }

    public boolean getTeleportType() {
        return preciseTeleport;
    }

    public void setTeleportType(boolean type) {
        this.preciseTeleport = type;
    }

    public boolean isWithinConfigRange(String num, COORD_TYPE type) {
        if ((getInt(num, type) - getDefaultCoordInt(type)) <= TConfig.SERVER.vmTeleportRange.get()) {
            return true;
        } else {
            this.minecraft.player.sendStatusMessage(TextHelper.createVortexManipMessage(new TranslationTextComponent("message.vm.illegalPos", TConfig.SERVER.vmTeleportRange.get(), type.toString())), false);
            return false;
        }
    }

    public void incrementId() {
        if (settingId + 1 > 1) {
            settingId = 0;
        } else {
            ++settingId;
        }
    }

    @Override
    public int getMinY() {
        return super.getMinY();
    }

    @Override
    public int getMinX() {
        return super.getMinX();
    }

    @Override
    public int getMaxX() {
        return super.getMaxX();
    }

    @Override
    public int getMaxY() {
        return super.getMaxY();
    }

    @Override
    public int texWidth() {
        return super.texWidth();
    }

    @Override
    public int texHeight() {
        return super.texHeight();
    }
    
    private void modIndex(int amount) {
    	int temp = index + amount;
        if (temp < worldKeys.size() && temp > 0) {
            this.index = temp;
        }
        if (temp > worldKeys.size() - 1) {
            this.index = 0;
        } else if (temp < 0) {
            this.index = worldKeys.size() - 1;
        }
        if (!this.worldKeys.isEmpty()) {
        	this.selectedWorld = this.worldKeys.get(index);
        	StringTextComponent text = new StringTextComponent(this.selectedWorld != null ? selectedWorld.getLocation().toString() : "");
            this.selectedDim.setMessage(text);
        }
    }

    public enum COORD_TYPE {
        X, Y, Z
    }

	@Override
	public void setWorldsFromServer(List<RegistryKey<World>> worlds) {
		this.worldKeys.clear();
		this.worldKeys.addAll(worlds);
	}
}
