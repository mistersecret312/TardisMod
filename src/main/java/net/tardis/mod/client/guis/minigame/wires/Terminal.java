package net.tardis.mod.client.guis.minigame.wires;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.util.math.vector.Vector3i;
import net.tardis.mod.client.guis.minigame.wires.Wire.Type;
import net.tardis.mod.helper.Helper;

public class Terminal {

	int x;
	int y;
	Type type;
	boolean isComplete = false;
	
	public Terminal(Type type, int x, int y) {
		this.type = type;
		this.x = x;
		this.y = y;
	}
	
	public boolean onClick(int mouseX, int mouseY, Wire w) {
		if(Helper.isInBounds(mouseX, mouseY, x, y, x + Wire.SIZE_X, y + Wire.SIZE_Y)) {
			if(w != null) {
				if(w.getType() == type) {
					this.setComplete(true);
					w.setComplete(true);
					w.setEndPos(new Vector3i(this.x + 1, this.y + Wire.SIZE_Y / 2 - 3, 0));
					return true;
				}
				return false;
				
			}
		}
		return true;
	}
	
	public void setComplete(boolean complete) {
		this.isComplete = complete;
	}
	
	public boolean isComplete() {
		return this.isComplete;
	}

	public void render(BufferBuilder bb) {
		
		float minU = (float) type.getMinU(), maxU = (float) type.getMaxU();
		float minV = (float) type.getMinV(), maxV = (float) type.getMaxV();
		
		bb.pos(x, y, 0).tex(minU, minV).endVertex();
		bb.pos(x, y + Wire.SIZE_Y, 0).tex(minU, maxV).endVertex();
		bb.pos(x + Wire.SIZE_X, y + Wire.SIZE_Y, 0).tex(maxU, maxV).endVertex();
		bb.pos(x + Wire.SIZE_X, y, 0).tex(maxU, minV).endVertex();
	}
	
}
