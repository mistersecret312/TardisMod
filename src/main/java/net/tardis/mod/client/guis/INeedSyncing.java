package net.tardis.mod.client.guis;

import net.tardis.mod.enums.EnumSubsystemType;

import java.util.Map;

public interface INeedSyncing {
	void setSubsystemStatesFromServer(Map<EnumSubsystemType, Boolean> subsystemStates);

	void setUpgradeStatesFromServer(Map<Integer, Boolean> upgradeStates);
}
