package net.tardis.mod.client.models.consoles;

import java.util.function.Function;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.controls.CommunicatorControl;
import net.tardis.mod.controls.DimensionControl;
import net.tardis.mod.controls.DoorControl;
import net.tardis.mod.controls.FacingControl;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.IncModControl;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.controls.RandomiserControl;
import net.tardis.mod.controls.RefuelerControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.controls.XControl;
import net.tardis.mod.controls.YControl;
import net.tardis.mod.controls.ZControl;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.subsystem.IStabilizerSubsystem;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;

// Made with Blockbench 3.7.5
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports


public class NemoConsoleModel extends AbstractConsoleRenderTypedModel<NemoConsoleTile>{
    private final LightModelRenderer glow_timerotor_slide_y;
    private final LightModelRenderer glow_baseoffset_1;
    private final LightModelRenderer glow_baseoffset_2;
    private final LightModelRenderer glow_baseoffset_3;
    private final ModelRenderer console;
    private final ModelRenderer bottom_plate;
    private final ModelRenderer stations;
    private final ModelRenderer station_1;
    private final ModelRenderer edge_1;
    private final ModelRenderer cooling_blades_1;
    private final ModelRenderer plasma_coil_1;
    private final ModelRenderer belly_1;
    private final ModelRenderer bolts;
    private final ModelRenderer bolts_b;
    private final ModelRenderer bolts_c;
    private final ModelRenderer plane_1;
    private final ModelRenderer rib_1;
    private final ModelRenderer rib_tilt_1;
    private final ModelRenderer rib_deco_1;
    private final ModelRenderer lowerrib_tilt_2;
    private final ModelRenderer base_fin_1;
    private final ModelRenderer clawfoot_1;
    private final ModelRenderer leg_1;
    private final ModelRenderer ball_1;
    private final ModelRenderer station_2;
    private final ModelRenderer edge_2;
    private final ModelRenderer cooling_blades_2;
    private final ModelRenderer plasma_coil_2;
    private final ModelRenderer belly_2;
    private final ModelRenderer bolts2;
    private final ModelRenderer bolts_b2;
    private final ModelRenderer bolts_c2;
    private final ModelRenderer plane_2;
    private final ModelRenderer rib_2;
    private final ModelRenderer rib_tilt_2;
    private final ModelRenderer rib_deco_2;
    private final ModelRenderer lowerrib_tilt_3;
    private final ModelRenderer base_fin_2;
    private final ModelRenderer clawfoot_2;
    private final ModelRenderer leg_2;
    private final ModelRenderer ball_2;
    private final ModelRenderer station_3;
    private final ModelRenderer edge_3;
    private final ModelRenderer cooling_blades_3;
    private final ModelRenderer plasma_coil_3;
    private final ModelRenderer belly_3;
    private final ModelRenderer bolts3;
    private final ModelRenderer bolts_b3;
    private final ModelRenderer bolts_c3;
    private final ModelRenderer plane_3;
    private final ModelRenderer rib_3;
    private final ModelRenderer rib_tilt_3;
    private final ModelRenderer rib_deco_3;
    private final ModelRenderer lowerrib_tilt_4;
    private final ModelRenderer base_fin_3;
    private final ModelRenderer clawfoot_3;
    private final ModelRenderer leg_3;
    private final ModelRenderer ball_3;
    private final ModelRenderer station_4;
    private final ModelRenderer edge_4;
    private final ModelRenderer cooling_blades_4;
    private final ModelRenderer plasma_coil_4;
    private final ModelRenderer belly_4;
    private final ModelRenderer bolts4;
    private final ModelRenderer bolts_b4;
    private final ModelRenderer bolts_c4;
    private final ModelRenderer plane_4;
    private final ModelRenderer rib_4;
    private final ModelRenderer rib_tilt_4;
    private final ModelRenderer rib_deco_4;
    private final ModelRenderer lowerrib_tilt_5;
    private final ModelRenderer base_fin_4;
    private final ModelRenderer clawfoot_4;
    private final ModelRenderer leg_4;
    private final ModelRenderer ball_4;
    private final ModelRenderer station_5;
    private final ModelRenderer edge_5;
    private final ModelRenderer cooling_blades_5;
    private final ModelRenderer plasma_coil_5;
    private final ModelRenderer belly_5;
    private final ModelRenderer bolts5;
    private final ModelRenderer bolts_b5;
    private final ModelRenderer bolts_c5;
    private final ModelRenderer plane_5;
    private final ModelRenderer rib_5;
    private final ModelRenderer rib_tilt_5;
    private final ModelRenderer rib_deco_5;
    private final ModelRenderer lowerrib_tilt_6;
    private final ModelRenderer base_fin_5;
    private final ModelRenderer clawfoot_5;
    private final ModelRenderer leg_5;
    private final ModelRenderer ball_5;
    private final ModelRenderer station_6;
    private final ModelRenderer edge_6;
    private final ModelRenderer cooling_blades_6;
    private final ModelRenderer plasma_coil_6;
    private final ModelRenderer belly_6;
    private final ModelRenderer bolts6;
    private final ModelRenderer bolts_b6;
    private final ModelRenderer bolts_c6;
    private final ModelRenderer plane_6;
    private final ModelRenderer rib_6;
    private final ModelRenderer rib_tilt_6;
    private final ModelRenderer rib_deco_6;
    private final ModelRenderer lowerrib_tilt_7;
    private final ModelRenderer base_fin_6;
    private final ModelRenderer clawfoot_6;
    private final ModelRenderer leg_6;
    private final ModelRenderer ball_6;
    private final ModelRenderer controls;
    private final ModelRenderer side1;
    private final ModelRenderer dummy_barometer;
    private final LightModelRenderer glow_barometer;
    private final ModelRenderer station_tilt_3;
    private final ModelRenderer throttle;
    private final ModelRenderer throttle_lever_rotate_x;
    private final ModelRenderer throttle_wheel;
    private final ModelRenderer bone2;
    private final ModelRenderer spacer;
    private final ModelRenderer spacer_wheel1;
    private final ModelRenderer spacer_tilt1;
    private final ModelRenderer handbreak;
    private final ModelRenderer handbreak_lever2_rotate_x;
    private final ModelRenderer handbreak_wheel2;
    private final ModelRenderer handbreak_tilt2;
    private final ModelRenderer refueler;
    private final ModelRenderer refuel_needle_rotate_z;
    private final LightModelRenderer glow_refuel;
    private final ModelRenderer dummy_dial_c1;
    private final LightModelRenderer glow_c1;
    private final ModelRenderer dummy_dial_c2;
    private final LightModelRenderer glow_c2;
    private final ModelRenderer dummy_button_C1;
    private final ModelRenderer side2;
    private final ModelRenderer station_tilt_2;
    private final ModelRenderer facing_dial;
    private final ModelRenderer faceing_needle_rotate_z;
    private final LightModelRenderer glow_dial;
    private final ModelRenderer landing_type;
    private final ModelRenderer rotate_bar_x;
    private final ModelRenderer slider_plate;
    private final ModelRenderer up;
    private final ModelRenderer up2;
    private final ModelRenderer dummy_button_b1;
    private final LightModelRenderer glow_b1;
    private final ModelRenderer randomizer;
    private final ModelRenderer rotate_y;
    private final LightModelRenderer glow_globe;
    private final ModelRenderer pipes_2;
    private final ModelRenderer side3;
    private final ModelRenderer station_offset_1;
    private final ModelRenderer xyz_increment_rotate_z;
    private final ModelRenderer spokes;
    private final ModelRenderer spokes2;
    private final ModelRenderer center;
    private final ModelRenderer station_tilt_1;
    private final ModelRenderer coorddial_x;
    private final LightModelRenderer glow_cord_x;
    private final ModelRenderer coorddial_y;
    private final LightModelRenderer glow_cord_y;
    private final ModelRenderer coorddial_z;
    private final LightModelRenderer glow_cord_z;
    private final ModelRenderer fancy_nameplate;
    private final ModelRenderer dummy_toggle_a1;
    private final ModelRenderer tilt;
    private final ModelRenderer tilt2;
    private final ModelRenderer dummy_toggle_a2;
    private final ModelRenderer tilt3;
    private final ModelRenderer tilt4;
    private final ModelRenderer dummy_toggle_a3;
    private final ModelRenderer tilt5;
    private final ModelRenderer tilt6;
    private final ModelRenderer dummy_toggle_a4;
    private final ModelRenderer tilt7;
    private final ModelRenderer tilt8;
    private final ModelRenderer dummy_toggle_a5;
    private final ModelRenderer tilt9;
    private final ModelRenderer tilt10;
    private final ModelRenderer dummy_toggle_a6;
    private final ModelRenderer tilt11;
    private final ModelRenderer tilt12;
    private final ModelRenderer dummy_toggle_a7;
    private final ModelRenderer tilt13;
    private final ModelRenderer tilt14;
    private final ModelRenderer dummy_toggle_a8;
    private final ModelRenderer tilt15;
    private final ModelRenderer tilt16;
    private final ModelRenderer dummy_button_a1;
    private final ModelRenderer dummy_button_a2;
    private final ModelRenderer dummy_button_a3;
    private final ModelRenderer side4;
    private final ModelRenderer dimention_select;
    private final ModelRenderer dialframe;
    private final ModelRenderer dialframe2;
    private final ModelRenderer needle_rotate_z;
    private final ModelRenderer station_tilt_4;
    private final ModelRenderer waypoint_selector;
    private final LightModelRenderer glow_selector;
    private final ModelRenderer dummy_button_d1;
    private final ModelRenderer dummy_button_d2;
    private final ModelRenderer sonic_port;
    private final ModelRenderer dummy_dial_d1;
    private final LightModelRenderer glow_d1;
    private final ModelRenderer dummy_dial_d2;
    private final LightModelRenderer glow_d2;
    private final ModelRenderer side5;
    private final ModelRenderer telepathic_circuits;
    private final ModelRenderer station_tilt_5;
    private final ModelRenderer pipes_5;
    private final LightModelRenderer glow_telepathics;
    private final ModelRenderer bone;
    private final ModelRenderer bone3;
    private final ModelRenderer dummy_plate;
    private final ModelRenderer dummy_bell_e1;
    private final ModelRenderer dummy_bell_e2;
    private final ModelRenderer dummy_bell_e3;
    private final ModelRenderer dummy_bell_e4;
    private final ModelRenderer fast_return;
    private final ModelRenderer fast_return_button_rotate_x;
    private final ModelRenderer stablizer;
    private final ModelRenderer stablizer_button_rotate_x2;
    private final ModelRenderer side6;
    private final ModelRenderer station_tilt_6;
    private final ModelRenderer door;
    private final ModelRenderer door_plate;
    private final ModelRenderer door_knob_rotate_z;
    private final ModelRenderer dummy_button_f1;
    private final LightModelRenderer glow_f1;
    private final ModelRenderer dummy_button_f2;
    private final LightModelRenderer glow_f2;
    private final ModelRenderer dummy_button_f3;
    private final ModelRenderer dummy_button_f4;
    private final LightModelRenderer glow_f4;
    private final ModelRenderer dummy_button_f5;
    private final ModelRenderer dummy_button_f6;
    private final LightModelRenderer glow_f6;
    private final ModelRenderer coms;
    private final ModelRenderer bell_rotate_x;
    private final ModelRenderer time_rotor_slide_y;
    private final ModelRenderer rotor_plate;
    private final ModelRenderer rotor_post_1;
    private final ModelRenderer rotor_post_2;
    private final ModelRenderer rotor_post_3;
    private final ModelRenderer controls_hitbox;
    private final ModelRenderer inc_hitbox;
    private final ModelRenderer x_hitbox;
    private final ModelRenderer y_hitbox;
    private final ModelRenderer z_hitbox;
    private final ModelRenderer randomizer_hitbox;
    private final ModelRenderer facing_hitbox;
    private final ModelRenderer landing_hitbox;
    private final ModelRenderer refuel_hitbox;
    private final ModelRenderer throttle_hitbox;
    private final ModelRenderer handbreak_hitbox;
    private final ModelRenderer dimention_hitbox;
    private final ModelRenderer sonic_hitbox;
    private final ModelRenderer waypoints_hitbox;
    private final ModelRenderer fastreturn_hitbox;
    private final ModelRenderer stablizers_hitbox;
    private final ModelRenderer telepathic_hitbox;
    private final ModelRenderer door_hitbox;
    private final ModelRenderer coms_hitbox;

    public NemoConsoleModel(Function<ResourceLocation, RenderType> renderType) {
        super(renderType);
        textureWidth = 256;
        textureHeight = 256;

        glow_timerotor_slide_y = new LightModelRenderer(this);
        glow_timerotor_slide_y.setRotationPoint(0.0F, 31.0F, 0.0F);
        glow_timerotor_slide_y.setTextureOffset(172, 10).addBox(-5.25F, -100.6F, -5.0F, 10.0F, 10.0F, 10.0F, 0.0F, false);
        glow_timerotor_slide_y.setTextureOffset(172, 10).addBox(-5.25F, -110.6F, -5.0F, 10.0F, 10.0F, 10.0F, 0.0F, false);

        glow_baseoffset_1 = new LightModelRenderer(this);
        glow_baseoffset_1.setRotationPoint(0.0F, -8.0F, 0.5F);
        glow_baseoffset_1.setTextureOffset(171, 2).addBox(-10.0F, -14.0F, -17.5F, 20.0F, 28.0F, 1.0F, 0.0F, false);
        glow_baseoffset_1.setTextureOffset(171, 2).addBox(-10.0F, -14.0F, 16.5F, 20.0F, 28.0F, 1.0F, 0.0F, false);

        glow_baseoffset_2 = new LightModelRenderer(this);
        glow_baseoffset_2.setRotationPoint(0.0F, -8.0F, 0.5F);
        setRotationAngle(glow_baseoffset_2, 0.0F, -1.0472F, 0.0F);
        glow_baseoffset_2.setTextureOffset(171, 2).addBox(-10.0F, -14.0F, -17.5F, 20.0F, 28.0F, 1.0F, 0.0F, false);
        glow_baseoffset_2.setTextureOffset(171, 2).addBox(-10.0F, -14.0F, 16.5F, 20.0F, 28.0F, 1.0F, 0.0F, false);

        glow_baseoffset_3 = new LightModelRenderer(this);
        glow_baseoffset_3.setRotationPoint(0.0F, -8.0F, 0.5F);
        setRotationAngle(glow_baseoffset_3, 0.0F, -2.0944F, 0.0F);
        glow_baseoffset_3.setTextureOffset(171, 2).addBox(-10.0F, -14.0F, -17.5F, 20.0F, 28.0F, 1.0F, 0.0F, false);
        glow_baseoffset_3.setTextureOffset(171, 2).addBox(-10.0F, -14.0F, 16.5F, 20.0F, 28.0F, 1.0F, 0.0F, false);

        console = new ModelRenderer(this);
        console.setRotationPoint(0.0F, 24.0F, 0.0F);
        

        bottom_plate = new ModelRenderer(this);
        bottom_plate.setRotationPoint(0.0F, 0.0F, 0.0F);
        console.addChild(bottom_plate);
        bottom_plate.setTextureOffset(11, 95).addBox(-20.0F, -1.6F, -8.0F, 40.0F, 2.0F, 16.0F, 0.0F, false);
        bottom_plate.setTextureOffset(21, 96).addBox(-16.0F, -1.6F, -16.0F, 32.0F, 2.0F, 8.0F, 0.0F, false);
        bottom_plate.setTextureOffset(26, 102).addBox(-16.0F, -1.6F, 8.0F, 32.0F, 2.0F, 8.0F, 0.0F, false);

        stations = new ModelRenderer(this);
        stations.setRotationPoint(0.0F, 0.0F, 0.0F);
        console.addChild(stations);
        

        station_1 = new ModelRenderer(this);
        station_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        stations.addChild(station_1);
        

        edge_1 = new ModelRenderer(this);
        edge_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_1.addChild(edge_1);
        edge_1.setTextureOffset(13, 1).addBox(-34.0F, -60.0F, -61.0F, 67.0F, 4.0F, 6.0F, 0.0F, false);
        edge_1.setTextureOffset(59, 96).addBox(-10.0F, -79.0F, -21.0F, 20.0F, 16.0F, 6.0F, 0.0F, false);
        edge_1.setTextureOffset(40, 95).addBox(-10.0F, -71.0F, -15.0F, 20.0F, 8.0F, 6.0F, 0.0F, false);
        edge_1.setTextureOffset(41, 102).addBox(-13.0F, -91.0F, -23.0F, 25.0F, 4.0F, 6.0F, 0.0F, false);
        edge_1.setTextureOffset(38, 93).addBox(-10.0F, -82.0F, -23.0F, 19.0F, 4.0F, 6.0F, 0.0F, false);
        edge_1.setTextureOffset(49, 95).addBox(-9.0F, -93.0F, -20.0F, 20.0F, 14.0F, 4.0F, 0.0F, false);
        edge_1.setTextureOffset(193, 64).addBox(-8.0F, -117.0F, -16.0F, 16.0F, 1.0F, 2.0F, 0.0F, false);
        edge_1.setTextureOffset(80, 110).addBox(-8.0F, -116.0F, -17.0F, 16.0F, 1.0F, 3.0F, 0.0F, false);
        edge_1.setTextureOffset(194, 71).addBox(-8.0F, -115.0F, -16.0F, 16.0F, 1.0F, 2.0F, 0.0F, false);

        cooling_blades_1 = new ModelRenderer(this);
        cooling_blades_1.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_1.addChild(cooling_blades_1);
        cooling_blades_1.setTextureOffset(41, 90).addBox(-10.9F, -45.0F, -22.0F, 4.0F, 28.0F, 5.0F, 0.0F, false);
        cooling_blades_1.setTextureOffset(30, 85).addBox(-2.0F, -45.0F, -22.0F, 4.0F, 28.0F, 5.0F, 0.0F, false);
        cooling_blades_1.setTextureOffset(46, 84).addBox(7.1F, -45.0F, -22.0F, 4.0F, 28.0F, 5.0F, 0.0F, false);

        plasma_coil_1 = new ModelRenderer(this);
        plasma_coil_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        edge_1.addChild(plasma_coil_1);
        plasma_coil_1.setTextureOffset(146, 12).addBox(-10.0F, -81.0F, -19.0F, 20.0F, 2.0F, 2.0F, 0.0F, false);

        belly_1 = new ModelRenderer(this);
        belly_1.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_1.addChild(belly_1);
        belly_1.setTextureOffset(46, 103).addBox(-14.0F, -19.0F, -24.0F, 28.0F, 8.0F, 3.0F, 0.0F, false);
        belly_1.setTextureOffset(41, 88).addBox(-13.0F, -61.0F, -24.0F, 26.0F, 20.0F, 3.0F, 0.0F, false);
        belly_1.setTextureOffset(42, 117).addBox(-14.0F, -2.0F, -28.0F, 28.0F, 3.0F, 12.0F, 0.0F, false);
        belly_1.setTextureOffset(46, 120).addBox(-14.0F, -4.0F, -27.0F, 28.0F, 2.0F, 2.0F, 0.0F, false);
        belly_1.setTextureOffset(48, 112).addBox(-14.0F, -11.0F, -26.0F, 27.0F, 7.0F, 3.0F, 0.0F, false);
        belly_1.setTextureOffset(47, 105).addBox(-14.0F, -59.0F, -26.0F, 27.0F, 10.0F, 3.0F, 0.0F, false);

        bolts = new ModelRenderer(this);
        bolts.setRotationPoint(0.0F, 0.0F, 0.0F);
        belly_1.addChild(bolts);
        bolts.setTextureOffset(79, 80).addBox(-11.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts.setTextureOffset(79, 80).addBox(-6.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts.setTextureOffset(79, 80).addBox(-1.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts.setTextureOffset(79, 80).addBox(4.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts.setTextureOffset(79, 80).addBox(9.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);

        bolts_b = new ModelRenderer(this);
        bolts_b.setRotationPoint(0.0F, 0.0F, 0.0F);
        belly_1.addChild(bolts_b);
        bolts_b.setTextureOffset(72, 82).addBox(-11.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b.setTextureOffset(72, 82).addBox(-6.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b.setTextureOffset(72, 82).addBox(-1.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b.setTextureOffset(72, 82).addBox(4.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b.setTextureOffset(72, 82).addBox(9.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);

        bolts_c = new ModelRenderer(this);
        bolts_c.setRotationPoint(0.0F, 0.0F, 0.0F);
        belly_1.addChild(bolts_c);
        bolts_c.setTextureOffset(77, 80).addBox(-9.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_c.setTextureOffset(77, 80).addBox(-4.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_c.setTextureOffset(77, 80).addBox(2.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_c.setTextureOffset(77, 80).addBox(7.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);

        plane_1 = new ModelRenderer(this);
        plane_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_1.addChild(plane_1);
        setRotationAngle(plane_1, 0.5236F, 0.0F, 0.0F);
        plane_1.setTextureOffset(5, 42).addBox(-31.2F, -80.0F, -20.0F, 62.0F, 4.0F, 6.0F, 0.0F, false);
        plane_1.setTextureOffset(5, 42).addBox(-28.2F, -80.0F, -14.0F, 56.0F, 4.0F, 6.0F, 0.0F, false);
        plane_1.setTextureOffset(5, 42).addBox(-26.0F, -80.0F, -8.0F, 51.0F, 4.0F, 6.0F, 0.0F, false);
        plane_1.setTextureOffset(5, 42).addBox(-22.0F, -80.0F, -2.0F, 44.0F, 4.0F, 6.0F, 0.0F, false);
        plane_1.setTextureOffset(2, 82).addBox(-17.0F, -77.0F, -17.0F, 34.0F, 8.0F, 30.0F, 0.0F, false);
        plane_1.setTextureOffset(17, 92).addBox(-11.0F, -69.0F, -11.0F, 22.0F, 4.0F, 24.0F, 0.0F, false);
        plane_1.setTextureOffset(5, 42).addBox(-19.0F, -80.0F, 4.0F, 38.0F, 4.0F, 6.0F, 0.0F, false);
        plane_1.setTextureOffset(5, 42).addBox(-16.0F, -80.0F, 10.0F, 32.0F, 4.0F, 6.0F, 0.0F, false);
        plane_1.setTextureOffset(5, 42).addBox(-14.0F, -80.0F, 16.0F, 27.0F, 4.0F, 6.0F, 0.0F, false);

        rib_1 = new ModelRenderer(this);
        rib_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_1.addChild(rib_1);
        setRotationAngle(rib_1, 0.0F, -0.5236F, 0.0F);
        

        rib_tilt_1 = new ModelRenderer(this);
        rib_tilt_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_1.addChild(rib_tilt_1);
        setRotationAngle(rib_tilt_1, 0.4363F, 0.0F, 0.0F);
        rib_tilt_1.setTextureOffset(20, 76).addBox(-2.0F, -83.4F, -39.5F, 4.0F, 8.0F, 48.0F, 0.0F, false);
        rib_tilt_1.setTextureOffset(20, 75).addBox(-1.0F, -76.0F, -39.0F, 2.0F, 6.0F, 52.0F, 0.0F, false);
        rib_tilt_1.setTextureOffset(65, 94).addBox(-1.0F, -76.0F, -13.0F, 2.0F, 26.0F, 11.0F, 0.0F, false);
        rib_tilt_1.setTextureOffset(65, 94).addBox(-2.0F, -70.0F, -8.0F, 4.0F, 14.0F, 22.0F, 0.0F, false);

        rib_deco_1 = new ModelRenderer(this);
        rib_deco_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_tilt_1.addChild(rib_deco_1);
        rib_deco_1.setTextureOffset(87, 112).addBox(-3.0F, -84.5F, -40.0F, 6.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_1.setTextureOffset(65, 94).addBox(-4.0F, -86.0F, 9.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_1.setTextureOffset(65, 94).addBox(-3.0F, -84.4F, 8.0F, 6.0F, 7.0F, 8.0F, 0.0F, false);

        lowerrib_tilt_2 = new ModelRenderer(this);
        lowerrib_tilt_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_1.addChild(lowerrib_tilt_2);
        setRotationAngle(lowerrib_tilt_2, 0.4363F, 0.0F, 0.0F);
        lowerrib_tilt_2.setTextureOffset(205, 68).addBox(-1.0F, -70.0F, -19.0F, 2.0F, 20.0F, 2.0F, 0.0F, false);
        lowerrib_tilt_2.setTextureOffset(65, 94).addBox(-2.0F, -70.0F, -8.0F, 4.0F, 14.0F, 22.0F, 0.0F, false);
        lowerrib_tilt_2.setTextureOffset(65, 94).addBox(-1.0F, -21.8F, -41.0F, 2.0F, 2.0F, 17.0F, 0.0F, false);
        lowerrib_tilt_2.setTextureOffset(65, 94).addBox(-1.0F, -19.8F, -34.0F, 2.0F, 3.0F, 9.0F, 0.0F, false);
        lowerrib_tilt_2.setTextureOffset(170, 82).addBox(-1.0F, -27.8F, -49.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

        base_fin_1 = new ModelRenderer(this);
        base_fin_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_1.addChild(base_fin_1);
        setRotationAngle(base_fin_1, 0.0F, -0.5236F, 0.0F);
        base_fin_1.setTextureOffset(91, 90).addBox(-1.0F, -48.2F, -23.0F, 2.0F, 28.0F, 4.0F, 0.0F, false);
        base_fin_1.setTextureOffset(60, 92).addBox(-2.0F, -94.2F, -27.0F, 4.0F, 24.0F, 12.0F, 0.0F, false);
        base_fin_1.setTextureOffset(51, 92).addBox(-2.0F, -118.2F, -19.0F, 4.0F, 24.0F, 4.0F, 0.0F, false);
        base_fin_1.setTextureOffset(76, 112).addBox(-2.4F, -13.2F, -32.0F, 5.0F, 13.0F, 6.0F, 0.0F, false);
        base_fin_1.setTextureOffset(63, 98).addBox(-1.4F, -21.2F, -28.0F, 3.0F, 8.0F, 8.0F, 0.0F, false);
        base_fin_1.setTextureOffset(174, 70).addBox(-0.4F, -41.0F, -33.0F, 1.0F, 40.0F, 2.0F, 0.0F, false);

        clawfoot_1 = new ModelRenderer(this);
        clawfoot_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_1.addChild(clawfoot_1);
        setRotationAngle(clawfoot_1, 0.0F, -0.5236F, 0.0F);
        clawfoot_1.setTextureOffset(231, 8).addBox(-1.5F, -38.7F, -39.4F, 3.0F, 4.0F, 3.0F, 0.0F, false);
        clawfoot_1.setTextureOffset(225, 12).addBox(-1.4F, -26.2F, -34.0F, 3.0F, 4.0F, 3.0F, 0.0F, false);
        clawfoot_1.setTextureOffset(226, 10).addBox(-1.4F, -26.2F, -39.4F, 3.0F, 4.0F, 3.0F, 0.0F, false);
        clawfoot_1.setTextureOffset(194, 81).addBox(-0.3F, -25.3F, -36.4F, 1.0F, 2.0F, 3.0F, 0.0F, false);
        clawfoot_1.setTextureOffset(178, 66).addBox(-0.5F, -39.0F, -39.0F, 1.0F, 28.0F, 2.0F, 0.0F, false);
        clawfoot_1.setTextureOffset(224, 16).addBox(-1.5F, -13.2F, -39.4F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        leg_1 = new ModelRenderer(this);
        leg_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_1.addChild(leg_1);
        leg_1.setTextureOffset(44, 102).addBox(-2.4F, -4.2F, -58.0F, 5.0F, 4.0F, 27.0F, 0.0F, false);

        ball_1 = new ModelRenderer(this);
        ball_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_1.addChild(ball_1);
        ball_1.setTextureOffset(29, 102).addBox(-3.4F, -6.0F, -57.0F, 7.0F, 6.0F, 6.0F, 0.0F, false);
        ball_1.setTextureOffset(29, 102).addBox(-3.4F, -6.0F, -47.0F, 7.0F, 6.0F, 2.0F, 0.0F, false);
        ball_1.setTextureOffset(29, 102).addBox(-3.4F, -6.0F, -44.6F, 7.0F, 6.0F, 2.0F, 0.0F, false);
        ball_1.setTextureOffset(29, 102).addBox(-3.4F, -6.0F, -42.1F, 7.0F, 6.0F, 2.0F, 0.0F, false);

        station_2 = new ModelRenderer(this);
        station_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        stations.addChild(station_2);
        setRotationAngle(station_2, 0.0F, -1.0472F, 0.0F);
        

        edge_2 = new ModelRenderer(this);
        edge_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_2.addChild(edge_2);
        edge_2.setTextureOffset(13, 1).addBox(-34.0F, -60.0F, -61.0F, 67.0F, 4.0F, 6.0F, 0.0F, false);
        edge_2.setTextureOffset(59, 96).addBox(-10.0F, -79.0F, -21.0F, 20.0F, 16.0F, 6.0F, 0.0F, false);
        edge_2.setTextureOffset(40, 95).addBox(-10.0F, -71.0F, -15.0F, 20.0F, 8.0F, 6.0F, 0.0F, false);
        edge_2.setTextureOffset(41, 102).addBox(-13.0F, -91.0F, -23.0F, 25.0F, 4.0F, 6.0F, 0.0F, false);
        edge_2.setTextureOffset(38, 93).addBox(-10.0F, -82.0F, -23.0F, 19.0F, 4.0F, 6.0F, 0.0F, false);
        edge_2.setTextureOffset(49, 95).addBox(-9.0F, -93.0F, -20.0F, 20.0F, 14.0F, 4.0F, 0.0F, false);
        edge_2.setTextureOffset(193, 64).addBox(-8.0F, -117.0F, -16.0F, 16.0F, 1.0F, 2.0F, 0.0F, false);
        edge_2.setTextureOffset(80, 110).addBox(-8.0F, -116.0F, -17.0F, 16.0F, 1.0F, 3.0F, 0.0F, false);
        edge_2.setTextureOffset(194, 71).addBox(-8.0F, -115.0F, -16.0F, 16.0F, 1.0F, 2.0F, 0.0F, false);

        cooling_blades_2 = new ModelRenderer(this);
        cooling_blades_2.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_2.addChild(cooling_blades_2);
        cooling_blades_2.setTextureOffset(41, 90).addBox(-10.9F, -45.0F, -22.0F, 4.0F, 28.0F, 5.0F, 0.0F, false);
        cooling_blades_2.setTextureOffset(30, 85).addBox(-2.0F, -45.0F, -22.0F, 4.0F, 28.0F, 5.0F, 0.0F, false);
        cooling_blades_2.setTextureOffset(46, 84).addBox(7.1F, -45.0F, -22.0F, 4.0F, 28.0F, 5.0F, 0.0F, false);

        plasma_coil_2 = new ModelRenderer(this);
        plasma_coil_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        edge_2.addChild(plasma_coil_2);
        plasma_coil_2.setTextureOffset(146, 12).addBox(-10.0F, -81.0F, -19.0F, 20.0F, 2.0F, 2.0F, 0.0F, false);

        belly_2 = new ModelRenderer(this);
        belly_2.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_2.addChild(belly_2);
        belly_2.setTextureOffset(44, 108).addBox(-14.0F, -19.0F, -24.0F, 28.0F, 8.0F, 3.0F, 0.0F, false);
        belly_2.setTextureOffset(41, 88).addBox(-13.0F, -61.0F, -24.0F, 26.0F, 20.0F, 3.0F, 0.0F, false);
        belly_2.setTextureOffset(47, 117).addBox(-14.0F, -2.0F, -28.0F, 28.0F, 3.0F, 12.0F, 0.0F, false);
        belly_2.setTextureOffset(39, 121).addBox(-14.0F, -4.0F, -27.0F, 28.0F, 2.0F, 2.0F, 0.0F, false);
        belly_2.setTextureOffset(42, 105).addBox(-14.0F, -11.0F, -26.0F, 27.0F, 7.0F, 3.0F, 0.0F, false);
        belly_2.setTextureOffset(40, 105).addBox(-14.0F, -59.0F, -26.0F, 27.0F, 10.0F, 3.0F, 0.0F, false);

        bolts2 = new ModelRenderer(this);
        bolts2.setRotationPoint(0.0F, 0.0F, 0.0F);
        belly_2.addChild(bolts2);
        bolts2.setTextureOffset(72, 80).addBox(-11.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts2.setTextureOffset(72, 80).addBox(-6.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts2.setTextureOffset(72, 80).addBox(-1.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts2.setTextureOffset(72, 80).addBox(4.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts2.setTextureOffset(72, 80).addBox(9.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);

        bolts_b2 = new ModelRenderer(this);
        bolts_b2.setRotationPoint(0.0F, 0.0F, 0.0F);
        belly_2.addChild(bolts_b2);
        bolts_b2.setTextureOffset(75, 83).addBox(-11.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b2.setTextureOffset(75, 83).addBox(-6.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b2.setTextureOffset(75, 83).addBox(-1.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b2.setTextureOffset(75, 83).addBox(4.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b2.setTextureOffset(75, 83).addBox(9.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);

        bolts_c2 = new ModelRenderer(this);
        bolts_c2.setRotationPoint(0.0F, 0.0F, 0.0F);
        belly_2.addChild(bolts_c2);
        bolts_c2.setTextureOffset(82, 85).addBox(-9.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_c2.setTextureOffset(82, 85).addBox(-4.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_c2.setTextureOffset(82, 85).addBox(2.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_c2.setTextureOffset(82, 85).addBox(7.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);

        plane_2 = new ModelRenderer(this);
        plane_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_2.addChild(plane_2);
        setRotationAngle(plane_2, 0.5236F, 0.0F, 0.0F);
        plane_2.setTextureOffset(5, 42).addBox(-31.2F, -80.0F, -20.0F, 62.0F, 4.0F, 6.0F, 0.0F, false);
        plane_2.setTextureOffset(5, 42).addBox(-28.2F, -80.0F, -14.0F, 56.0F, 4.0F, 6.0F, 0.0F, false);
        plane_2.setTextureOffset(5, 42).addBox(-26.0F, -80.0F, -8.0F, 51.0F, 4.0F, 6.0F, 0.0F, false);
        plane_2.setTextureOffset(5, 42).addBox(-22.0F, -80.0F, -2.0F, 44.0F, 4.0F, 6.0F, 0.0F, false);
        plane_2.setTextureOffset(2, 82).addBox(-17.0F, -77.0F, -17.0F, 34.0F, 8.0F, 30.0F, 0.0F, false);
        plane_2.setTextureOffset(17, 92).addBox(-11.0F, -69.0F, -11.0F, 22.0F, 4.0F, 24.0F, 0.0F, false);
        plane_2.setTextureOffset(5, 42).addBox(-19.0F, -80.0F, 4.0F, 38.0F, 4.0F, 6.0F, 0.0F, false);
        plane_2.setTextureOffset(5, 42).addBox(-16.0F, -80.0F, 10.0F, 32.0F, 4.0F, 6.0F, 0.0F, false);
        plane_2.setTextureOffset(5, 42).addBox(-14.0F, -80.0F, 16.0F, 27.0F, 4.0F, 6.0F, 0.0F, false);

        rib_2 = new ModelRenderer(this);
        rib_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_2.addChild(rib_2);
        setRotationAngle(rib_2, 0.0F, -0.5236F, 0.0F);
        

        rib_tilt_2 = new ModelRenderer(this);
        rib_tilt_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_2.addChild(rib_tilt_2);
        setRotationAngle(rib_tilt_2, 0.4363F, 0.0F, 0.0F);
        rib_tilt_2.setTextureOffset(20, 76).addBox(-2.0F, -83.4F, -39.5F, 4.0F, 8.0F, 48.0F, 0.0F, false);
        rib_tilt_2.setTextureOffset(20, 75).addBox(-1.0F, -76.0F, -39.0F, 2.0F, 6.0F, 52.0F, 0.0F, false);
        rib_tilt_2.setTextureOffset(65, 94).addBox(-1.0F, -76.0F, -13.0F, 2.0F, 26.0F, 11.0F, 0.0F, false);
        rib_tilt_2.setTextureOffset(65, 94).addBox(-2.0F, -70.0F, -8.0F, 4.0F, 14.0F, 22.0F, 0.0F, false);

        rib_deco_2 = new ModelRenderer(this);
        rib_deco_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_tilt_2.addChild(rib_deco_2);
        rib_deco_2.setTextureOffset(87, 114).addBox(-3.0F, -84.5F, -40.0F, 6.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_2.setTextureOffset(65, 94).addBox(-4.0F, -86.0F, 9.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_2.setTextureOffset(65, 94).addBox(-3.0F, -84.4F, 8.0F, 6.0F, 7.0F, 8.0F, 0.0F, false);

        lowerrib_tilt_3 = new ModelRenderer(this);
        lowerrib_tilt_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_2.addChild(lowerrib_tilt_3);
        setRotationAngle(lowerrib_tilt_3, 0.4363F, 0.0F, 0.0F);
        lowerrib_tilt_3.setTextureOffset(65, 94).addBox(-2.0F, -70.0F, -8.0F, 4.0F, 14.0F, 22.0F, 0.0F, false);

        base_fin_2 = new ModelRenderer(this);
        base_fin_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_2.addChild(base_fin_2);
        setRotationAngle(base_fin_2, 0.0F, -0.5236F, 0.0F);
        base_fin_2.setTextureOffset(91, 90).addBox(-1.0F, -48.2F, -23.0F, 2.0F, 28.0F, 4.0F, 0.0F, false);
        base_fin_2.setTextureOffset(60, 92).addBox(-2.0F, -94.2F, -27.0F, 4.0F, 24.0F, 12.0F, 0.0F, false);
        base_fin_2.setTextureOffset(51, 92).addBox(-2.0F, -118.2F, -19.0F, 4.0F, 24.0F, 4.0F, 0.0F, false);
        base_fin_2.setTextureOffset(75, 105).addBox(-2.4F, -13.2F, -32.0F, 5.0F, 14.0F, 6.0F, 0.0F, false);
        base_fin_2.setTextureOffset(63, 98).addBox(-1.4F, -21.2F, -28.0F, 3.0F, 8.0F, 8.0F, 0.0F, false);

        clawfoot_2 = new ModelRenderer(this);
        clawfoot_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_2.addChild(clawfoot_2);
        setRotationAngle(clawfoot_2, 0.0F, -0.5236F, 0.0F);
        

        leg_2 = new ModelRenderer(this);
        leg_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_2.addChild(leg_2);
        

        ball_2 = new ModelRenderer(this);
        ball_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_2.addChild(ball_2);
        

        station_3 = new ModelRenderer(this);
        station_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        stations.addChild(station_3);
        setRotationAngle(station_3, 0.0F, -2.0944F, 0.0F);
        

        edge_3 = new ModelRenderer(this);
        edge_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_3.addChild(edge_3);
        edge_3.setTextureOffset(13, 1).addBox(-34.0F, -60.0F, -61.0F, 67.0F, 4.0F, 6.0F, 0.0F, false);
        edge_3.setTextureOffset(59, 96).addBox(-10.0F, -79.0F, -21.0F, 20.0F, 16.0F, 6.0F, 0.0F, false);
        edge_3.setTextureOffset(40, 95).addBox(-10.0F, -71.0F, -15.0F, 20.0F, 8.0F, 6.0F, 0.0F, false);
        edge_3.setTextureOffset(41, 102).addBox(-13.0F, -91.0F, -23.0F, 25.0F, 4.0F, 6.0F, 0.0F, false);
        edge_3.setTextureOffset(38, 93).addBox(-10.0F, -82.0F, -23.0F, 19.0F, 4.0F, 6.0F, 0.0F, false);
        edge_3.setTextureOffset(49, 95).addBox(-9.0F, -93.0F, -20.0F, 20.0F, 14.0F, 4.0F, 0.0F, false);
        edge_3.setTextureOffset(193, 64).addBox(-8.0F, -117.0F, -16.0F, 16.0F, 1.0F, 2.0F, 0.0F, false);
        edge_3.setTextureOffset(80, 110).addBox(-8.0F, -116.0F, -17.0F, 16.0F, 1.0F, 3.0F, 0.0F, false);
        edge_3.setTextureOffset(194, 71).addBox(-8.0F, -115.0F, -16.0F, 16.0F, 1.0F, 2.0F, 0.0F, false);

        cooling_blades_3 = new ModelRenderer(this);
        cooling_blades_3.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_3.addChild(cooling_blades_3);
        cooling_blades_3.setTextureOffset(41, 90).addBox(-10.9F, -45.0F, -22.0F, 4.0F, 28.0F, 5.0F, 0.0F, false);
        cooling_blades_3.setTextureOffset(30, 85).addBox(-2.0F, -45.0F, -22.0F, 4.0F, 28.0F, 5.0F, 0.0F, false);
        cooling_blades_3.setTextureOffset(46, 84).addBox(7.1F, -45.0F, -22.0F, 4.0F, 28.0F, 5.0F, 0.0F, false);

        plasma_coil_3 = new ModelRenderer(this);
        plasma_coil_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        edge_3.addChild(plasma_coil_3);
        plasma_coil_3.setTextureOffset(146, 12).addBox(-10.0F, -81.0F, -19.0F, 20.0F, 2.0F, 2.0F, 0.0F, false);

        belly_3 = new ModelRenderer(this);
        belly_3.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_3.addChild(belly_3);
        belly_3.setTextureOffset(40, 105).addBox(-14.0F, -19.0F, -24.0F, 28.0F, 8.0F, 3.0F, 0.0F, false);
        belly_3.setTextureOffset(41, 88).addBox(-13.0F, -61.0F, -24.0F, 26.0F, 20.0F, 3.0F, 0.0F, false);
        belly_3.setTextureOffset(39, 118).addBox(-14.0F, -2.0F, -28.0F, 28.0F, 3.0F, 12.0F, 0.0F, false);
        belly_3.setTextureOffset(48, 119).addBox(-14.0F, -4.0F, -27.0F, 28.0F, 2.0F, 2.0F, 0.0F, false);
        belly_3.setTextureOffset(45, 114).addBox(-14.0F, -11.0F, -26.0F, 27.0F, 7.0F, 3.0F, 0.0F, false);
        belly_3.setTextureOffset(51, 108).addBox(-14.0F, -59.0F, -26.0F, 27.0F, 10.0F, 3.0F, 0.0F, false);

        bolts3 = new ModelRenderer(this);
        bolts3.setRotationPoint(0.0F, 0.0F, 0.0F);
        belly_3.addChild(bolts3);
        bolts3.setTextureOffset(82, 83).addBox(-11.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts3.setTextureOffset(82, 83).addBox(-6.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts3.setTextureOffset(82, 83).addBox(-1.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts3.setTextureOffset(82, 83).addBox(4.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts3.setTextureOffset(82, 83).addBox(9.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);

        bolts_b3 = new ModelRenderer(this);
        bolts_b3.setRotationPoint(0.0F, 0.0F, 0.0F);
        belly_3.addChild(bolts_b3);
        bolts_b3.setTextureOffset(75, 84).addBox(-11.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b3.setTextureOffset(75, 84).addBox(-6.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b3.setTextureOffset(75, 84).addBox(-1.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b3.setTextureOffset(75, 84).addBox(4.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b3.setTextureOffset(75, 84).addBox(9.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);

        bolts_c3 = new ModelRenderer(this);
        bolts_c3.setRotationPoint(0.0F, 0.0F, 0.0F);
        belly_3.addChild(bolts_c3);
        bolts_c3.setTextureOffset(57, 80).addBox(-9.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_c3.setTextureOffset(57, 80).addBox(-4.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_c3.setTextureOffset(57, 80).addBox(2.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_c3.setTextureOffset(57, 80).addBox(7.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);

        plane_3 = new ModelRenderer(this);
        plane_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_3.addChild(plane_3);
        setRotationAngle(plane_3, 0.5236F, 0.0F, 0.0F);
        plane_3.setTextureOffset(5, 42).addBox(-31.2F, -80.0F, -20.0F, 62.0F, 4.0F, 6.0F, 0.0F, false);
        plane_3.setTextureOffset(5, 42).addBox(-28.2F, -80.0F, -14.0F, 56.0F, 4.0F, 6.0F, 0.0F, false);
        plane_3.setTextureOffset(5, 42).addBox(-26.0F, -80.0F, -8.0F, 51.0F, 4.0F, 6.0F, 0.0F, false);
        plane_3.setTextureOffset(5, 42).addBox(-22.0F, -80.0F, -2.0F, 44.0F, 4.0F, 6.0F, 0.0F, false);
        plane_3.setTextureOffset(2, 82).addBox(-17.0F, -77.0F, -17.0F, 34.0F, 8.0F, 30.0F, 0.0F, false);
        plane_3.setTextureOffset(17, 92).addBox(-11.0F, -69.0F, -11.0F, 22.0F, 4.0F, 24.0F, 0.0F, false);
        plane_3.setTextureOffset(5, 42).addBox(-19.0F, -80.0F, 4.0F, 38.0F, 4.0F, 6.0F, 0.0F, false);
        plane_3.setTextureOffset(5, 42).addBox(-16.0F, -80.0F, 10.0F, 32.0F, 4.0F, 6.0F, 0.0F, false);
        plane_3.setTextureOffset(5, 42).addBox(-14.0F, -80.0F, 16.0F, 27.0F, 4.0F, 6.0F, 0.0F, false);

        rib_3 = new ModelRenderer(this);
        rib_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_3.addChild(rib_3);
        setRotationAngle(rib_3, 0.0F, -0.5236F, 0.0F);
        

        rib_tilt_3 = new ModelRenderer(this);
        rib_tilt_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_3.addChild(rib_tilt_3);
        setRotationAngle(rib_tilt_3, 0.4363F, 0.0F, 0.0F);
        rib_tilt_3.setTextureOffset(20, 76).addBox(-2.0F, -83.4F, -39.5F, 4.0F, 8.0F, 48.0F, 0.0F, false);
        rib_tilt_3.setTextureOffset(20, 75).addBox(-1.0F, -76.0F, -39.0F, 2.0F, 6.0F, 52.0F, 0.0F, false);
        rib_tilt_3.setTextureOffset(65, 94).addBox(-1.0F, -76.0F, -13.0F, 2.0F, 26.0F, 11.0F, 0.0F, false);
        rib_tilt_3.setTextureOffset(65, 94).addBox(-2.0F, -70.0F, -8.0F, 4.0F, 14.0F, 22.0F, 0.0F, false);

        rib_deco_3 = new ModelRenderer(this);
        rib_deco_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_tilt_3.addChild(rib_deco_3);
        rib_deco_3.setTextureOffset(86, 114).addBox(-3.0F, -84.5F, -40.0F, 6.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_3.setTextureOffset(65, 94).addBox(-4.0F, -86.0F, 9.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_3.setTextureOffset(65, 94).addBox(-3.0F, -84.4F, 8.0F, 6.0F, 7.0F, 8.0F, 0.0F, false);

        lowerrib_tilt_4 = new ModelRenderer(this);
        lowerrib_tilt_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_3.addChild(lowerrib_tilt_4);
        setRotationAngle(lowerrib_tilt_4, 0.4363F, 0.0F, 0.0F);
        lowerrib_tilt_4.setTextureOffset(205, 68).addBox(-1.0F, -70.0F, -19.0F, 2.0F, 20.0F, 2.0F, 0.0F, false);
        lowerrib_tilt_4.setTextureOffset(65, 94).addBox(-2.0F, -70.0F, -8.0F, 4.0F, 14.0F, 22.0F, 0.0F, false);
        lowerrib_tilt_4.setTextureOffset(65, 94).addBox(-1.0F, -21.8F, -41.0F, 2.0F, 2.0F, 17.0F, 0.0F, false);
        lowerrib_tilt_4.setTextureOffset(65, 94).addBox(-1.0F, -19.8F, -34.0F, 2.0F, 3.0F, 9.0F, 0.0F, false);
        lowerrib_tilt_4.setTextureOffset(170, 82).addBox(-1.0F, -27.8F, -49.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

        base_fin_3 = new ModelRenderer(this);
        base_fin_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_3.addChild(base_fin_3);
        setRotationAngle(base_fin_3, 0.0F, -0.5236F, 0.0F);
        base_fin_3.setTextureOffset(91, 90).addBox(-1.0F, -48.2F, -23.0F, 2.0F, 28.0F, 4.0F, 0.0F, false);
        base_fin_3.setTextureOffset(60, 92).addBox(-2.0F, -94.2F, -27.0F, 4.0F, 24.0F, 12.0F, 0.0F, false);
        base_fin_3.setTextureOffset(51, 92).addBox(-2.0F, -118.2F, -19.0F, 4.0F, 24.0F, 4.0F, 0.0F, false);
        base_fin_3.setTextureOffset(84, 105).addBox(-2.4F, -13.2F, -32.0F, 5.0F, 13.0F, 6.0F, 0.0F, false);
        base_fin_3.setTextureOffset(63, 98).addBox(-1.4F, -21.2F, -28.0F, 3.0F, 8.0F, 8.0F, 0.0F, false);
        base_fin_3.setTextureOffset(174, 70).addBox(-0.4F, -41.0F, -33.0F, 1.0F, 40.0F, 2.0F, 0.0F, false);

        clawfoot_3 = new ModelRenderer(this);
        clawfoot_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_3.addChild(clawfoot_3);
        setRotationAngle(clawfoot_3, 0.0F, -0.5236F, 0.0F);
        clawfoot_3.setTextureOffset(231, 8).addBox(-1.5F, -38.7F, -39.4F, 3.0F, 4.0F, 3.0F, 0.0F, false);
        clawfoot_3.setTextureOffset(225, 12).addBox(-1.4F, -26.2F, -34.0F, 3.0F, 4.0F, 3.0F, 0.0F, false);
        clawfoot_3.setTextureOffset(226, 10).addBox(-1.4F, -26.2F, -39.4F, 3.0F, 4.0F, 3.0F, 0.0F, false);
        clawfoot_3.setTextureOffset(194, 81).addBox(-0.3F, -25.3F, -36.4F, 1.0F, 2.0F, 3.0F, 0.0F, false);
        clawfoot_3.setTextureOffset(178, 66).addBox(-0.5F, -39.0F, -39.0F, 1.0F, 28.0F, 2.0F, 0.0F, false);
        clawfoot_3.setTextureOffset(224, 16).addBox(-1.5F, -13.2F, -39.4F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        leg_3 = new ModelRenderer(this);
        leg_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_3.addChild(leg_3);
        leg_3.setTextureOffset(54, 101).addBox(-2.4F, -4.2F, -58.0F, 5.0F, 4.0F, 27.0F, 0.0F, false);

        ball_3 = new ModelRenderer(this);
        ball_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_3.addChild(ball_3);
        ball_3.setTextureOffset(29, 102).addBox(-3.4F, -6.0F, -57.0F, 7.0F, 6.0F, 6.0F, 0.0F, false);
        ball_3.setTextureOffset(29, 102).addBox(-3.4F, -6.0F, -47.0F, 7.0F, 6.0F, 2.0F, 0.0F, false);
        ball_3.setTextureOffset(29, 102).addBox(-3.4F, -6.0F, -44.6F, 7.0F, 6.0F, 2.0F, 0.0F, false);
        ball_3.setTextureOffset(29, 102).addBox(-3.4F, -6.0F, -42.1F, 7.0F, 6.0F, 2.0F, 0.0F, false);

        station_4 = new ModelRenderer(this);
        station_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        stations.addChild(station_4);
        setRotationAngle(station_4, 0.0F, 3.1416F, 0.0F);
        

        edge_4 = new ModelRenderer(this);
        edge_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_4.addChild(edge_4);
        edge_4.setTextureOffset(13, 1).addBox(-34.0F, -60.0F, -61.0F, 67.0F, 4.0F, 6.0F, 0.0F, false);
        edge_4.setTextureOffset(59, 96).addBox(-10.0F, -79.0F, -21.0F, 20.0F, 16.0F, 6.0F, 0.0F, false);
        edge_4.setTextureOffset(40, 95).addBox(-10.0F, -71.0F, -15.0F, 20.0F, 8.0F, 6.0F, 0.0F, false);
        edge_4.setTextureOffset(41, 102).addBox(-13.0F, -91.0F, -23.0F, 25.0F, 4.0F, 6.0F, 0.0F, false);
        edge_4.setTextureOffset(38, 93).addBox(-10.0F, -82.0F, -23.0F, 19.0F, 4.0F, 6.0F, 0.0F, false);
        edge_4.setTextureOffset(49, 95).addBox(-9.0F, -93.0F, -20.0F, 20.0F, 14.0F, 4.0F, 0.0F, false);
        edge_4.setTextureOffset(193, 64).addBox(-8.0F, -117.0F, -16.0F, 16.0F, 1.0F, 2.0F, 0.0F, false);
        edge_4.setTextureOffset(80, 110).addBox(-8.0F, -116.0F, -17.0F, 16.0F, 1.0F, 3.0F, 0.0F, false);
        edge_4.setTextureOffset(194, 71).addBox(-8.0F, -115.0F, -16.0F, 16.0F, 1.0F, 2.0F, 0.0F, false);

        cooling_blades_4 = new ModelRenderer(this);
        cooling_blades_4.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_4.addChild(cooling_blades_4);
        cooling_blades_4.setTextureOffset(41, 90).addBox(-10.9F, -45.0F, -22.0F, 4.0F, 28.0F, 5.0F, 0.0F, false);
        cooling_blades_4.setTextureOffset(30, 85).addBox(-2.0F, -45.0F, -22.0F, 4.0F, 28.0F, 5.0F, 0.0F, false);
        cooling_blades_4.setTextureOffset(46, 84).addBox(7.1F, -45.0F, -22.0F, 4.0F, 28.0F, 5.0F, 0.0F, false);

        plasma_coil_4 = new ModelRenderer(this);
        plasma_coil_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        edge_4.addChild(plasma_coil_4);
        plasma_coil_4.setTextureOffset(146, 12).addBox(-10.0F, -81.0F, -19.0F, 20.0F, 2.0F, 2.0F, 0.0F, false);

        belly_4 = new ModelRenderer(this);
        belly_4.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_4.addChild(belly_4);
        belly_4.setTextureOffset(43, 104).addBox(-14.0F, -19.0F, -24.0F, 28.0F, 8.0F, 3.0F, 0.0F, false);
        belly_4.setTextureOffset(41, 88).addBox(-13.0F, -61.0F, -24.0F, 26.0F, 20.0F, 3.0F, 0.0F, false);
        belly_4.setTextureOffset(41, 119).addBox(-14.0F, -2.0F, -28.0F, 28.0F, 3.0F, 12.0F, 0.0F, false);
        belly_4.setTextureOffset(55, 121).addBox(-15.0F, -4.0F, -27.0F, 28.0F, 2.0F, 2.0F, 0.0F, false);
        belly_4.setTextureOffset(42, 107).addBox(-14.0F, -11.0F, -26.0F, 27.0F, 7.0F, 3.0F, 0.0F, false);
        belly_4.setTextureOffset(46, 114).addBox(-14.0F, -59.0F, -26.0F, 27.0F, 10.0F, 3.0F, 0.0F, false);

        bolts4 = new ModelRenderer(this);
        bolts4.setRotationPoint(0.0F, 0.0F, 0.0F);
        belly_4.addChild(bolts4);
        bolts4.setTextureOffset(78, 78).addBox(-11.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts4.setTextureOffset(78, 78).addBox(-6.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts4.setTextureOffset(78, 78).addBox(-1.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts4.setTextureOffset(78, 78).addBox(4.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts4.setTextureOffset(78, 78).addBox(9.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);

        bolts_b4 = new ModelRenderer(this);
        bolts_b4.setRotationPoint(0.0F, 0.0F, 0.0F);
        belly_4.addChild(bolts_b4);
        bolts_b4.setTextureOffset(78, 80).addBox(-11.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b4.setTextureOffset(78, 80).addBox(-6.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b4.setTextureOffset(78, 80).addBox(-1.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b4.setTextureOffset(78, 80).addBox(4.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b4.setTextureOffset(78, 80).addBox(9.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);

        bolts_c4 = new ModelRenderer(this);
        bolts_c4.setRotationPoint(0.0F, 0.0F, 0.0F);
        belly_4.addChild(bolts_c4);
        bolts_c4.setTextureOffset(58, 80).addBox(-9.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_c4.setTextureOffset(58, 80).addBox(-4.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_c4.setTextureOffset(58, 80).addBox(2.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_c4.setTextureOffset(58, 80).addBox(7.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);

        plane_4 = new ModelRenderer(this);
        plane_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_4.addChild(plane_4);
        setRotationAngle(plane_4, 0.5236F, 0.0F, 0.0F);
        plane_4.setTextureOffset(5, 42).addBox(-31.2F, -80.0F, -20.0F, 62.0F, 4.0F, 6.0F, 0.0F, false);
        plane_4.setTextureOffset(5, 42).addBox(-28.2F, -80.0F, -14.0F, 56.0F, 4.0F, 6.0F, 0.0F, false);
        plane_4.setTextureOffset(5, 42).addBox(-26.0F, -80.0F, -8.0F, 51.0F, 4.0F, 6.0F, 0.0F, false);
        plane_4.setTextureOffset(5, 42).addBox(-22.0F, -80.0F, -2.0F, 44.0F, 4.0F, 6.0F, 0.0F, false);
        plane_4.setTextureOffset(2, 82).addBox(-17.0F, -77.0F, -17.0F, 34.0F, 8.0F, 30.0F, 0.0F, false);
        plane_4.setTextureOffset(17, 92).addBox(-11.0F, -69.0F, -11.0F, 22.0F, 4.0F, 24.0F, 0.0F, false);
        plane_4.setTextureOffset(5, 42).addBox(-19.0F, -80.0F, 4.0F, 38.0F, 4.0F, 6.0F, 0.0F, false);
        plane_4.setTextureOffset(5, 42).addBox(-16.0F, -80.0F, 10.0F, 32.0F, 4.0F, 6.0F, 0.0F, false);
        plane_4.setTextureOffset(5, 42).addBox(-14.0F, -80.0F, 16.0F, 27.0F, 4.0F, 6.0F, 0.0F, false);

        rib_4 = new ModelRenderer(this);
        rib_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_4.addChild(rib_4);
        setRotationAngle(rib_4, 0.0F, -0.5236F, 0.0F);
        

        rib_tilt_4 = new ModelRenderer(this);
        rib_tilt_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_4.addChild(rib_tilt_4);
        setRotationAngle(rib_tilt_4, 0.4363F, 0.0F, 0.0F);
        rib_tilt_4.setTextureOffset(20, 76).addBox(-2.0F, -83.4F, -39.5F, 4.0F, 8.0F, 48.0F, 0.0F, false);
        rib_tilt_4.setTextureOffset(20, 75).addBox(-1.0F, -76.0F, -39.0F, 2.0F, 6.0F, 52.0F, 0.0F, false);
        rib_tilt_4.setTextureOffset(65, 94).addBox(-1.0F, -76.0F, -13.0F, 2.0F, 26.0F, 11.0F, 0.0F, false);
        rib_tilt_4.setTextureOffset(65, 94).addBox(-2.0F, -70.0F, -8.0F, 4.0F, 14.0F, 22.0F, 0.0F, false);

        rib_deco_4 = new ModelRenderer(this);
        rib_deco_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_tilt_4.addChild(rib_deco_4);
        rib_deco_4.setTextureOffset(79, 114).addBox(-3.0F, -84.5F, -40.0F, 6.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_4.setTextureOffset(65, 94).addBox(-4.0F, -86.0F, 9.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_4.setTextureOffset(65, 94).addBox(-3.0F, -84.4F, 8.0F, 6.0F, 7.0F, 8.0F, 0.0F, false);

        lowerrib_tilt_5 = new ModelRenderer(this);
        lowerrib_tilt_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_4.addChild(lowerrib_tilt_5);
        setRotationAngle(lowerrib_tilt_5, 0.4363F, 0.0F, 0.0F);
        lowerrib_tilt_5.setTextureOffset(65, 94).addBox(-2.0F, -70.0F, -8.0F, 4.0F, 14.0F, 22.0F, 0.0F, false);

        base_fin_4 = new ModelRenderer(this);
        base_fin_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_4.addChild(base_fin_4);
        setRotationAngle(base_fin_4, 0.0F, -0.5236F, 0.0F);
        base_fin_4.setTextureOffset(91, 90).addBox(-1.0F, -48.2F, -23.0F, 2.0F, 28.0F, 4.0F, 0.0F, false);
        base_fin_4.setTextureOffset(60, 92).addBox(-2.0F, -94.2F, -27.0F, 4.0F, 24.0F, 12.0F, 0.0F, false);
        base_fin_4.setTextureOffset(51, 92).addBox(-2.0F, -118.2F, -19.0F, 4.0F, 24.0F, 4.0F, 0.0F, false);
        base_fin_4.setTextureOffset(71, 115).addBox(-2.4F, -13.2F, -32.0F, 5.0F, 14.0F, 6.0F, 0.0F, false);
        base_fin_4.setTextureOffset(63, 98).addBox(-1.4F, -21.2F, -28.0F, 3.0F, 8.0F, 8.0F, 0.0F, false);

        clawfoot_4 = new ModelRenderer(this);
        clawfoot_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_4.addChild(clawfoot_4);
        setRotationAngle(clawfoot_4, 0.0F, -0.5236F, 0.0F);
        

        leg_4 = new ModelRenderer(this);
        leg_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_4.addChild(leg_4);
        

        ball_4 = new ModelRenderer(this);
        ball_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_4.addChild(ball_4);
        

        station_5 = new ModelRenderer(this);
        station_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        stations.addChild(station_5);
        setRotationAngle(station_5, 0.0F, 2.0944F, 0.0F);
        

        edge_5 = new ModelRenderer(this);
        edge_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_5.addChild(edge_5);
        edge_5.setTextureOffset(13, 1).addBox(-34.0F, -60.0F, -61.0F, 67.0F, 4.0F, 6.0F, 0.0F, false);
        edge_5.setTextureOffset(59, 96).addBox(-10.0F, -79.0F, -21.0F, 20.0F, 16.0F, 6.0F, 0.0F, false);
        edge_5.setTextureOffset(40, 95).addBox(-10.0F, -71.0F, -15.0F, 20.0F, 8.0F, 6.0F, 0.0F, false);
        edge_5.setTextureOffset(41, 102).addBox(-13.0F, -91.0F, -23.0F, 25.0F, 4.0F, 6.0F, 0.0F, false);
        edge_5.setTextureOffset(38, 93).addBox(-10.0F, -82.0F, -23.0F, 19.0F, 4.0F, 6.0F, 0.0F, false);
        edge_5.setTextureOffset(49, 95).addBox(-9.0F, -93.0F, -20.0F, 20.0F, 14.0F, 4.0F, 0.0F, false);
        edge_5.setTextureOffset(193, 64).addBox(-8.0F, -117.0F, -16.0F, 16.0F, 1.0F, 2.0F, 0.0F, false);
        edge_5.setTextureOffset(80, 110).addBox(-8.0F, -116.0F, -17.0F, 16.0F, 1.0F, 3.0F, 0.0F, false);
        edge_5.setTextureOffset(194, 71).addBox(-8.0F, -115.0F, -16.0F, 16.0F, 1.0F, 2.0F, 0.0F, false);

        cooling_blades_5 = new ModelRenderer(this);
        cooling_blades_5.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_5.addChild(cooling_blades_5);
        cooling_blades_5.setTextureOffset(41, 90).addBox(-10.9F, -45.0F, -22.0F, 4.0F, 28.0F, 5.0F, 0.0F, false);
        cooling_blades_5.setTextureOffset(30, 85).addBox(-2.0F, -45.0F, -22.0F, 4.0F, 28.0F, 5.0F, 0.0F, false);
        cooling_blades_5.setTextureOffset(46, 84).addBox(7.1F, -45.0F, -22.0F, 4.0F, 28.0F, 5.0F, 0.0F, false);

        plasma_coil_5 = new ModelRenderer(this);
        plasma_coil_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        edge_5.addChild(plasma_coil_5);
        plasma_coil_5.setTextureOffset(146, 12).addBox(-10.0F, -81.0F, -19.0F, 20.0F, 2.0F, 2.0F, 0.0F, false);

        belly_5 = new ModelRenderer(this);
        belly_5.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_5.addChild(belly_5);
        belly_5.setTextureOffset(54, 102).addBox(-14.0F, -19.0F, -24.0F, 28.0F, 8.0F, 3.0F, 0.0F, false);
        belly_5.setTextureOffset(41, 88).addBox(-13.0F, -61.0F, -24.0F, 26.0F, 20.0F, 3.0F, 0.0F, false);
        belly_5.setTextureOffset(42, 119).addBox(-13.0F, -2.0F, -27.0F, 28.0F, 3.0F, 12.0F, 0.0F, false);
        belly_5.setTextureOffset(46, 119).addBox(-14.0F, -4.0F, -27.0F, 28.0F, 2.0F, 2.0F, 0.0F, false);
        belly_5.setTextureOffset(41, 112).addBox(-14.0F, -11.0F, -26.0F, 27.0F, 7.0F, 3.0F, 0.0F, false);
        belly_5.setTextureOffset(50, 110).addBox(-14.0F, -59.0F, -26.0F, 27.0F, 10.0F, 3.0F, 0.0F, false);

        bolts5 = new ModelRenderer(this);
        bolts5.setRotationPoint(0.0F, 0.0F, 0.0F);
        belly_5.addChild(bolts5);
        bolts5.setTextureOffset(70, 79).addBox(-11.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts5.setTextureOffset(70, 79).addBox(-6.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts5.setTextureOffset(70, 79).addBox(-1.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts5.setTextureOffset(70, 79).addBox(4.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts5.setTextureOffset(70, 79).addBox(9.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);

        bolts_b5 = new ModelRenderer(this);
        bolts_b5.setRotationPoint(0.0F, 0.0F, 0.0F);
        belly_5.addChild(bolts_b5);
        bolts_b5.setTextureOffset(74, 79).addBox(-11.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b5.setTextureOffset(74, 79).addBox(-6.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b5.setTextureOffset(74, 79).addBox(-1.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b5.setTextureOffset(74, 79).addBox(4.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b5.setTextureOffset(74, 79).addBox(9.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);

        bolts_c5 = new ModelRenderer(this);
        bolts_c5.setRotationPoint(0.0F, 0.0F, 0.0F);
        belly_5.addChild(bolts_c5);
        bolts_c5.setTextureOffset(79, 81).addBox(-9.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_c5.setTextureOffset(79, 81).addBox(-4.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_c5.setTextureOffset(79, 81).addBox(2.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_c5.setTextureOffset(79, 81).addBox(7.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);

        plane_5 = new ModelRenderer(this);
        plane_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_5.addChild(plane_5);
        setRotationAngle(plane_5, 0.5236F, 0.0F, 0.0F);
        plane_5.setTextureOffset(5, 42).addBox(-31.2F, -80.0F, -20.0F, 62.0F, 4.0F, 6.0F, 0.0F, false);
        plane_5.setTextureOffset(5, 42).addBox(-28.2F, -80.0F, -14.0F, 56.0F, 4.0F, 6.0F, 0.0F, false);
        plane_5.setTextureOffset(5, 42).addBox(-26.0F, -80.0F, -8.0F, 51.0F, 4.0F, 6.0F, 0.0F, false);
        plane_5.setTextureOffset(5, 42).addBox(-22.0F, -80.0F, -2.0F, 44.0F, 4.0F, 6.0F, 0.0F, false);
        plane_5.setTextureOffset(2, 82).addBox(-17.0F, -77.0F, -17.0F, 34.0F, 8.0F, 30.0F, 0.0F, false);
        plane_5.setTextureOffset(17, 92).addBox(-11.0F, -69.0F, -11.0F, 22.0F, 4.0F, 24.0F, 0.0F, false);
        plane_5.setTextureOffset(5, 42).addBox(-19.0F, -80.0F, 4.0F, 38.0F, 4.0F, 6.0F, 0.0F, false);
        plane_5.setTextureOffset(5, 42).addBox(-16.0F, -80.0F, 10.0F, 32.0F, 4.0F, 6.0F, 0.0F, false);
        plane_5.setTextureOffset(5, 42).addBox(-14.0F, -80.0F, 16.0F, 27.0F, 4.0F, 6.0F, 0.0F, false);

        rib_5 = new ModelRenderer(this);
        rib_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_5.addChild(rib_5);
        setRotationAngle(rib_5, 0.0F, -0.5236F, 0.0F);
        

        rib_tilt_5 = new ModelRenderer(this);
        rib_tilt_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_5.addChild(rib_tilt_5);
        setRotationAngle(rib_tilt_5, 0.4363F, 0.0F, 0.0F);
        rib_tilt_5.setTextureOffset(20, 76).addBox(-2.0F, -83.4F, -39.5F, 4.0F, 8.0F, 48.0F, 0.0F, false);
        rib_tilt_5.setTextureOffset(20, 75).addBox(-1.0F, -76.0F, -39.0F, 2.0F, 6.0F, 52.0F, 0.0F, false);
        rib_tilt_5.setTextureOffset(65, 94).addBox(-1.0F, -76.0F, -13.0F, 2.0F, 26.0F, 11.0F, 0.0F, false);
        rib_tilt_5.setTextureOffset(65, 94).addBox(-2.0F, -70.0F, -8.0F, 4.0F, 14.0F, 22.0F, 0.0F, false);

        rib_deco_5 = new ModelRenderer(this);
        rib_deco_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_tilt_5.addChild(rib_deco_5);
        rib_deco_5.setTextureOffset(77, 114).addBox(-3.0F, -84.5F, -40.0F, 6.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_5.setTextureOffset(65, 94).addBox(-4.0F, -86.0F, 9.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_5.setTextureOffset(65, 94).addBox(-3.0F, -84.4F, 8.0F, 6.0F, 7.0F, 8.0F, 0.0F, false);

        lowerrib_tilt_6 = new ModelRenderer(this);
        lowerrib_tilt_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_5.addChild(lowerrib_tilt_6);
        setRotationAngle(lowerrib_tilt_6, 0.4363F, 0.0F, 0.0F);
        lowerrib_tilt_6.setTextureOffset(205, 68).addBox(-1.0F, -70.0F, -19.0F, 2.0F, 20.0F, 2.0F, 0.0F, false);
        lowerrib_tilt_6.setTextureOffset(65, 94).addBox(-2.0F, -70.0F, -8.0F, 4.0F, 14.0F, 22.0F, 0.0F, false);
        lowerrib_tilt_6.setTextureOffset(65, 94).addBox(-1.0F, -21.8F, -41.0F, 2.0F, 2.0F, 17.0F, 0.0F, false);
        lowerrib_tilt_6.setTextureOffset(65, 94).addBox(-1.0F, -19.8F, -34.0F, 2.0F, 3.0F, 9.0F, 0.0F, false);
        lowerrib_tilt_6.setTextureOffset(170, 82).addBox(-1.0F, -27.8F, -49.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

        base_fin_5 = new ModelRenderer(this);
        base_fin_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_5.addChild(base_fin_5);
        setRotationAngle(base_fin_5, 0.0F, -0.5236F, 0.0F);
        base_fin_5.setTextureOffset(91, 90).addBox(-1.0F, -48.2F, -23.0F, 2.0F, 28.0F, 4.0F, 0.0F, false);
        base_fin_5.setTextureOffset(60, 92).addBox(-2.0F, -94.2F, -27.0F, 4.0F, 24.0F, 12.0F, 0.0F, false);
        base_fin_5.setTextureOffset(51, 92).addBox(-2.0F, -118.2F, -19.0F, 4.0F, 24.0F, 4.0F, 0.0F, false);
        base_fin_5.setTextureOffset(65, 113).addBox(-2.4F, -13.2F, -32.0F, 5.0F, 13.0F, 6.0F, 0.0F, false);
        base_fin_5.setTextureOffset(63, 98).addBox(-1.4F, -21.2F, -28.0F, 3.0F, 8.0F, 8.0F, 0.0F, false);
        base_fin_5.setTextureOffset(174, 70).addBox(-0.4F, -41.0F, -33.0F, 1.0F, 40.0F, 2.0F, 0.0F, false);

        clawfoot_5 = new ModelRenderer(this);
        clawfoot_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_5.addChild(clawfoot_5);
        setRotationAngle(clawfoot_5, 0.0F, -0.5236F, 0.0F);
        clawfoot_5.setTextureOffset(231, 8).addBox(-1.5F, -38.7F, -39.4F, 3.0F, 4.0F, 3.0F, 0.0F, false);
        clawfoot_5.setTextureOffset(225, 12).addBox(-1.4F, -26.2F, -34.0F, 3.0F, 4.0F, 3.0F, 0.0F, false);
        clawfoot_5.setTextureOffset(226, 10).addBox(-1.4F, -26.2F, -39.4F, 3.0F, 4.0F, 3.0F, 0.0F, false);
        clawfoot_5.setTextureOffset(194, 81).addBox(-0.3F, -25.3F, -36.4F, 1.0F, 2.0F, 3.0F, 0.0F, false);
        clawfoot_5.setTextureOffset(178, 66).addBox(-0.5F, -39.0F, -39.0F, 1.0F, 28.0F, 2.0F, 0.0F, false);
        clawfoot_5.setTextureOffset(224, 16).addBox(-1.5F, -13.2F, -39.4F, 3.0F, 4.0F, 3.0F, 0.0F, false);

        leg_5 = new ModelRenderer(this);
        leg_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_5.addChild(leg_5);
        leg_5.setTextureOffset(42, 102).addBox(-2.4F, -4.2F, -58.0F, 5.0F, 4.0F, 27.0F, 0.0F, false);

        ball_5 = new ModelRenderer(this);
        ball_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_5.addChild(ball_5);
        ball_5.setTextureOffset(29, 102).addBox(-3.4F, -6.0F, -57.0F, 7.0F, 6.0F, 6.0F, 0.0F, false);
        ball_5.setTextureOffset(29, 102).addBox(-3.4F, -6.0F, -47.0F, 7.0F, 6.0F, 2.0F, 0.0F, false);
        ball_5.setTextureOffset(29, 102).addBox(-3.4F, -6.0F, -44.6F, 7.0F, 6.0F, 2.0F, 0.0F, false);
        ball_5.setTextureOffset(29, 102).addBox(-3.4F, -6.0F, -42.1F, 7.0F, 6.0F, 2.0F, 0.0F, false);

        station_6 = new ModelRenderer(this);
        station_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        stations.addChild(station_6);
        setRotationAngle(station_6, 0.0F, 1.0472F, 0.0F);
        

        edge_6 = new ModelRenderer(this);
        edge_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_6.addChild(edge_6);
        edge_6.setTextureOffset(13, 1).addBox(-34.0F, -60.0F, -61.0F, 67.0F, 4.0F, 6.0F, 0.0F, false);
        edge_6.setTextureOffset(59, 96).addBox(-10.0F, -79.0F, -21.0F, 20.0F, 16.0F, 6.0F, 0.0F, false);
        edge_6.setTextureOffset(40, 95).addBox(-10.0F, -71.0F, -15.0F, 20.0F, 8.0F, 6.0F, 0.0F, false);
        edge_6.setTextureOffset(41, 102).addBox(-13.0F, -91.0F, -23.0F, 25.0F, 4.0F, 6.0F, 0.0F, false);
        edge_6.setTextureOffset(38, 93).addBox(-10.0F, -82.0F, -23.0F, 19.0F, 4.0F, 6.0F, 0.0F, false);
        edge_6.setTextureOffset(49, 95).addBox(-9.0F, -93.0F, -20.0F, 20.0F, 14.0F, 4.0F, 0.0F, false);
        edge_6.setTextureOffset(193, 64).addBox(-8.0F, -117.0F, -16.0F, 16.0F, 1.0F, 2.0F, 0.0F, false);
        edge_6.setTextureOffset(80, 110).addBox(-8.0F, -116.0F, -17.0F, 16.0F, 1.0F, 3.0F, 0.0F, false);
        edge_6.setTextureOffset(194, 71).addBox(-8.0F, -115.0F, -16.0F, 16.0F, 1.0F, 2.0F, 0.0F, false);

        cooling_blades_6 = new ModelRenderer(this);
        cooling_blades_6.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_6.addChild(cooling_blades_6);
        cooling_blades_6.setTextureOffset(41, 90).addBox(-10.9F, -45.0F, -22.0F, 4.0F, 28.0F, 5.0F, 0.0F, false);
        cooling_blades_6.setTextureOffset(30, 85).addBox(-2.0F, -45.0F, -22.0F, 4.0F, 28.0F, 5.0F, 0.0F, false);
        cooling_blades_6.setTextureOffset(46, 84).addBox(7.1F, -45.0F, -22.0F, 4.0F, 28.0F, 5.0F, 0.0F, false);

        plasma_coil_6 = new ModelRenderer(this);
        plasma_coil_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        edge_6.addChild(plasma_coil_6);
        plasma_coil_6.setTextureOffset(146, 12).addBox(-10.0F, -81.0F, -19.0F, 20.0F, 2.0F, 2.0F, 0.0F, false);

        belly_6 = new ModelRenderer(this);
        belly_6.setRotationPoint(0.0F, -1.0F, 0.0F);
        edge_6.addChild(belly_6);
        belly_6.setTextureOffset(45, 107).addBox(-14.0F, -19.0F, -24.0F, 28.0F, 8.0F, 3.0F, 0.0F, false);
        belly_6.setTextureOffset(41, 88).addBox(-13.0F, -61.0F, -24.0F, 26.0F, 20.0F, 3.0F, 0.0F, false);
        belly_6.setTextureOffset(45, 119).addBox(-14.0F, -2.0F, -28.0F, 28.0F, 3.0F, 12.0F, 0.0F, false);
        belly_6.setTextureOffset(48, 120).addBox(-14.0F, -4.0F, -27.0F, 28.0F, 2.0F, 2.0F, 0.0F, false);
        belly_6.setTextureOffset(37, 116).addBox(-14.0F, -11.0F, -26.0F, 27.0F, 7.0F, 3.0F, 0.0F, false);
        belly_6.setTextureOffset(45, 107).addBox(-14.0F, -59.0F, -26.0F, 27.0F, 10.0F, 3.0F, 0.0F, false);

        bolts6 = new ModelRenderer(this);
        bolts6.setRotationPoint(0.0F, 0.0F, 0.0F);
        belly_6.addChild(bolts6);
        bolts6.setTextureOffset(85, 81).addBox(-11.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts6.setTextureOffset(85, 81).addBox(-6.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts6.setTextureOffset(85, 81).addBox(-1.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts6.setTextureOffset(85, 81).addBox(4.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts6.setTextureOffset(85, 81).addBox(9.0F, -16.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);

        bolts_b6 = new ModelRenderer(this);
        bolts_b6.setRotationPoint(0.0F, 0.0F, 0.0F);
        belly_6.addChild(bolts_b6);
        bolts_b6.setTextureOffset(77, 82).addBox(-11.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b6.setTextureOffset(77, 82).addBox(-6.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b6.setTextureOffset(77, 82).addBox(-1.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b6.setTextureOffset(77, 82).addBox(4.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_b6.setTextureOffset(77, 82).addBox(9.0F, -45.0F, -25.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);

        bolts_c6 = new ModelRenderer(this);
        bolts_c6.setRotationPoint(0.0F, 0.0F, 0.0F);
        belly_6.addChild(bolts_c6);
        bolts_c6.setTextureOffset(78, 81).addBox(-9.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_c6.setTextureOffset(78, 81).addBox(-4.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_c6.setTextureOffset(78, 81).addBox(2.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);
        bolts_c6.setTextureOffset(78, 81).addBox(7.0F, -89.0F, -24.0F, 2.0F, 2.0F, 3.0F, 0.0F, false);

        plane_6 = new ModelRenderer(this);
        plane_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_6.addChild(plane_6);
        setRotationAngle(plane_6, 0.5236F, 0.0F, 0.0F);
        plane_6.setTextureOffset(5, 42).addBox(-31.2F, -80.0F, -20.0F, 62.0F, 4.0F, 6.0F, 0.0F, false);
        plane_6.setTextureOffset(5, 42).addBox(-28.2F, -80.0F, -14.0F, 56.0F, 4.0F, 6.0F, 0.0F, false);
        plane_6.setTextureOffset(5, 42).addBox(-26.0F, -80.0F, -8.0F, 51.0F, 4.0F, 6.0F, 0.0F, false);
        plane_6.setTextureOffset(5, 42).addBox(-22.0F, -80.0F, -2.0F, 44.0F, 4.0F, 6.0F, 0.0F, false);
        plane_6.setTextureOffset(2, 82).addBox(-17.0F, -77.0F, -17.0F, 34.0F, 8.0F, 30.0F, 0.0F, false);
        plane_6.setTextureOffset(17, 92).addBox(-11.0F, -69.0F, -11.0F, 22.0F, 4.0F, 24.0F, 0.0F, false);
        plane_6.setTextureOffset(5, 42).addBox(-19.0F, -80.0F, 4.0F, 38.0F, 4.0F, 6.0F, 0.0F, false);
        plane_6.setTextureOffset(5, 42).addBox(-16.0F, -80.0F, 10.0F, 32.0F, 4.0F, 6.0F, 0.0F, false);
        plane_6.setTextureOffset(5, 42).addBox(-14.0F, -80.0F, 16.0F, 27.0F, 4.0F, 6.0F, 0.0F, false);

        rib_6 = new ModelRenderer(this);
        rib_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_6.addChild(rib_6);
        setRotationAngle(rib_6, 0.0F, -0.5236F, 0.0F);
        

        rib_tilt_6 = new ModelRenderer(this);
        rib_tilt_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_6.addChild(rib_tilt_6);
        setRotationAngle(rib_tilt_6, 0.4363F, 0.0F, 0.0F);
        rib_tilt_6.setTextureOffset(20, 76).addBox(-2.0F, -83.4F, -39.5F, 4.0F, 8.0F, 48.0F, 0.0F, false);
        rib_tilt_6.setTextureOffset(20, 75).addBox(-1.0F, -76.0F, -39.0F, 2.0F, 6.0F, 52.0F, 0.0F, false);
        rib_tilt_6.setTextureOffset(65, 94).addBox(-1.0F, -76.0F, -13.0F, 2.0F, 26.0F, 11.0F, 0.0F, false);
        rib_tilt_6.setTextureOffset(65, 94).addBox(-2.0F, -70.0F, -8.0F, 4.0F, 14.0F, 22.0F, 0.0F, false);

        rib_deco_6 = new ModelRenderer(this);
        rib_deco_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_tilt_6.addChild(rib_deco_6);
        rib_deco_6.setTextureOffset(80, 113).addBox(-3.0F, -84.5F, -40.0F, 6.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_6.setTextureOffset(65, 94).addBox(-4.0F, -86.0F, 9.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        rib_deco_6.setTextureOffset(65, 94).addBox(-3.0F, -84.4F, 8.0F, 6.0F, 7.0F, 8.0F, 0.0F, false);

        lowerrib_tilt_7 = new ModelRenderer(this);
        lowerrib_tilt_7.setRotationPoint(0.0F, 0.0F, 0.0F);
        rib_6.addChild(lowerrib_tilt_7);
        setRotationAngle(lowerrib_tilt_7, 0.4363F, 0.0F, 0.0F);
        lowerrib_tilt_7.setTextureOffset(65, 94).addBox(-2.0F, -70.0F, -8.0F, 4.0F, 14.0F, 22.0F, 0.0F, false);

        base_fin_6 = new ModelRenderer(this);
        base_fin_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_6.addChild(base_fin_6);
        setRotationAngle(base_fin_6, 0.0F, -0.5236F, 0.0F);
        base_fin_6.setTextureOffset(91, 90).addBox(-1.0F, -48.2F, -23.0F, 2.0F, 28.0F, 4.0F, 0.0F, false);
        base_fin_6.setTextureOffset(60, 92).addBox(-2.0F, -94.2F, -27.0F, 4.0F, 24.0F, 12.0F, 0.0F, false);
        base_fin_6.setTextureOffset(51, 92).addBox(-2.0F, -118.2F, -19.0F, 4.0F, 24.0F, 4.0F, 0.0F, false);
        base_fin_6.setTextureOffset(65, 114).addBox(-2.4F, -13.2F, -32.0F, 5.0F, 14.0F, 6.0F, 0.0F, false);
        base_fin_6.setTextureOffset(63, 98).addBox(-1.4F, -21.2F, -28.0F, 3.0F, 8.0F, 8.0F, 0.0F, false);

        clawfoot_6 = new ModelRenderer(this);
        clawfoot_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_6.addChild(clawfoot_6);
        setRotationAngle(clawfoot_6, 0.0F, -0.5236F, 0.0F);
        

        leg_6 = new ModelRenderer(this);
        leg_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_6.addChild(leg_6);
        

        ball_6 = new ModelRenderer(this);
        ball_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        clawfoot_6.addChild(ball_6);
        

        controls = new ModelRenderer(this);
        controls.setRotationPoint(0.0F, 24.0F, 0.0F);
        

        side1 = new ModelRenderer(this);
        side1.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(side1);
        

        dummy_barometer = new ModelRenderer(this);
        dummy_barometer.setRotationPoint(0.0F, 0.0F, 0.0F);
        side1.addChild(dummy_barometer);
        dummy_barometer.setTextureOffset(182, 152).addBox(-5.5F, -80.0F, -32.4F, 4.0F, 1.0F, 4.0F, 0.0F, false);
        dummy_barometer.setTextureOffset(182, 152).addBox(-5.5F, -88.0F, -32.4F, 4.0F, 1.0F, 4.0F, 0.0F, false);

        glow_barometer = new LightModelRenderer(this);
        glow_barometer.setRotationPoint(0.0F, 0.0F, 0.0F);
        dummy_barometer.addChild(glow_barometer);
        glow_barometer.setTextureOffset(24, 153).addBox(-5.0F, -90.0F, -32.0F, 3.0F, 16.0F, 3.0F, 0.0F, false);
        glow_barometer.setTextureOffset(25, 164).addBox(6.5F, -83.5F, -31.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        glow_barometer.setTextureOffset(33, 161).addBox(3.5F, -87.5F, -31.0F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        glow_barometer.setTextureOffset(23, 158).addBox(3.0F, -87.1F, -31.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        glow_barometer.setTextureOffset(23, 161).addBox(-4.6F, -91.0F, -31.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        glow_barometer.setTextureOffset(28, 157).addBox(-4.1F, -93.4F, -31.0F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        glow_barometer.setTextureOffset(22, 157).addBox(1.5F, -82.0F, -33.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
        glow_barometer.setTextureOffset(16, 167).addBox(3.5F, -83.5F, -31.0F, 3.0F, 1.0F, 1.0F, 0.0F, false);
        glow_barometer.setTextureOffset(206, 216).addBox(2.5F, -78.0F, -32.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        glow_barometer.setTextureOffset(203, 201).addBox(1.5F, -80.0F, -33.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
        glow_barometer.setTextureOffset(205, 212).addBox(5.5F, -79.5F, -31.0F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        glow_barometer.setTextureOffset(208, 207).addBox(6.5F, -81.5F, -31.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);

        station_tilt_3 = new ModelRenderer(this);
        station_tilt_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        side1.addChild(station_tilt_3);
        setRotationAngle(station_tilt_3, -1.0472F, 0.0F, 0.0F);
        station_tilt_3.setTextureOffset(182, 152).addBox(-7.0F, -14.0F, -81.0F, 14.0F, 5.0F, 4.0F, 0.0F, false);
        station_tilt_3.setTextureOffset(182, 152).addBox(-7.6F, -14.6F, -80.6F, 15.0F, 6.0F, 4.0F, 0.0F, false);
        station_tilt_3.setTextureOffset(182, 152).addBox(-21.0F, 5.0F, -80.4F, 12.0F, 12.0F, 4.0F, 0.0F, false);
        station_tilt_3.setTextureOffset(182, 152).addBox(-19.4F, 13.4F, -81.2F, 9.0F, 3.0F, 4.0F, 0.0F, false);
        station_tilt_3.setTextureOffset(182, 152).addBox(-20.4F, 7.4F, -80.8F, 11.0F, 5.0F, 4.0F, 0.0F, false);
        station_tilt_3.setTextureOffset(182, 152).addBox(-19.4F, 5.4F, -80.8F, 9.0F, 2.0F, 4.0F, 0.0F, false);
        station_tilt_3.setTextureOffset(182, 152).addBox(10.0F, 5.0F, -81.0F, 12.0F, 12.0F, 4.0F, 0.0F, false);
        station_tilt_3.setTextureOffset(182, 152).addBox(9.4F, 4.4F, -80.5F, 13.0F, 13.0F, 4.0F, 0.0F, false);
        station_tilt_3.setTextureOffset(69, 152).addBox(-7.0F, -4.0F, -81.0F, 14.0F, 21.0F, 4.0F, 0.0F, false);

        throttle = new ModelRenderer(this);
        throttle.setRotationPoint(-15.3F, -5.0F, -3.0F);
        station_tilt_3.addChild(throttle);
        

        throttle_lever_rotate_x = new ModelRenderer(this);
        throttle_lever_rotate_x.setRotationPoint(9.5F, 12.0F, -74.5F);
        throttle.addChild(throttle_lever_rotate_x);
        throttle_lever_rotate_x.setTextureOffset(192, 91).addBox(0.3F, -1.5F, -10.5F, 3.0F, 3.0F, 12.0F, 0.0F, false);
        throttle_lever_rotate_x.setTextureOffset(142, 156).addBox(1.5F, -0.8F, -12.5F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        throttle_lever_rotate_x.setTextureOffset(141, 161).addBox(1.5F, -0.8F, -13.7F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        throttle_lever_rotate_x.setTextureOffset(137, 157).addBox(1.5F, -0.8F, -14.9F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        throttle_lever_rotate_x.setTextureOffset(192, 91).addBox(2.0F, -0.2F, -15.1F, 1.0F, 1.0F, 6.0F, 0.0F, false);

        throttle_wheel = new ModelRenderer(this);
        throttle_wheel.setRotationPoint(0.0F, 0.0F, 0.0F);
        throttle.addChild(throttle_wheel);
        throttle_wheel.setTextureOffset(87, 161).addBox(10.0F, 8.0F, -84.0F, 4.0F, 8.0F, 1.0F, 0.0F, false);
        throttle_wheel.setTextureOffset(16, 154).addBox(10.5F, 8.0F, -83.0F, 3.0F, 8.0F, 6.0F, 0.0F, false);
        throttle_wheel.setTextureOffset(16, 154).addBox(10.5F, 3.0F, -79.0F, 3.0F, 8.0F, 6.0F, 0.0F, false);
        throttle_wheel.setTextureOffset(32, 106).addBox(10.0F, 2.3F, -78.4F, 4.0F, 1.0F, 8.0F, 0.0F, false);

        bone2 = new ModelRenderer(this);
        bone2.setRotationPoint(12.0F, 12.0F, -80.0F);
        throttle_wheel.addChild(bone2);
        setRotationAngle(bone2, 0.7854F, 0.0F, 0.0F);
        bone2.setTextureOffset(16, 154).addBox(-1.5F, 1.0F, -5.0F, 3.0F, 8.0F, 6.0F, 0.0F, false);
        bone2.setTextureOffset(16, 154).addBox(-1.5F, -5.0F, 1.0F, 3.0F, 8.0F, 6.0F, 0.0F, false);
        bone2.setTextureOffset(81, 164).addBox(-2.0F, 0.0F, -5.7F, 4.0F, 8.0F, 1.0F, 0.0F, false);
        bone2.setTextureOffset(75, 157).addBox(-2.0F, -5.7F, 0.0F, 4.0F, 1.0F, 8.0F, 0.0F, false);

        spacer = new ModelRenderer(this);
        spacer.setRotationPoint(-22.0F, -5.0F, -3.0F);
        station_tilt_3.addChild(spacer);
        

        spacer_wheel1 = new ModelRenderer(this);
        spacer_wheel1.setRotationPoint(9.0F, 0.0F, 0.0F);
        spacer.addChild(spacer_wheel1);
        spacer_wheel1.setTextureOffset(221, 12).addBox(12.0F, 8.0F, -84.0F, 2.0F, 8.0F, 1.0F, 0.0F, false);
        spacer_wheel1.setTextureOffset(221, 12).addBox(12.0F, 2.3F, -78.4F, 2.0F, 1.0F, 8.0F, 0.0F, false);

        spacer_tilt1 = new ModelRenderer(this);
        spacer_tilt1.setRotationPoint(12.0F, 12.0F, -80.0F);
        spacer_wheel1.addChild(spacer_tilt1);
        setRotationAngle(spacer_tilt1, 0.7854F, 0.0F, 0.0F);
        spacer_tilt1.setTextureOffset(221, 12).addBox(0.0F, 0.0F, -5.7F, 2.0F, 8.0F, 1.0F, 0.0F, false);
        spacer_tilt1.setTextureOffset(221, 12).addBox(0.0F, -5.7F, 0.0F, 2.0F, 1.0F, 8.0F, 0.0F, false);

        handbreak = new ModelRenderer(this);
        handbreak.setRotationPoint(-8.7F, -5.0F, -3.0F);
        station_tilt_3.addChild(handbreak);
        

        handbreak_lever2_rotate_x = new ModelRenderer(this);
        handbreak_lever2_rotate_x.setRotationPoint(9.5F, 12.0F, -74.5F);
        handbreak.addChild(handbreak_lever2_rotate_x);
        handbreak_lever2_rotate_x.setTextureOffset(131, 166).addBox(1.6F, -0.8F, -12.5F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        handbreak_lever2_rotate_x.setTextureOffset(144, 155).addBox(1.6F, -0.8F, -13.7F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        handbreak_lever2_rotate_x.setTextureOffset(144, 147).addBox(1.6F, -0.8F, -14.9F, 2.0F, 2.0F, 1.0F, 0.0F, false);
        handbreak_lever2_rotate_x.setTextureOffset(200, 96).addBox(2.1F, -0.2F, -15.1F, 1.0F, 1.0F, 6.0F, 0.0F, false);
        handbreak_lever2_rotate_x.setTextureOffset(200, 96).addBox(1.8F, -1.5F, -10.5F, 3.0F, 3.0F, 12.0F, 0.0F, false);

        handbreak_wheel2 = new ModelRenderer(this);
        handbreak_wheel2.setRotationPoint(0.0F, 0.0F, 0.0F);
        handbreak.addChild(handbreak_wheel2);
        handbreak_wheel2.setTextureOffset(83, 149).addBox(10.0F, 8.0F, -84.0F, 4.0F, 8.0F, 1.0F, 0.0F, false);
        handbreak_wheel2.setTextureOffset(17, 161).addBox(10.5F, 8.0F, -83.0F, 3.0F, 8.0F, 6.0F, 0.0F, false);
        handbreak_wheel2.setTextureOffset(23, 157).addBox(10.5F, 3.0F, -79.0F, 3.0F, 8.0F, 6.0F, 0.0F, false);
        handbreak_wheel2.setTextureOffset(96, 97).addBox(10.0F, 2.3F, -78.4F, 4.0F, 1.0F, 8.0F, 0.0F, false);

        handbreak_tilt2 = new ModelRenderer(this);
        handbreak_tilt2.setRotationPoint(12.0F, 12.0F, -80.0F);
        handbreak_wheel2.addChild(handbreak_tilt2);
        setRotationAngle(handbreak_tilt2, 0.7854F, 0.0F, 0.0F);
        handbreak_tilt2.setTextureOffset(32, 155).addBox(-1.5F, 1.0F, -5.0F, 3.0F, 8.0F, 6.0F, 0.0F, false);
        handbreak_tilt2.setTextureOffset(20, 147).addBox(-1.5F, -5.0F, 1.0F, 3.0F, 8.0F, 6.0F, 0.0F, false);
        handbreak_tilt2.setTextureOffset(86, 166).addBox(-2.0F, 0.0F, -5.7F, 4.0F, 8.0F, 1.0F, 0.0F, false);
        handbreak_tilt2.setTextureOffset(75, 156).addBox(-2.0F, -5.7F, 0.0F, 4.0F, 1.0F, 8.0F, 0.0F, false);

        refueler = new ModelRenderer(this);
        refueler.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_tilt_3.addChild(refueler);
        refueler.setTextureOffset(134, 161).addBox(-20.0F, 11.0F, -81.6F, 10.0F, 1.0F, 4.0F, 0.0F, false);
        refueler.setTextureOffset(134, 161).addBox(-19.0F, 6.0F, -81.6F, 8.0F, 1.0F, 4.0F, 0.0F, false);
        refueler.setTextureOffset(134, 161).addBox(-19.0F, 7.0F, -81.6F, 1.0F, 2.0F, 4.0F, 0.0F, false);
        refueler.setTextureOffset(134, 161).addBox(-12.0F, 7.0F, -81.6F, 1.0F, 2.0F, 4.0F, 0.0F, false);
        refueler.setTextureOffset(134, 161).addBox(-11.0F, 8.0F, -81.6F, 1.0F, 3.0F, 4.0F, 0.0F, false);
        refueler.setTextureOffset(134, 161).addBox(-20.0F, 8.0F, -81.6F, 1.0F, 3.0F, 4.0F, 0.0F, false);

        refuel_needle_rotate_z = new ModelRenderer(this);
        refuel_needle_rotate_z.setRotationPoint(-15.0F, 20.0F, -79.5375F);
        refueler.addChild(refuel_needle_rotate_z);
        refuel_needle_rotate_z.setTextureOffset(105, 230).addBox(-0.5F, -11.5F, -2.0F, 1.0F, 3.0F, 4.0F, 0.0F, false);

        glow_refuel = new LightModelRenderer(this);
        glow_refuel.setRotationPoint(0.0F, 0.0F, 0.0F);
        refueler.addChild(glow_refuel);
        glow_refuel.setTextureOffset(97, 233).addBox(-19.0F, 14.0F, -82.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        glow_refuel.setTextureOffset(92, 230).addBox(-16.0F, 14.0F, -82.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        glow_refuel.setTextureOffset(101, 236).addBox(-13.0F, 14.0F, -82.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        glow_refuel.setTextureOffset(14, 164).addBox(-19.0F, 9.0F, -81.2F, 8.0F, 2.0F, 4.0F, 0.0F, false);
        glow_refuel.setTextureOffset(16, 164).addBox(-18.0F, 7.0F, -81.2F, 6.0F, 2.0F, 4.0F, 0.0F, false);

        dummy_dial_c1 = new ModelRenderer(this);
        dummy_dial_c1.setRotationPoint(-11.05F, -17.1F, -79.0F);
        station_tilt_3.addChild(dummy_dial_c1);
        setRotationAngle(dummy_dial_c1, 0.0F, 0.0F, 0.7854F);
        dummy_dial_c1.setTextureOffset(182, 152).addBox(3.45F, 4.9F, -1.4F, 7.0F, 7.0F, 4.0F, 0.0F, false);
        dummy_dial_c1.setTextureOffset(208, 64).addBox(4.55F, 6.0F, -2.4F, 5.0F, 5.0F, 4.0F, 0.0F, false);
        dummy_dial_c1.setTextureOffset(136, 153).addBox(5.05F, 6.6F, -3.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        glow_c1 = new LightModelRenderer(this);
        glow_c1.setRotationPoint(11.05F, 17.1F, 79.0F);
        dummy_dial_c1.addChild(glow_c1);
        glow_c1.setTextureOffset(22, 158).addBox(-5.4F, -10.0F, -82.6F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_dial_c2 = new ModelRenderer(this);
        dummy_dial_c2.setRotationPoint(-11.05F, -17.1F, -79.0F);
        station_tilt_3.addChild(dummy_dial_c2);
        setRotationAngle(dummy_dial_c2, 0.0F, 0.0F, 0.7854F);
        dummy_dial_c2.setTextureOffset(182, 152).addBox(20.45F, -12.1F, -1.4F, 7.0F, 7.0F, 4.0F, 0.0F, false);
        dummy_dial_c2.setTextureOffset(200, 68).addBox(21.55F, -11.0F, -2.4F, 5.0F, 5.0F, 4.0F, 0.0F, false);
        dummy_dial_c2.setTextureOffset(141, 160).addBox(22.05F, -10.4F, -3.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        glow_c2 = new LightModelRenderer(this);
        glow_c2.setRotationPoint(11.05F, 17.1F, 79.0F);
        dummy_dial_c2.addChild(glow_c2);
        glow_c2.setTextureOffset(23, 160).addBox(11.6F, -27.0F, -82.6F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_button_C1 = new ModelRenderer(this);
        dummy_button_C1.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_tilt_3.addChild(dummy_button_C1);
        dummy_button_C1.setTextureOffset(196, 87).addBox(16.4F, 5.6F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        dummy_button_C1.setTextureOffset(89, 152).addBox(11.0F, 6.0F, -81.3F, 4.0F, 10.0F, 4.0F, 0.0F, false);
        dummy_button_C1.setTextureOffset(196, 87).addBox(19.3F, 5.6F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        dummy_button_C1.setTextureOffset(196, 87).addBox(19.3F, 8.4F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        dummy_button_C1.setTextureOffset(196, 87).addBox(16.4F, 8.4F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        dummy_button_C1.setTextureOffset(196, 87).addBox(19.3F, 11.3F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        dummy_button_C1.setTextureOffset(196, 87).addBox(16.4F, 11.3F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        dummy_button_C1.setTextureOffset(196, 87).addBox(19.3F, 14.3F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        dummy_button_C1.setTextureOffset(196, 87).addBox(16.4F, 14.3F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);

        side2 = new ModelRenderer(this);
        side2.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(side2);
        setRotationAngle(side2, 0.0F, -1.0472F, 0.0F);
        

        station_tilt_2 = new ModelRenderer(this);
        station_tilt_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        side2.addChild(station_tilt_2);
        setRotationAngle(station_tilt_2, -1.0472F, 0.0F, 0.0F);
        station_tilt_2.setTextureOffset(182, 152).addBox(-4.0F, 2.0F, -81.0F, 8.0F, 12.0F, 4.0F, 0.0F, false);
        station_tilt_2.setTextureOffset(182, 152).addBox(-19.0F, 2.0F, -81.0F, 12.0F, 12.0F, 4.0F, 0.0F, false);
        station_tilt_2.setTextureOffset(182, 152).addBox(7.0F, 2.0F, -81.0F, 12.0F, 12.0F, 4.0F, 0.0F, false);

        facing_dial = new ModelRenderer(this);
        facing_dial.setRotationPoint(-13.0F, 8.0F, -80.0F);
        station_tilt_2.addChild(facing_dial);
        setRotationAngle(facing_dial, 0.0F, 0.0F, 0.7854F);
        facing_dial.setTextureOffset(73, 157).addBox(-5.0F, -5.0F, -2.0F, 10.0F, 10.0F, 4.0F, 0.0F, false);
        facing_dial.setTextureOffset(182, 152).addBox(-1.0F, -1.0F, -3.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        facing_dial.setTextureOffset(191, 103).addBox(-4.0F, 3.0F, -3.0F, 7.0F, 1.0F, 4.0F, 0.0F, false);
        facing_dial.setTextureOffset(195, 90).addBox(-3.0F, -4.0F, -3.0F, 7.0F, 1.0F, 4.0F, 0.0F, false);
        facing_dial.setTextureOffset(214, 92).addBox(3.0F, -3.0F, -3.0F, 1.0F, 7.0F, 4.0F, 0.0F, false);
        facing_dial.setTextureOffset(198, 91).addBox(-4.0F, -4.0F, -3.0F, 1.0F, 7.0F, 4.0F, 0.0F, false);

        faceing_needle_rotate_z = new ModelRenderer(this);
        faceing_needle_rotate_z.setRotationPoint(0.0F, 0.0F, -2.625F);
        facing_dial.addChild(faceing_needle_rotate_z);
        setRotationAngle(faceing_needle_rotate_z, 0.0F, 0.0F, 0.7854F);
        faceing_needle_rotate_z.setTextureOffset(144, 155).addBox(-2.8018F, -0.5F, -0.125F, 3.0F, 1.0F, 4.0F, 0.0F, false);

        glow_dial = new LightModelRenderer(this);
        glow_dial.setRotationPoint(13.0F, -8.0F, 80.0F);
        facing_dial.addChild(glow_dial);
        glow_dial.setTextureOffset(21, 159).addBox(-16.0F, 5.0F, -82.6F, 6.0F, 6.0F, 4.0F, 0.0F, false);

        landing_type = new ModelRenderer(this);
        landing_type.setRotationPoint(0.6F, 0.0F, 0.7F);
        station_tilt_2.addChild(landing_type);
        landing_type.setTextureOffset(133, 152).addBox(8.0F, 4.0F, -82.3F, 9.0F, 8.0F, 4.0F, 0.0F, false);

        rotate_bar_x = new ModelRenderer(this);
        rotate_bar_x.setRotationPoint(12.5F, 6.5F, 13.6F);
        landing_type.addChild(rotate_bar_x);
        rotate_bar_x.setTextureOffset(73, 160).addBox(-3.5F, -0.5F, -98.0F, 7.0F, 1.0F, 4.0F, 0.0F, false);

        slider_plate = new ModelRenderer(this);
        slider_plate.setRotationPoint(0.0F, 0.0F, 0.0F);
        landing_type.addChild(slider_plate);
        slider_plate.setTextureOffset(183, 82).addBox(8.5F, 10.0F, -83.3F, 8.0F, 1.0F, 4.0F, 0.0F, false);
        slider_plate.setTextureOffset(199, 88).addBox(8.5F, 5.0F, -83.3F, 8.0F, 1.0F, 4.0F, 0.0F, false);
        slider_plate.setTextureOffset(184, 89).addBox(8.5F, 6.0F, -83.3F, 1.0F, 4.0F, 4.0F, 0.0F, false);
        slider_plate.setTextureOffset(216, 80).addBox(15.5F, 6.0F, -83.3F, 1.0F, 4.0F, 4.0F, 0.0F, false);
        slider_plate.setTextureOffset(200, 91).addBox(10.5F, 6.0F, -83.3F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        up = new ModelRenderer(this);
        up.setRotationPoint(-13.0F, 8.0F, -80.0F);
        landing_type.addChild(up);
        setRotationAngle(up, 0.0F, 0.0F, 0.7854F);
        up.setTextureOffset(203, 77).addBox(13.0F, -23.0F, -2.0F, 5.0F, 5.0F, 4.0F, 0.0F, false);

        up2 = new ModelRenderer(this);
        up2.setRotationPoint(-13.0F, 8.0F, -80.0F);
        landing_type.addChild(up2);
        setRotationAngle(up2, 0.0F, 0.0F, 0.7854F);
        up2.setTextureOffset(205, 95).addBox(18.0F, -18.0F, -2.0F, 5.0F, 5.0F, 4.0F, 0.0F, false);

        dummy_button_b1 = new ModelRenderer(this);
        dummy_button_b1.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_tilt_2.addChild(dummy_button_b1);
        

        glow_b1 = new LightModelRenderer(this);
        glow_b1.setRotationPoint(0.0F, 0.0F, 0.0F);
        dummy_button_b1.addChild(glow_b1);
        glow_b1.setTextureOffset(188, 12).addBox(-3.5F, 2.7F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        glow_b1.setTextureOffset(188, 12).addBox(-1.0F, 2.7F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        glow_b1.setTextureOffset(188, 12).addBox(1.5F, 2.7F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        glow_b1.setTextureOffset(188, 12).addBox(1.5F, 5.5F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        glow_b1.setTextureOffset(188, 12).addBox(-3.5F, 5.5F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        glow_b1.setTextureOffset(188, 12).addBox(-1.0F, 5.5F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        glow_b1.setTextureOffset(188, 12).addBox(1.5F, 8.4F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        glow_b1.setTextureOffset(188, 12).addBox(-3.5F, 8.4F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        glow_b1.setTextureOffset(188, 12).addBox(-1.0F, 8.4F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        glow_b1.setTextureOffset(188, 12).addBox(1.5F, 11.4F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        glow_b1.setTextureOffset(188, 12).addBox(-3.5F, 11.4F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        glow_b1.setTextureOffset(188, 12).addBox(-1.0F, 11.4F, -81.3F, 2.0F, 2.0F, 4.0F, 0.0F, false);

        randomizer = new ModelRenderer(this);
        randomizer.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_tilt_2.addChild(randomizer);
        randomizer.setTextureOffset(133, 155).addBox(-6.6F, -15.0F, -80.4F, 12.0F, 9.0F, 5.0F, 0.0F, false);
        randomizer.setTextureOffset(182, 152).addBox(-7.6F, -15.0F, -81.4F, 1.0F, 9.0F, 5.0F, 0.0F, false);
        randomizer.setTextureOffset(182, 152).addBox(5.4F, -15.0F, -81.4F, 1.0F, 9.0F, 5.0F, 0.0F, false);
        randomizer.setTextureOffset(182, 152).addBox(-7.6F, -6.0F, -81.4F, 14.0F, 1.0F, 5.0F, 0.0F, false);
        randomizer.setTextureOffset(182, 152).addBox(-7.6F, -16.0F, -81.4F, 14.0F, 1.0F, 5.0F, 0.0F, false);
        randomizer.setTextureOffset(205, 80).addBox(-2.5F, -5.7F, -82.1F, 4.0F, 1.0F, 6.0F, 0.0F, false);
        randomizer.setTextureOffset(202, 92).addBox(-2.5F, -16.7F, -82.1F, 4.0F, 1.0F, 6.0F, 0.0F, false);

        rotate_y = new ModelRenderer(this);
        rotate_y.setRotationPoint(-0.5F, -6.7F, -81.1F);
        randomizer.addChild(rotate_y);
        rotate_y.setTextureOffset(11, 225).addBox(-4.4F, -2.7F, -4.1F, 2.0F, 3.0F, 3.0F, 0.0F, false);
        rotate_y.setTextureOffset(4, 222).addBox(-1.4F, -6.3F, -4.6F, 5.0F, 3.0F, 3.0F, 0.0F, false);
        rotate_y.setTextureOffset(9, 221).addBox(-3.1F, -2.7F, 1.3F, 2.0F, 3.0F, 3.0F, 0.0F, false);
        rotate_y.setTextureOffset(12, 225).addBox(-4.3F, -6.2F, -2.1F, 2.0F, 5.0F, 5.0F, 0.0F, false);
        rotate_y.setTextureOffset(18, 219).addBox(-4.2F, -8.2F, 0.3F, 2.0F, 3.0F, 4.0F, 0.0F, false);
        rotate_y.setTextureOffset(13, 220).addBox(2.2F, -4.8F, 0.3F, 2.0F, 3.0F, 4.0F, 0.0F, false);
        rotate_y.setTextureOffset(16, 219).addBox(2.4F, -8.2F, -0.7F, 2.0F, 4.0F, 3.0F, 0.0F, false);
        rotate_y.setTextureOffset(8, 226).addBox(2.2F, -2.8F, -4.2F, 2.0F, 3.0F, 4.0F, 0.0F, false);
        rotate_y.setTextureOffset(16, 220).addBox(3.2F, -6.8F, -4.2F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        rotate_y.setTextureOffset(202, 83).addBox(-0.5F, -8.4F, -5.0F, 1.0F, 9.0F, 10.0F, 0.0F, false);
        rotate_y.setTextureOffset(206, 83).addBox(-0.5F, -9.4F, -0.5F, 1.0F, 11.0F, 1.0F, 0.0F, false);

        glow_globe = new LightModelRenderer(this);
        glow_globe.setRotationPoint(0.5F, 6.7F, 81.1F);
        rotate_y.addChild(glow_globe);
        glow_globe.setTextureOffset(181, 205).addBox(-4.5F, -14.7F, -85.1F, 8.0F, 8.0F, 8.0F, 0.0F, false);

        pipes_2 = new ModelRenderer(this);
        pipes_2.setRotationPoint(9.0F, -16.0F, -1.75F);
        station_tilt_2.addChild(pipes_2);
        pipes_2.setTextureOffset(209, 92).addBox(-19.7F, 14.1F, -79.25F, 1.0F, 4.0F, 2.0F, 0.0F, false);
        pipes_2.setTextureOffset(209, 92).addBox(-15.2F, 10.6F, -79.25F, 1.0F, 4.0F, 2.0F, 0.0F, false);
        pipes_2.setTextureOffset(209, 92).addBox(7.55F, 13.1F, -79.25F, 1.0F, 5.0F, 2.0F, 0.0F, false);
        pipes_2.setTextureOffset(209, 92).addBox(0.55F, 7.85F, -79.25F, 1.0F, 4.0F, 2.0F, 0.0F, false);
        pipes_2.setTextureOffset(209, 92).addBox(-15.2F, -4.65F, -79.25F, 1.0F, 4.0F, 2.0F, 0.0F, false);
        pipes_2.setTextureOffset(228, 17).addBox(-20.2F, 20.35F, -79.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        pipes_2.setTextureOffset(228, 17).addBox(-20.2F, 13.35F, -79.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        pipes_2.setTextureOffset(228, 17).addBox(-15.7F, 13.35F, -79.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        pipes_2.setTextureOffset(228, 17).addBox(0.05F, 6.85F, -79.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        pipes_2.setTextureOffset(228, 17).addBox(-15.7F, -1.65F, -79.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        pipes_2.setTextureOffset(228, 17).addBox(0.05F, 11.35F, -79.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        pipes_2.setTextureOffset(228, 17).addBox(7.05F, 11.35F, -79.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        pipes_2.setTextureOffset(228, 17).addBox(-20.2F, 26.6F, -79.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        pipes_2.setTextureOffset(209, 92).addBox(-23.7F, 27.1F, -79.25F, 4.0F, 1.0F, 2.0F, 0.0F, false);
        pipes_2.setTextureOffset(209, 92).addBox(1.55F, 11.85F, -79.25F, 6.0F, 1.0F, 1.0F, 0.0F, false);
        pipes_2.setTextureOffset(209, 92).addBox(-2.7F, 7.35F, -79.25F, 4.0F, 1.0F, 1.0F, 0.0F, false);
        pipes_2.setTextureOffset(209, 92).addBox(-18.7F, 13.85F, -79.25F, 4.0F, 1.0F, 2.0F, 0.0F, false);

        side3 = new ModelRenderer(this);
        side3.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(side3);
        setRotationAngle(side3, 0.0F, -2.0944F, 0.0F);
        

        station_offset_1 = new ModelRenderer(this);
        station_offset_1.setRotationPoint(0.0F, -14.0F, 12.4F);
        side3.addChild(station_offset_1);
        setRotationAngle(station_offset_1, -0.2618F, 0.0F, 0.0F);
        station_offset_1.setTextureOffset(68, 152).addBox(-4.0F, -33.0F, -81.0F, 8.0F, 8.0F, 12.0F, 0.0F, false);

        xyz_increment_rotate_z = new ModelRenderer(this);
        xyz_increment_rotate_z.setRotationPoint(0.0F, -29.0F, -81.4F);
        station_offset_1.addChild(xyz_increment_rotate_z);
        

        spokes = new ModelRenderer(this);
        spokes.setRotationPoint(0.2F, -0.1F, -1.7F);
        xyz_increment_rotate_z.addChild(spokes);
        spokes.setTextureOffset(182, 152).addBox(-1.2F, -9.6F, -3.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);
        spokes.setTextureOffset(197, 90).addBox(-1.2F, -14.6F, -3.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        spokes.setTextureOffset(206, 90).addBox(-1.2F, 10.8F, -3.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        spokes.setTextureOffset(203, 88).addBox(-0.7F, -11.0F, -2.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        spokes.setTextureOffset(206, 80).addBox(-0.7F, 9.0F, -2.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        spokes.setTextureOffset(182, 152).addBox(-9.6F, -1.0F, -3.0F, 8.0F, 2.0F, 2.0F, 0.0F, false);
        spokes.setTextureOffset(182, 152).addBox(1.4F, -1.0F, -3.0F, 8.0F, 2.0F, 2.0F, 0.0F, false);
        spokes.setTextureOffset(223, 59).addBox(11.2F, -1.0F, -3.0F, 4.0F, 2.0F, 2.0F, 0.0F, false);
        spokes.setTextureOffset(191, 80).addBox(-15.4F, -1.0F, -3.0F, 4.0F, 2.0F, 2.0F, 0.0F, false);
        spokes.setTextureOffset(216, 95).addBox(9.4F, -0.5F, -2.4F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        spokes.setTextureOffset(208, 94).addBox(-11.6F, -0.5F, -2.5F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        spokes.setTextureOffset(182, 152).addBox(-3.6F, -8.5F, -4.0F, 7.0F, 2.0F, 4.0F, 0.0F, false);
        spokes.setTextureOffset(182, 152).addBox(-3.6F, 6.4F, -4.0F, 7.0F, 2.0F, 4.0F, 0.0F, false);
        spokes.setTextureOffset(182, 152).addBox(6.3F, -3.6F, -4.0F, 2.0F, 7.0F, 4.0F, 0.0F, false);
        spokes.setTextureOffset(182, 152).addBox(-8.6F, -3.6F, -4.0F, 2.0F, 7.0F, 4.0F, 0.0F, false);
        spokes.setTextureOffset(182, 152).addBox(-1.2F, 1.6F, -3.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);

        spokes2 = new ModelRenderer(this);
        spokes2.setRotationPoint(0.2F, -0.1F, -1.7F);
        xyz_increment_rotate_z.addChild(spokes2);
        setRotationAngle(spokes2, 0.0F, 0.0F, -0.7854F);
        spokes2.setTextureOffset(182, 152).addBox(-1.2F, -9.6F, -3.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);
        spokes2.setTextureOffset(208, 99).addBox(-1.2F, -14.6F, -3.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        spokes2.setTextureOffset(204, 90).addBox(-1.2F, 10.8F, -3.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
        spokes2.setTextureOffset(208, 95).addBox(-0.7F, -11.0F, -2.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        spokes2.setTextureOffset(207, 80).addBox(-0.7F, 9.0F, -2.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        spokes2.setTextureOffset(182, 152).addBox(-9.6F, -1.0F, -3.0F, 8.0F, 2.0F, 2.0F, 0.0F, false);
        spokes2.setTextureOffset(182, 152).addBox(1.4F, -1.0F, -3.0F, 8.0F, 2.0F, 2.0F, 0.0F, false);
        spokes2.setTextureOffset(200, 99).addBox(11.2F, -1.0F, -3.0F, 4.0F, 2.0F, 2.0F, 0.0F, false);
        spokes2.setTextureOffset(209, 100).addBox(-15.4F, -1.0F, -3.0F, 4.0F, 2.0F, 2.0F, 0.0F, false);
        spokes2.setTextureOffset(205, 89).addBox(9.4F, -0.5F, -2.4F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        spokes2.setTextureOffset(205, 91).addBox(-11.6F, -0.5F, -2.5F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        spokes2.setTextureOffset(182, 152).addBox(-3.5F, -8.6F, -4.0F, 7.0F, 2.0F, 4.0F, 0.0F, false);
        spokes2.setTextureOffset(182, 152).addBox(-3.5F, 6.3F, -4.0F, 7.0F, 2.0F, 4.0F, 0.0F, false);
        spokes2.setTextureOffset(182, 152).addBox(6.4F, -3.6F, -4.0F, 2.0F, 7.0F, 4.0F, 0.0F, false);
        spokes2.setTextureOffset(182, 152).addBox(-8.5F, -3.6F, -4.0F, 2.0F, 7.0F, 4.0F, 0.0F, false);
        spokes2.setTextureOffset(182, 152).addBox(-1.2F, 1.6F, -3.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);

        center = new ModelRenderer(this);
        center.setRotationPoint(0.0F, 29.0F, 82.6F);
        xyz_increment_rotate_z.addChild(center);
        center.setTextureOffset(198, 84).addBox(-1.4F, -30.6F, -88.0F, 3.0F, 3.0F, 10.0F, 0.0F, false);

        station_tilt_1 = new ModelRenderer(this);
        station_tilt_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        side3.addChild(station_tilt_1);
        setRotationAngle(station_tilt_1, -1.0472F, 0.0F, 0.0F);
        

        coorddial_x = new ModelRenderer(this);
        coorddial_x.setRotationPoint(-11.05F, -17.1F, -79.0F);
        station_tilt_1.addChild(coorddial_x);
        setRotationAngle(coorddial_x, 0.0F, 0.0F, 0.7854F);
        coorddial_x.setTextureOffset(182, 152).addBox(4.05F, -0.6F, -1.4F, 7.0F, 7.0F, 4.0F, 0.0F, false);
        coorddial_x.setTextureOffset(209, 64).addBox(5.15F, 0.5F, -2.4F, 5.0F, 5.0F, 4.0F, 0.0F, false);
        coorddial_x.setTextureOffset(138, 157).addBox(5.65F, 1.1F, -3.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        glow_cord_x = new LightModelRenderer(this);
        glow_cord_x.setRotationPoint(11.05F, 17.1F, 79.0F);
        coorddial_x.addChild(glow_cord_x);
        glow_cord_x.setTextureOffset(185, 11).addBox(-4.8F, -15.5F, -82.6F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        coorddial_y = new ModelRenderer(this);
        coorddial_y.setRotationPoint(-11.05F, -17.1F, -79.0F);
        station_tilt_1.addChild(coorddial_y);
        setRotationAngle(coorddial_y, 0.0F, 0.0F, 0.7854F);
        coorddial_y.setTextureOffset(182, 152).addBox(7.45F, -8.6F, -1.4F, 7.0F, 7.0F, 4.0F, 0.0F, false);
        coorddial_y.setTextureOffset(209, 69).addBox(8.55F, -7.5F, -2.4F, 5.0F, 5.0F, 4.0F, 0.0F, false);
        coorddial_y.setTextureOffset(143, 158).addBox(9.05F, -6.9F, -3.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        glow_cord_y = new LightModelRenderer(this);
        glow_cord_y.setRotationPoint(11.05F, 17.1F, 79.0F);
        coorddial_y.addChild(glow_cord_y);
        glow_cord_y.setTextureOffset(186, 13).addBox(-1.4F, -23.5F, -82.6F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        coorddial_z = new ModelRenderer(this);
        coorddial_z.setRotationPoint(-11.05F, -17.1F, -79.0F);
        station_tilt_1.addChild(coorddial_z);
        setRotationAngle(coorddial_z, 0.0F, 0.0F, 0.7854F);
        coorddial_z.setTextureOffset(182, 152).addBox(15.45F, -12.1F, -1.4F, 7.0F, 7.0F, 4.0F, 0.0F, false);
        coorddial_z.setTextureOffset(209, 64).addBox(16.55F, -11.0F, -2.4F, 5.0F, 5.0F, 4.0F, 0.0F, false);
        coorddial_z.setTextureOffset(137, 152).addBox(17.05F, -10.4F, -3.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        glow_cord_z = new LightModelRenderer(this);
        glow_cord_z.setRotationPoint(11.05F, 17.1F, 79.0F);
        coorddial_z.addChild(glow_cord_z);
        glow_cord_z.setTextureOffset(190, 11).addBox(6.6F, -27.0F, -82.6F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        fancy_nameplate = new ModelRenderer(this);
        fancy_nameplate.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_tilt_1.addChild(fancy_nameplate);
        fancy_nameplate.setTextureOffset(182, 152).addBox(-3.7F, -5.9F, -80.5F, 8.0F, 4.0F, 4.0F, 0.0F, false);
        fancy_nameplate.setTextureOffset(182, 152).addBox(-4.7F, -4.9F, -80.5F, 1.0F, 2.0F, 4.0F, 0.0F, false);
        fancy_nameplate.setTextureOffset(182, 152).addBox(4.3F, -4.9F, -80.5F, 1.0F, 2.0F, 4.0F, 0.0F, false);
        fancy_nameplate.setTextureOffset(190, 56).addBox(-3.2F, -5.3F, -80.9F, 7.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_toggle_a1 = new ModelRenderer(this);
        dummy_toggle_a1.setRotationPoint(-12.62F, 7.46F, -47.1F);
        station_tilt_1.addChild(dummy_toggle_a1);
        dummy_toggle_a1.setTextureOffset(203, 156).addBox(-9.08F, -0.36F, -33.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        tilt = new ModelRenderer(this);
        tilt.setRotationPoint(-7.08F, 1.74F, -32.4F);
        dummy_toggle_a1.addChild(tilt);
        setRotationAngle(tilt, 0.3491F, 0.0F, 0.0F);
        tilt.setTextureOffset(207, 86).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 3.0F, 4.0F, 0.0F, false);

        tilt2 = new ModelRenderer(this);
        tilt2.setRotationPoint(-7.08F, 1.74F, -32.4F);
        dummy_toggle_a1.addChild(tilt2);
        setRotationAngle(tilt2, -0.3491F, 0.0F, 0.0F);
        tilt2.setTextureOffset(207, 86).addBox(-1.0F, -1.3F, -1.1F, 2.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_toggle_a2 = new ModelRenderer(this);
        dummy_toggle_a2.setRotationPoint(-6.82F, 5.46F, -47.1F);
        station_tilt_1.addChild(dummy_toggle_a2);
        dummy_toggle_a2.setTextureOffset(194, 162).addBox(-9.88F, 1.64F, -33.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        tilt3 = new ModelRenderer(this);
        tilt3.setRotationPoint(-7.88F, 3.74F, -32.4F);
        dummy_toggle_a2.addChild(tilt3);
        setRotationAngle(tilt3, 0.3491F, 0.0F, 0.0F);
        tilt3.setTextureOffset(204, 96).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 3.0F, 4.0F, 0.0F, false);

        tilt4 = new ModelRenderer(this);
        tilt4.setRotationPoint(-7.88F, 3.74F, -32.4F);
        dummy_toggle_a2.addChild(tilt4);
        setRotationAngle(tilt4, -0.3491F, 0.0F, 0.0F);
        tilt4.setTextureOffset(204, 96).addBox(-1.0F, -1.3F, -1.1F, 2.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_toggle_a3 = new ModelRenderer(this);
        dummy_toggle_a3.setRotationPoint(-11.02F, 8.46F, -47.1F);
        station_tilt_1.addChild(dummy_toggle_a3);
        dummy_toggle_a3.setTextureOffset(195, 160).addBox(-10.68F, 3.64F, -33.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        tilt5 = new ModelRenderer(this);
        tilt5.setRotationPoint(-8.68F, 5.74F, -32.4F);
        dummy_toggle_a3.addChild(tilt5);
        setRotationAngle(tilt5, 0.3491F, 0.0F, 0.0F);
        tilt5.setTextureOffset(195, 85).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 3.0F, 4.0F, 0.0F, false);

        tilt6 = new ModelRenderer(this);
        tilt6.setRotationPoint(-8.68F, 5.74F, -32.4F);
        dummy_toggle_a3.addChild(tilt6);
        setRotationAngle(tilt6, -0.3491F, 0.0F, 0.0F);
        tilt6.setTextureOffset(195, 85).addBox(-1.0F, -1.3F, -1.1F, 2.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_toggle_a4 = new ModelRenderer(this);
        dummy_toggle_a4.setRotationPoint(-6.02F, 8.46F, -47.1F);
        station_tilt_1.addChild(dummy_toggle_a4);
        dummy_toggle_a4.setTextureOffset(200, 161).addBox(-10.68F, 3.64F, -33.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        tilt7 = new ModelRenderer(this);
        tilt7.setRotationPoint(-8.68F, 5.74F, -32.4F);
        dummy_toggle_a4.addChild(tilt7);
        setRotationAngle(tilt7, 0.3491F, 0.0F, 0.0F);
        tilt7.setTextureOffset(211, 96).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 3.0F, 4.0F, 0.0F, false);

        tilt8 = new ModelRenderer(this);
        tilt8.setRotationPoint(-8.68F, 5.74F, -32.4F);
        dummy_toggle_a4.addChild(tilt8);
        setRotationAngle(tilt8, -0.3491F, 0.0F, 0.0F);
        tilt8.setTextureOffset(211, 96).addBox(-1.0F, -1.3F, -1.1F, 2.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_toggle_a5 = new ModelRenderer(this);
        dummy_toggle_a5.setRotationPoint(22.38F, 7.46F, -47.1F);
        station_tilt_1.addChild(dummy_toggle_a5);
        dummy_toggle_a5.setTextureOffset(195, 153).addBox(-9.08F, -0.36F, -33.4F, 4.0F, 4.0F, 6.0F, 0.0F, false);

        tilt9 = new ModelRenderer(this);
        tilt9.setRotationPoint(-7.08F, 1.74F, -32.4F);
        dummy_toggle_a5.addChild(tilt9);
        setRotationAngle(tilt9, 0.3491F, 0.0F, 0.0F);
        tilt9.setTextureOffset(197, 105).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 3.0F, 4.0F, 0.0F, false);

        tilt10 = new ModelRenderer(this);
        tilt10.setRotationPoint(-7.08F, 1.74F, -32.4F);
        dummy_toggle_a5.addChild(tilt10);
        setRotationAngle(tilt10, -0.3491F, 0.0F, 0.0F);
        tilt10.setTextureOffset(197, 105).addBox(-1.0F, -1.3F, -1.1F, 2.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_toggle_a6 = new ModelRenderer(this);
        dummy_toggle_a6.setRotationPoint(27.38F, 7.46F, -47.1F);
        station_tilt_1.addChild(dummy_toggle_a6);
        dummy_toggle_a6.setTextureOffset(194, 157).addBox(-9.08F, -0.36F, -33.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        tilt11 = new ModelRenderer(this);
        tilt11.setRotationPoint(-7.08F, 1.74F, -32.4F);
        dummy_toggle_a6.addChild(tilt11);
        setRotationAngle(tilt11, 0.3491F, 0.0F, 0.0F);
        tilt11.setTextureOffset(213, 87).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 3.0F, 4.0F, 0.0F, false);

        tilt12 = new ModelRenderer(this);
        tilt12.setRotationPoint(-7.08F, 1.74F, -32.4F);
        dummy_toggle_a6.addChild(tilt12);
        setRotationAngle(tilt12, -0.3491F, 0.0F, 0.0F);
        tilt12.setTextureOffset(213, 87).addBox(-1.0F, -1.3F, -1.1F, 2.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_toggle_a7 = new ModelRenderer(this);
        dummy_toggle_a7.setRotationPoint(22.38F, 12.46F, -47.1F);
        station_tilt_1.addChild(dummy_toggle_a7);
        dummy_toggle_a7.setTextureOffset(198, 157).addBox(-9.08F, -0.36F, -33.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        tilt13 = new ModelRenderer(this);
        tilt13.setRotationPoint(-7.08F, 1.74F, -32.4F);
        dummy_toggle_a7.addChild(tilt13);
        setRotationAngle(tilt13, 0.3491F, 0.0F, 0.0F);
        tilt13.setTextureOffset(198, 102).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 3.0F, 4.0F, 0.0F, false);

        tilt14 = new ModelRenderer(this);
        tilt14.setRotationPoint(-7.08F, 1.74F, -32.4F);
        dummy_toggle_a7.addChild(tilt14);
        setRotationAngle(tilt14, -0.3491F, 0.0F, 0.0F);
        tilt14.setTextureOffset(198, 102).addBox(-1.0F, -1.3F, -1.1F, 2.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_toggle_a8 = new ModelRenderer(this);
        dummy_toggle_a8.setRotationPoint(27.38F, 12.46F, -47.1F);
        station_tilt_1.addChild(dummy_toggle_a8);
        dummy_toggle_a8.setTextureOffset(196, 158).addBox(-9.08F, -0.36F, -33.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        tilt15 = new ModelRenderer(this);
        tilt15.setRotationPoint(-7.08F, 1.74F, -32.4F);
        dummy_toggle_a8.addChild(tilt15);
        setRotationAngle(tilt15, 0.3491F, 0.0F, 0.0F);
        tilt15.setTextureOffset(190, 93).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 3.0F, 4.0F, 0.0F, false);

        tilt16 = new ModelRenderer(this);
        tilt16.setRotationPoint(-7.08F, 1.74F, -32.4F);
        dummy_toggle_a8.addChild(tilt16);
        setRotationAngle(tilt16, -0.3491F, 0.0F, 0.0F);
        tilt16.setTextureOffset(190, 93).addBox(-1.0F, -1.3F, -1.1F, 2.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_button_a1 = new ModelRenderer(this);
        dummy_button_a1.setRotationPoint(7.38F, 0.46F, -47.1F);
        station_tilt_1.addChild(dummy_button_a1);
        dummy_button_a1.setTextureOffset(182, 152).addBox(-25.08F, -0.36F, -33.4F, 12.0F, 4.0F, 4.0F, 0.0F, false);
        dummy_button_a1.setTextureOffset(132, 153).addBox(-20.58F, 0.14F, -34.1F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        dummy_button_a1.setTextureOffset(133, 153).addBox(-24.38F, 0.14F, -34.4F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        dummy_button_a1.setTextureOffset(134, 153).addBox(-16.88F, 0.14F, -34.1F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_button_a2 = new ModelRenderer(this);
        dummy_button_a2.setRotationPoint(7.38F, 0.46F, -47.1F);
        station_tilt_1.addChild(dummy_button_a2);
        dummy_button_a2.setTextureOffset(182, 152).addBox(-13.08F, -0.36F, -33.4F, 12.0F, 4.0F, 4.0F, 0.0F, false);
        dummy_button_a2.setTextureOffset(146, 155).addBox(-8.58F, 0.14F, -34.5F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        dummy_button_a2.setTextureOffset(133, 158).addBox(-12.38F, 0.14F, -34.1F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        dummy_button_a2.setTextureOffset(147, 160).addBox(-4.88F, 0.14F, -34.1F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_button_a3 = new ModelRenderer(this);
        dummy_button_a3.setRotationPoint(7.38F, 0.46F, -47.1F);
        station_tilt_1.addChild(dummy_button_a3);
        dummy_button_a3.setTextureOffset(182, 152).addBox(-1.08F, -0.36F, -33.4F, 12.0F, 4.0F, 4.0F, 0.0F, false);
        dummy_button_a3.setTextureOffset(141, 154).addBox(3.42F, 0.14F, -34.1F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        dummy_button_a3.setTextureOffset(146, 160).addBox(-0.38F, 0.14F, -34.1F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        dummy_button_a3.setTextureOffset(137, 159).addBox(7.12F, 0.14F, -34.5F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        side4 = new ModelRenderer(this);
        side4.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(side4);
        setRotationAngle(side4, 0.0F, 3.1416F, 0.0F);
        

        dimention_select = new ModelRenderer(this);
        dimention_select.setRotationPoint(-0.2F, -57.0F, 55.6F);
        side4.addChild(dimention_select);
        

        dialframe = new ModelRenderer(this);
        dialframe.setRotationPoint(0.2F, -29.1F, -84.3F);
        dimention_select.addChild(dialframe);
        dialframe.setTextureOffset(202, 79).addBox(-3.6F, -8.5F, -4.0F, 7.0F, 2.0F, 4.0F, 0.0F, false);
        dialframe.setTextureOffset(202, 79).addBox(-1.2F, -0.9F, -5.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        dialframe.setTextureOffset(14, 155).addBox(-3.6F, -6.5F, -3.3F, 7.0F, 14.0F, 3.0F, 0.0F, false);
        dialframe.setTextureOffset(21, 158).addBox(3.4F, -6.5F, -3.3F, 2.0F, 12.0F, 3.0F, 0.0F, false);
        dialframe.setTextureOffset(17, 154).addBox(-5.6F, -6.5F, -3.3F, 2.0F, 12.0F, 3.0F, 0.0F, false);
        dialframe.setTextureOffset(14, 160).addBox(5.4F, -4.5F, -3.3F, 2.0F, 8.0F, 3.0F, 0.0F, false);
        dialframe.setTextureOffset(14, 158).addBox(-7.6F, -4.5F, -3.3F, 2.0F, 8.0F, 3.0F, 0.0F, false);
        dialframe.setTextureOffset(202, 79).addBox(-3.6F, 6.4F, -4.0F, 7.0F, 2.0F, 4.0F, 0.0F, false);
        dialframe.setTextureOffset(202, 79).addBox(6.3F, -3.6F, -4.0F, 2.0F, 7.0F, 4.0F, 0.0F, false);
        dialframe.setTextureOffset(202, 79).addBox(-8.6F, -3.6F, -4.0F, 2.0F, 7.0F, 4.0F, 0.0F, false);
        dialframe.setTextureOffset(202, 79).addBox(-1.7F, 7.4F, -3.0F, 1.0F, 4.0F, 2.0F, 0.0F, false);
        dialframe.setTextureOffset(202, 79).addBox(0.7F, 7.4F, -3.0F, 1.0F, 4.0F, 2.0F, 0.0F, false);

        dialframe2 = new ModelRenderer(this);
        dialframe2.setRotationPoint(0.2F, -29.1F, -84.3F);
        dimention_select.addChild(dialframe2);
        setRotationAngle(dialframe2, 0.0F, 0.0F, -0.7854F);
        dialframe2.setTextureOffset(202, 79).addBox(-3.5F, -8.6F, -4.0F, 7.0F, 2.0F, 4.0F, 0.0F, false);
        dialframe2.setTextureOffset(202, 79).addBox(-3.5F, 6.3F, -4.0F, 7.0F, 2.0F, 4.0F, 0.0F, false);
        dialframe2.setTextureOffset(202, 79).addBox(6.4F, -3.6F, -4.0F, 2.0F, 7.0F, 4.0F, 0.0F, false);
        dialframe2.setTextureOffset(202, 79).addBox(-8.5F, -3.6F, -4.0F, 2.0F, 7.0F, 4.0F, 0.0F, false);

        needle_rotate_z = new ModelRenderer(this);
        needle_rotate_z.setRotationPoint(0.0F, -28.9F, -87.9F);
        dimention_select.addChild(needle_rotate_z);
        setRotationAngle(needle_rotate_z, 0.0F, 0.0F, -2.0944F);
        needle_rotate_z.setTextureOffset(144, 161).addBox(-0.5F, -6.1F, 0.0F, 1.0F, 7.0F, 0.0F, 0.0F, false);

        station_tilt_4 = new ModelRenderer(this);
        station_tilt_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        side4.addChild(station_tilt_4);
        setRotationAngle(station_tilt_4, -1.0472F, 0.0F, 0.0F);
        station_tilt_4.setTextureOffset(182, 152).addBox(-2.5F, -13.8F, -81.0F, 5.0F, 5.0F, 4.0F, 0.0F, false);
        station_tilt_4.setTextureOffset(182, 152).addBox(8.5F, 4.4F, -81.0F, 11.0F, 11.0F, 4.0F, 0.0F, false);
        station_tilt_4.setTextureOffset(182, 152).addBox(8.0F, 3.8F, -80.3F, 12.0F, 12.0F, 4.0F, 0.0F, false);
        station_tilt_4.setTextureOffset(69, 156).addBox(-21.0F, 4.0F, -81.0F, 14.0F, 12.0F, 4.0F, 0.0F, false);

        waypoint_selector = new ModelRenderer(this);
        waypoint_selector.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_tilt_4.addChild(waypoint_selector);
        

        glow_selector = new LightModelRenderer(this);
        glow_selector.setRotationPoint(0.0F, 0.0F, 0.0F);
        waypoint_selector.addChild(glow_selector);
        glow_selector.setTextureOffset(181, 11).addBox(10.0F, 6.0F, -82.6F, 8.0F, 8.0F, 4.0F, 0.0F, false);
        glow_selector.setTextureOffset(181, 13).addBox(11.0F, 7.0F, -83.1F, 6.0F, 6.0F, 4.0F, 0.0F, false);
        glow_selector.setTextureOffset(178, 13).addBox(11.0F, 14.0F, -82.0F, 6.0F, 1.0F, 4.0F, 0.0F, false);
        glow_selector.setTextureOffset(180, 17).addBox(11.0F, 5.0F, -82.0F, 6.0F, 1.0F, 4.0F, 0.0F, false);
        glow_selector.setTextureOffset(185, 10).addBox(9.0F, 7.0F, -82.0F, 1.0F, 6.0F, 4.0F, 0.0F, false);
        glow_selector.setTextureOffset(186, 13).addBox(18.0F, 7.0F, -82.0F, 1.0F, 6.0F, 4.0F, 0.0F, false);

        dummy_button_d1 = new ModelRenderer(this);
        dummy_button_d1.setRotationPoint(7.38F, 0.46F, -47.1F);
        station_tilt_4.addChild(dummy_button_d1);
        dummy_button_d1.setTextureOffset(14, 162).addBox(-27.33F, 4.64F, -34.65F, 12.0F, 4.0F, 4.0F, 0.0F, false);
        dummy_button_d1.setTextureOffset(141, 154).addBox(-22.83F, 5.14F, -35.35F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        dummy_button_d1.setTextureOffset(146, 160).addBox(-26.63F, 5.14F, -35.35F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        dummy_button_d1.setTextureOffset(137, 159).addBox(-19.13F, 5.14F, -35.75F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_button_d2 = new ModelRenderer(this);
        dummy_button_d2.setRotationPoint(7.38F, 0.46F, -47.1F);
        station_tilt_4.addChild(dummy_button_d2);
        dummy_button_d2.setTextureOffset(14, 162).addBox(-27.33F, 9.64F, -34.65F, 12.0F, 4.0F, 4.0F, 0.0F, false);
        dummy_button_d2.setTextureOffset(141, 154).addBox(-22.83F, 10.14F, -35.35F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        dummy_button_d2.setTextureOffset(146, 160).addBox(-26.63F, 10.14F, -35.35F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        dummy_button_d2.setTextureOffset(137, 159).addBox(-19.13F, 10.14F, -35.75F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        sonic_port = new ModelRenderer(this);
        sonic_port.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_tilt_4.addChild(sonic_port);
        sonic_port.setTextureOffset(135, 161).addBox(-2.0F, 0.0F, -80.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(182, 152).addBox(-3.0F, -1.0F, -81.4F, 1.0F, 6.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(182, 152).addBox(2.0F, -1.0F, -81.4F, 1.0F, 6.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(182, 152).addBox(-2.0F, -2.0F, -81.4F, 4.0F, 2.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(182, 152).addBox(-1.0F, -3.0F, -81.4F, 2.0F, 1.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(182, 152).addBox(-1.0F, 6.0F, -81.4F, 2.0F, 1.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(201, 95).addBox(-0.5F, 6.9F, -80.4F, 1.0F, 5.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(206, 102).addBox(10.8F, 1.0F, -80.4F, 1.0F, 5.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(198, 96).addBox(-8.5F, 11.9F, -80.4F, 8.0F, 1.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(188, 94).addBox(2.8F, -0.1F, -80.4F, 8.0F, 1.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(231, 18).addBox(-1.2F, 11.3F, -80.7F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(182, 152).addBox(1.5F, 5.6F, -80.7F, 1.0F, 1.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(182, 152).addBox(1.5F, -2.7F, -80.7F, 1.0F, 1.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(182, 152).addBox(-2.6F, 5.6F, -80.7F, 1.0F, 1.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(182, 152).addBox(-2.6F, -2.7F, -80.7F, 1.0F, 1.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(228, 17).addBox(10.3F, -0.7F, -80.7F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        sonic_port.setTextureOffset(182, 152).addBox(-2.0F, 4.0F, -81.4F, 4.0F, 2.0F, 4.0F, 0.0F, false);

        dummy_dial_d1 = new ModelRenderer(this);
        dummy_dial_d1.setRotationPoint(-11.05F, -17.1F, -79.0F);
        station_tilt_4.addChild(dummy_dial_d1);
        setRotationAngle(dummy_dial_d1, 0.0F, 0.0F, 0.7854F);
        dummy_dial_d1.setTextureOffset(182, 152).addBox(3.45F, 0.9F, -1.4F, 7.0F, 7.0F, 4.0F, 0.0F, false);
        dummy_dial_d1.setTextureOffset(204, 51).addBox(4.55F, 2.0F, -2.4F, 5.0F, 5.0F, 4.0F, 0.0F, false);
        dummy_dial_d1.setTextureOffset(132, 166).addBox(5.05F, 2.6F, -3.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        glow_d1 = new LightModelRenderer(this);
        glow_d1.setRotationPoint(11.05F, 17.1F, 79.0F);
        dummy_dial_d1.addChild(glow_d1);
        glow_d1.setTextureOffset(24, 160).addBox(-5.4F, -14.0F, -82.6F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_dial_d2 = new ModelRenderer(this);
        dummy_dial_d2.setRotationPoint(-11.05F, -17.1F, -79.0F);
        station_tilt_4.addChild(dummy_dial_d2);
        setRotationAngle(dummy_dial_d2, 0.0F, 0.0F, 0.7854F);
        dummy_dial_d2.setTextureOffset(182, 152).addBox(16.45F, -12.1F, -1.4F, 7.0F, 7.0F, 4.0F, 0.0F, false);
        dummy_dial_d2.setTextureOffset(192, 77).addBox(17.55F, -11.0F, -2.4F, 5.0F, 5.0F, 4.0F, 0.0F, false);
        dummy_dial_d2.setTextureOffset(131, 159).addBox(18.05F, -10.4F, -3.4F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        glow_d2 = new LightModelRenderer(this);
        glow_d2.setRotationPoint(11.05F, 17.1F, 79.0F);
        dummy_dial_d2.addChild(glow_d2);
        glow_d2.setTextureOffset(26, 162).addBox(7.6F, -27.0F, -82.6F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        side5 = new ModelRenderer(this);
        side5.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(side5);
        setRotationAngle(side5, 0.0F, 2.0944F, 0.0F);
        

        telepathic_circuits = new ModelRenderer(this);
        telepathic_circuits.setRotationPoint(0.0F, 0.0F, 0.0F);
        side5.addChild(telepathic_circuits);
        telepathic_circuits.setTextureOffset(186, 84).addBox(-6.0F, -71.3F, -55.0F, 12.0F, 12.0F, 12.0F, 0.0F, false);
        telepathic_circuits.setTextureOffset(186, 84).addBox(-7.0F, -72.2F, -56.0F, 14.0F, 1.0F, 14.0F, 0.0F, false);

        station_tilt_5 = new ModelRenderer(this);
        station_tilt_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        side5.addChild(station_tilt_5);
        setRotationAngle(station_tilt_5, -1.0472F, 0.0F, 0.0F);
        station_tilt_5.setTextureOffset(182, 152).addBox(-7.0F, 2.0F, -80.75F, 14.0F, 16.0F, 4.0F, 0.0F, false);
        station_tilt_5.setTextureOffset(191, 79).addBox(-5.0F, 2.0F, -90.0F, 10.0F, 10.0F, 7.0F, 0.0F, false);

        pipes_5 = new ModelRenderer(this);
        pipes_5.setRotationPoint(9.0F, -16.0F, -1.75F);
        station_tilt_5.addChild(pipes_5);
        pipes_5.setTextureOffset(209, 92).addBox(-19.7F, 11.1F, -79.25F, 1.0F, 17.0F, 2.0F, 0.0F, false);
        pipes_5.setTextureOffset(209, 92).addBox(7.55F, 16.1F, -79.25F, 1.0F, 12.0F, 2.0F, 0.0F, false);
        pipes_5.setTextureOffset(209, 92).addBox(0.55F, 12.85F, -79.25F, 1.0F, 4.0F, 2.0F, 0.0F, false);
        pipes_5.setTextureOffset(209, 92).addBox(-9.2F, -4.65F, -79.25F, 1.0F, 4.0F, 2.0F, 0.0F, false);
        pipes_5.setTextureOffset(228, 17).addBox(-20.2F, 20.35F, -79.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        pipes_5.setTextureOffset(228, 17).addBox(-20.2F, 10.85F, -79.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        pipes_5.setTextureOffset(228, 17).addBox(0.05F, 10.85F, -79.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        pipes_5.setTextureOffset(228, 17).addBox(-9.7F, -0.65F, -79.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        pipes_5.setTextureOffset(228, 17).addBox(0.05F, 15.35F, -79.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        pipes_5.setTextureOffset(228, 17).addBox(7.05F, 15.35F, -79.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        pipes_5.setTextureOffset(228, 17).addBox(-20.2F, 26.6F, -79.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        pipes_5.setTextureOffset(209, 92).addBox(-23.7F, 27.1F, -79.25F, 4.0F, 1.0F, 2.0F, 0.0F, false);
        pipes_5.setTextureOffset(209, 92).addBox(1.55F, 15.85F, -79.25F, 6.0F, 1.0F, 1.0F, 0.0F, false);
        pipes_5.setTextureOffset(209, 92).addBox(-18.7F, 20.85F, -79.25F, 4.0F, 1.0F, 2.0F, 0.0F, false);

        glow_telepathics = new LightModelRenderer(this);
        glow_telepathics.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_tilt_5.addChild(glow_telepathics);
        glow_telepathics.setTextureOffset(167, 195).addBox(-4.0F, 3.0F, -90.4F, 8.0F, 8.0F, 4.0F, 0.0F, false);

        bone = new ModelRenderer(this);
        bone.setRotationPoint(0.0F, 2.5F, -93.5F);
        station_tilt_5.addChild(bone);
        setRotationAngle(bone, 0.6109F, 0.0F, 0.0F);
        bone.setTextureOffset(195, 102).addBox(-4.6F, 1.7F, -2.8F, 9.0F, 1.0F, 6.0F, 0.0F, false);

        bone3 = new ModelRenderer(this);
        bone3.setRotationPoint(0.0F, 2.5F, -93.5F);
        station_tilt_5.addChild(bone3);
        setRotationAngle(bone3, 0.6109F, 0.0F, 0.0F);
        bone3.setTextureOffset(200, 94).addBox(-4.6F, 2.7F, -2.8F, 1.0F, 7.0F, 6.0F, 0.0F, false);
        bone3.setTextureOffset(211, 88).addBox(3.4F, 2.7F, -2.8F, 1.0F, 7.0F, 6.0F, 0.0F, false);

        dummy_plate = new ModelRenderer(this);
        dummy_plate.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_tilt_5.addChild(dummy_plate);
        dummy_plate.setTextureOffset(51, 97).addBox(-11.75F, -15.0F, -81.0F, 24.0F, 10.0F, 4.0F, 0.0F, false);
        dummy_plate.setTextureOffset(229, 13).addBox(-10.25F, -8.0F, -81.25F, 4.0F, 2.0F, 4.0F, 0.0F, false);
        dummy_plate.setTextureOffset(229, 13).addBox(-4.75F, -8.0F, -81.25F, 4.0F, 2.0F, 4.0F, 0.0F, false);
        dummy_plate.setTextureOffset(229, 13).addBox(1.0F, -8.0F, -81.25F, 4.0F, 2.0F, 4.0F, 0.0F, false);
        dummy_plate.setTextureOffset(229, 13).addBox(6.75F, -8.0F, -81.25F, 4.0F, 2.0F, 4.0F, 0.0F, false);

        dummy_bell_e1 = new ModelRenderer(this);
        dummy_bell_e1.setRotationPoint(-1.62F, -13.54F, -48.85F);
        dummy_plate.addChild(dummy_bell_e1);
        dummy_bell_e1.setTextureOffset(195, 153).addBox(-9.08F, -0.36F, -33.4F, 5.0F, 5.0F, 6.0F, 0.0F, false);
        dummy_bell_e1.setTextureOffset(209, 92).addBox(-8.58F, 0.14F, -34.9F, 4.0F, 4.0F, 6.0F, 0.0F, false);
        dummy_bell_e1.setTextureOffset(209, 92).addBox(-8.08F, 0.64F, -35.65F, 3.0F, 3.0F, 6.0F, 0.0F, false);
        dummy_bell_e1.setTextureOffset(209, 92).addBox(-7.08F, 1.64F, -36.15F, 1.0F, 1.0F, 6.0F, 0.0F, false);

        dummy_bell_e2 = new ModelRenderer(this);
        dummy_bell_e2.setRotationPoint(-1.62F, -13.54F, -48.85F);
        dummy_plate.addChild(dummy_bell_e2);
        dummy_bell_e2.setTextureOffset(195, 153).addBox(-3.58F, -0.36F, -33.4F, 5.0F, 5.0F, 6.0F, 0.0F, false);
        dummy_bell_e2.setTextureOffset(209, 92).addBox(-3.08F, 0.14F, -34.9F, 4.0F, 4.0F, 6.0F, 0.0F, false);
        dummy_bell_e2.setTextureOffset(209, 92).addBox(-2.58F, 0.64F, -35.65F, 3.0F, 3.0F, 6.0F, 0.0F, false);
        dummy_bell_e2.setTextureOffset(209, 92).addBox(-1.58F, 1.64F, -36.15F, 1.0F, 1.0F, 6.0F, 0.0F, false);

        dummy_bell_e3 = new ModelRenderer(this);
        dummy_bell_e3.setRotationPoint(-1.62F, -13.54F, -48.85F);
        dummy_plate.addChild(dummy_bell_e3);
        dummy_bell_e3.setTextureOffset(195, 153).addBox(2.17F, -0.36F, -33.4F, 5.0F, 5.0F, 6.0F, 0.0F, false);
        dummy_bell_e3.setTextureOffset(209, 92).addBox(2.67F, 0.14F, -34.9F, 4.0F, 4.0F, 6.0F, 0.0F, false);
        dummy_bell_e3.setTextureOffset(209, 92).addBox(3.17F, 0.64F, -35.65F, 3.0F, 3.0F, 6.0F, 0.0F, false);
        dummy_bell_e3.setTextureOffset(209, 92).addBox(4.17F, 1.64F, -36.15F, 1.0F, 1.0F, 6.0F, 0.0F, false);

        dummy_bell_e4 = new ModelRenderer(this);
        dummy_bell_e4.setRotationPoint(-1.62F, -13.54F, -48.85F);
        dummy_plate.addChild(dummy_bell_e4);
        dummy_bell_e4.setTextureOffset(195, 153).addBox(7.92F, -0.36F, -33.4F, 5.0F, 5.0F, 6.0F, 0.0F, false);
        dummy_bell_e4.setTextureOffset(209, 92).addBox(8.42F, 0.14F, -34.9F, 4.0F, 4.0F, 6.0F, 0.0F, false);
        dummy_bell_e4.setTextureOffset(209, 92).addBox(8.92F, 0.64F, -35.65F, 3.0F, 3.0F, 6.0F, 0.0F, false);
        dummy_bell_e4.setTextureOffset(209, 92).addBox(9.92F, 1.64F, -36.15F, 1.0F, 1.0F, 6.0F, 0.0F, false);

        fast_return = new ModelRenderer(this);
        fast_return.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_tilt_5.addChild(fast_return);
        fast_return.setTextureOffset(182, 152).addBox(-22.0F, 9.75F, -80.75F, 8.0F, 5.0F, 4.0F, 0.0F, false);
        fast_return.setTextureOffset(29, 155).addBox(-21.5F, 10.75F, -81.75F, 7.0F, 3.0F, 4.0F, 0.0F, false);

        fast_return_button_rotate_x = new ModelRenderer(this);
        fast_return_button_rotate_x.setRotationPoint(-0.2128F, 77.3564F, -85.1029F);
        fast_return.addChild(fast_return_button_rotate_x);
        setRotationAngle(fast_return_button_rotate_x, 0.0175F, 0.0F, 0.0F);
        fast_return_button_rotate_x.setTextureOffset(96, 231).addBox(-20.7872F, -65.8564F, 2.1029F, 6.0F, 2.0F, 4.0F, 0.0F, false);

        stablizer = new ModelRenderer(this);
        stablizer.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_tilt_5.addChild(stablizer);
        stablizer.setTextureOffset(182, 152).addBox(15.0F, 10.0F, -80.5F, 8.0F, 5.0F, 4.0F, 0.0F, false);
        stablizer.setTextureOffset(29, 155).addBox(15.5F, 11.0F, -81.5F, 7.0F, 3.0F, 4.0F, 0.0F, false);

        stablizer_button_rotate_x2 = new ModelRenderer(this);
        stablizer_button_rotate_x2.setRotationPoint(0.7872F, 77.3564F, -85.1029F);
        stablizer.addChild(stablizer_button_rotate_x2);
        setRotationAngle(stablizer_button_rotate_x2, 0.0175F, 0.0F, 0.0F);
        stablizer_button_rotate_x2.setTextureOffset(8, 211).addBox(15.2128F, -65.8564F, 2.3529F, 6.0F, 2.0F, 4.0F, 0.0F, false);

        side6 = new ModelRenderer(this);
        side6.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls.addChild(side6);
        setRotationAngle(side6, 0.0F, 1.0472F, 0.0F);
        

        station_tilt_6 = new ModelRenderer(this);
        station_tilt_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        side6.addChild(station_tilt_6);
        setRotationAngle(station_tilt_6, -1.0472F, 0.0F, 0.0F);
        station_tilt_6.setTextureOffset(182, 152).addBox(-10.0F, -14.4F, -82.0F, 20.0F, 8.0F, 4.0F, 0.0F, false);
        station_tilt_6.setTextureOffset(182, 152).addBox(-11.0F, -15.4F, -81.0F, 22.0F, 10.0F, 4.0F, 0.0F, false);

        door = new ModelRenderer(this);
        door.setRotationPoint(0.0F, 0.0F, 0.0F);
        station_tilt_6.addChild(door);
        

        door_plate = new ModelRenderer(this);
        door_plate.setRotationPoint(0.0F, 0.0F, 0.0F);
        door.addChild(door_plate);
        door_plate.setTextureOffset(82, 160).addBox(-1.0F, 9.0F, -81.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        door_plate.setTextureOffset(133, 159).addBox(-0.5F, 7.25F, -81.25F, 1.0F, 2.0F, 4.0F, 0.0F, false);
        door_plate.setTextureOffset(133, 159).addBox(-1.0F, 5.25F, -81.25F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        door_plate.setTextureOffset(82, 160).addBox(-1.0F, -2.0F, -81.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        door_plate.setTextureOffset(82, 160).addBox(-2.5F, 8.45F, -80.75F, 1.0F, 1.0F, 4.0F, 0.0F, false);
        door_plate.setTextureOffset(82, 160).addBox(1.5F, 8.45F, -80.75F, 1.0F, 1.0F, 4.0F, 0.0F, false);
        door_plate.setTextureOffset(82, 160).addBox(1.5F, -0.55F, -80.75F, 1.0F, 1.0F, 4.0F, 0.0F, false);
        door_plate.setTextureOffset(82, 160).addBox(-2.5F, -0.55F, -80.75F, 1.0F, 1.0F, 4.0F, 0.0F, false);
        door_plate.setTextureOffset(82, 160).addBox(-2.0F, 0.0F, -81.0F, 4.0F, 9.0F, 4.0F, 0.0F, false);
        door_plate.setTextureOffset(82, 160).addBox(-1.5F, 1.25F, -81.75F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        door_knob_rotate_z = new ModelRenderer(this);
        door_knob_rotate_z.setRotationPoint(0.0F, 3.0F, -83.6667F);
        door.addChild(door_knob_rotate_z);
        door_knob_rotate_z.setTextureOffset(203, 93).addBox(-0.5F, -0.5F, 0.1667F, 1.0F, 1.0F, 4.0F, 0.0F, false);
        door_knob_rotate_z.setTextureOffset(23, 167).addBox(-1.5F, -1.5F, -1.8333F, 3.0F, 3.0F, 2.0F, 0.0F, false);
        door_knob_rotate_z.setTextureOffset(20, 161).addBox(-1.0F, -1.0F, -2.3333F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        dummy_button_f1 = new ModelRenderer(this);
        dummy_button_f1.setRotationPoint(7.38F, 0.46F, -47.1F);
        station_tilt_6.addChild(dummy_button_f1);
        dummy_button_f1.setTextureOffset(182, 152).addBox(-26.08F, 0.64F, -33.4F, 12.0F, 4.0F, 4.0F, 0.0F, false);
        dummy_button_f1.setTextureOffset(141, 154).addBox(-21.58F, 1.14F, -34.1F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        dummy_button_f1.setTextureOffset(137, 159).addBox(-17.88F, 1.14F, -34.5F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        glow_f1 = new LightModelRenderer(this);
        glow_f1.setRotationPoint(10.62F, -2.46F, 47.1F);
        dummy_button_f1.addChild(glow_f1);
        glow_f1.setTextureOffset(96, 236).addBox(-36.0F, 3.6F, -81.45F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_button_f2 = new ModelRenderer(this);
        dummy_button_f2.setRotationPoint(7.38F, 0.46F, -47.1F);
        station_tilt_6.addChild(dummy_button_f2);
        dummy_button_f2.setTextureOffset(182, 152).addBox(-26.08F, 5.64F, -33.4F, 12.0F, 4.0F, 4.0F, 0.0F, false);
        dummy_button_f2.setTextureOffset(146, 160).addBox(-25.38F, 6.14F, -34.1F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        dummy_button_f2.setTextureOffset(137, 159).addBox(-17.88F, 6.14F, -34.25F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        glow_f2 = new LightModelRenderer(this);
        glow_f2.setRotationPoint(10.62F, -2.46F, 47.1F);
        dummy_button_f2.addChild(glow_f2);
        glow_f2.setTextureOffset(183, 17).addBox(-32.2F, 8.6F, -81.2F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_button_f3 = new ModelRenderer(this);
        dummy_button_f3.setRotationPoint(7.38F, 0.46F, -47.1F);
        station_tilt_6.addChild(dummy_button_f3);
        dummy_button_f3.setTextureOffset(182, 152).addBox(-26.08F, 10.64F, -33.4F, 12.0F, 4.0F, 4.0F, 0.0F, false);
        dummy_button_f3.setTextureOffset(141, 154).addBox(-21.58F, 11.14F, -34.85F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        dummy_button_f3.setTextureOffset(146, 160).addBox(-25.38F, 11.14F, -34.1F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        dummy_button_f3.setTextureOffset(137, 159).addBox(-17.88F, 11.14F, -34.5F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_button_f4 = new ModelRenderer(this);
        dummy_button_f4.setRotationPoint(7.38F, 0.46F, -47.1F);
        station_tilt_6.addChild(dummy_button_f4);
        dummy_button_f4.setTextureOffset(182, 152).addBox(-1.08F, 0.64F, -33.4F, 12.0F, 4.0F, 4.0F, 0.0F, false);
        dummy_button_f4.setTextureOffset(141, 154).addBox(3.42F, 1.14F, -34.1F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        dummy_button_f4.setTextureOffset(146, 160).addBox(-0.38F, 1.14F, -34.1F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        glow_f4 = new LightModelRenderer(this);
        glow_f4.setRotationPoint(10.62F, -2.46F, 47.1F);
        dummy_button_f4.addChild(glow_f4);
        glow_f4.setTextureOffset(176, 19).addBox(-3.5F, 3.6F, -81.6F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_button_f5 = new ModelRenderer(this);
        dummy_button_f5.setRotationPoint(7.38F, 0.46F, -47.1F);
        station_tilt_6.addChild(dummy_button_f5);
        dummy_button_f5.setTextureOffset(182, 152).addBox(-1.08F, 5.64F, -33.4F, 12.0F, 4.0F, 4.0F, 0.0F, false);
        dummy_button_f5.setTextureOffset(141, 154).addBox(3.42F, 6.14F, -34.1F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        dummy_button_f5.setTextureOffset(146, 160).addBox(-0.38F, 6.14F, -34.6F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        dummy_button_f5.setTextureOffset(137, 159).addBox(7.12F, 6.14F, -34.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        dummy_button_f6 = new ModelRenderer(this);
        dummy_button_f6.setRotationPoint(7.38F, 0.46F, -47.1F);
        station_tilt_6.addChild(dummy_button_f6);
        dummy_button_f6.setTextureOffset(182, 152).addBox(-1.08F, 10.64F, -33.4F, 12.0F, 4.0F, 4.0F, 0.0F, false);
        dummy_button_f6.setTextureOffset(146, 160).addBox(-0.38F, 11.14F, -34.1F, 3.0F, 3.0F, 4.0F, 0.0F, false);
        dummy_button_f6.setTextureOffset(137, 159).addBox(7.12F, 11.14F, -34.25F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        glow_f6 = new LightModelRenderer(this);
        glow_f6.setRotationPoint(10.62F, -2.46F, 47.1F);
        dummy_button_f6.addChild(glow_f6);
        glow_f6.setTextureOffset(190, 16).addBox(-7.2F, 13.6F, -81.2F, 3.0F, 3.0F, 4.0F, 0.0F, false);

        coms = new ModelRenderer(this);
        coms.setRotationPoint(0.0F, 0.0F, 0.0F);
        side6.addChild(coms);
        coms.setTextureOffset(182, 152).addBox(-7.0F, -99.0F, -33.0F, 14.0F, 3.0F, 3.0F, 0.0F, false);
        coms.setTextureOffset(80, 148).addBox(7.0F, -100.0F, -34.0F, 2.0F, 31.0F, 5.0F, 0.0F, false);
        coms.setTextureOffset(83, 141).addBox(-9.0F, -100.0F, -34.0F, 2.0F, 31.0F, 5.0F, 0.0F, false);

        bell_rotate_x = new ModelRenderer(this);
        bell_rotate_x.setRotationPoint(0.0F, -96.0F, -32.0F);
        coms.addChild(bell_rotate_x);
        bell_rotate_x.setTextureOffset(197, 89).addBox(-6.0F, 12.0F, -6.0F, 12.0F, 2.0F, 12.0F, 0.0F, false);
        bell_rotate_x.setTextureOffset(122, 155).addBox(-5.4F, 12.4F, -5.4F, 11.0F, 2.0F, 11.0F, 0.0F, false);
        bell_rotate_x.setTextureOffset(193, 99).addBox(-4.0F, 2.0F, -4.0F, 8.0F, 10.0F, 8.0F, 0.0F, false);
        bell_rotate_x.setTextureOffset(207, 100).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        time_rotor_slide_y = new ModelRenderer(this);
        time_rotor_slide_y.setRotationPoint(0.0F, -58.0F, 0.0F);
        

        rotor_plate = new ModelRenderer(this);
        rotor_plate.setRotationPoint(0.0F, 89.0F, 0.0F);
        time_rotor_slide_y.addChild(rotor_plate);
        rotor_plate.setTextureOffset(24, 109).addBox(-16.25F, -89.6F, -8.0F, 33.0F, 2.0F, 16.0F, 0.0F, false);
        rotor_plate.setTextureOffset(59, 113).addBox(-6.25F, -98.6F, -6.0F, 12.0F, 3.0F, 12.0F, 0.0F, false);
        rotor_plate.setTextureOffset(59, 113).addBox(-6.25F, -108.6F, -6.0F, 12.0F, 3.0F, 12.0F, 0.0F, false);
        rotor_plate.setTextureOffset(26, 112).addBox(-12.75F, -89.6F, -17.0F, 26.0F, 2.0F, 9.0F, 0.0F, false);
        rotor_plate.setTextureOffset(46, 112).addBox(-13.75F, -89.6F, 8.0F, 27.0F, 2.0F, 8.0F, 0.0F, false);

        rotor_post_1 = new ModelRenderer(this);
        rotor_post_1.setRotationPoint(0.0F, -21.6F, 0.0F);
        time_rotor_slide_y.addChild(rotor_post_1);
        rotor_post_1.setTextureOffset(198, 57).addBox(-2.0F, -20.0F, 9.0F, 4.0F, 40.0F, 4.0F, 0.0F, false);
        rotor_post_1.setTextureOffset(198, 57).addBox(-2.0F, -20.0F, -13.0F, 4.0F, 40.0F, 4.0F, 0.0F, false);

        rotor_post_2 = new ModelRenderer(this);
        rotor_post_2.setRotationPoint(0.0F, -21.6F, 0.0F);
        time_rotor_slide_y.addChild(rotor_post_2);
        setRotationAngle(rotor_post_2, 0.0F, -1.0472F, 0.0F);
        rotor_post_2.setTextureOffset(198, 57).addBox(-2.0F, -20.0F, 9.0F, 4.0F, 40.0F, 4.0F, 0.0F, false);
        rotor_post_2.setTextureOffset(198, 57).addBox(-2.0F, -20.0F, -13.0F, 4.0F, 40.0F, 4.0F, 0.0F, false);

        rotor_post_3 = new ModelRenderer(this);
        rotor_post_3.setRotationPoint(0.0F, -21.6F, 0.0F);
        time_rotor_slide_y.addChild(rotor_post_3);
        setRotationAngle(rotor_post_3, 0.0F, -2.0944F, 0.0F);
        rotor_post_3.setTextureOffset(198, 57).addBox(-2.0F, -20.0F, 9.0F, 4.0F, 40.0F, 4.0F, 0.0F, false);
        rotor_post_3.setTextureOffset(198, 57).addBox(-2.0F, -20.0F, -13.0F, 4.0F, 40.0F, 4.0F, 0.0F, false);

        controls_hitbox = new ModelRenderer(this);
        controls_hitbox.setRotationPoint(0.0F, 31.0F, 0.0F);
        

        inc_hitbox = new ModelRenderer(this);
        inc_hitbox.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_hitbox.addChild(inc_hitbox);
        inc_hitbox.setTextureOffset(234, 245).addBox(-15.0F, -85.0F, -68.0F, 30.0F, 27.0F, 11.0F, 0.0F, false);

        x_hitbox = new ModelRenderer(this);
        x_hitbox.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_hitbox.addChild(x_hitbox);
        x_hitbox.setTextureOffset(234, 245).addBox(-12.0F, -85.0F, -36.0F, 8.0F, 7.0F, 8.0F, 0.0F, false);

        y_hitbox = new ModelRenderer(this);
        y_hitbox.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_hitbox.addChild(y_hitbox);
        y_hitbox.setTextureOffset(234, 245).addBox(-4.0F, -86.0F, -32.0F, 8.0F, 7.0F, 8.0F, 0.0F, false);

        z_hitbox = new ModelRenderer(this);
        z_hitbox.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_hitbox.addChild(z_hitbox);
        z_hitbox.setTextureOffset(234, 245).addBox(4.25F, -85.0F, -36.0F, 8.0F, 7.0F, 8.0F, 0.0F, false);

        randomizer_hitbox = new ModelRenderer(this);
        randomizer_hitbox.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_hitbox.addChild(randomizer_hitbox);
        randomizer_hitbox.setTextureOffset(234, 245).addBox(22.0F, -89.0F, -22.0F, 11.0F, 11.0F, 11.0F, 0.0F, false);

        facing_hitbox = new ModelRenderer(this);
        facing_hitbox.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_hitbox.addChild(facing_hitbox);
        facing_hitbox.setTextureOffset(234, 245).addBox(28.0F, -79.0F, -40.0F, 11.0F, 11.0F, 11.0F, 0.0F, false);

        landing_hitbox = new ModelRenderer(this);
        landing_hitbox.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_hitbox.addChild(landing_hitbox);
        landing_hitbox.setTextureOffset(234, 245).addBox(42.0F, -79.0F, -18.0F, 11.0F, 11.0F, 11.0F, 0.0F, false);

        refuel_hitbox = new ModelRenderer(this);
        refuel_hitbox.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_hitbox.addChild(refuel_hitbox);
        refuel_hitbox.setTextureOffset(234, 245).addBox(45.0F, -75.0F, 5.0F, 11.0F, 11.0F, 11.0F, 0.0F, false);

        throttle_hitbox = new ModelRenderer(this);
        throttle_hitbox.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_hitbox.addChild(throttle_hitbox);
        throttle_hitbox.setTextureOffset(234, 245).addBox(48.0F, -85.0F, 22.0F, 4.0F, 18.0F, 4.0F, 0.0F, false);
        throttle_hitbox.setTextureOffset(234, 245).addBox(43.0F, -85.0F, 20.0F, 6.0F, 18.0F, 4.0F, 0.0F, false);
        throttle_hitbox.setTextureOffset(234, 245).addBox(39.0F, -85.0F, 17.0F, 6.0F, 18.0F, 4.0F, 0.0F, false);
        throttle_hitbox.setTextureOffset(234, 245).addBox(35.0F, -85.0F, 15.0F, 4.0F, 18.0F, 4.0F, 0.0F, false);

        handbreak_hitbox = new ModelRenderer(this);
        handbreak_hitbox.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_hitbox.addChild(handbreak_hitbox);
        handbreak_hitbox.setTextureOffset(234, 245).addBox(43.0F, -85.0F, 28.0F, 4.0F, 18.0F, 4.0F, 0.0F, false);
        handbreak_hitbox.setTextureOffset(234, 245).addBox(38.0F, -85.0F, 26.0F, 6.0F, 18.0F, 4.0F, 0.0F, false);
        handbreak_hitbox.setTextureOffset(234, 245).addBox(34.0F, -85.0F, 23.0F, 6.0F, 18.0F, 4.0F, 0.0F, false);
        handbreak_hitbox.setTextureOffset(234, 245).addBox(30.0F, -85.0F, 21.0F, 4.0F, 18.0F, 4.0F, 0.0F, false);

        dimention_hitbox = new ModelRenderer(this);
        dimention_hitbox.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_hitbox.addChild(dimention_hitbox);
        dimention_hitbox.setTextureOffset(234, 245).addBox(-9.0F, -102.0F, 28.0F, 18.0F, 18.0F, 5.0F, 0.0F, false);

        sonic_hitbox = new ModelRenderer(this);
        sonic_hitbox.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_hitbox.addChild(sonic_hitbox);
        sonic_hitbox.setTextureOffset(234, 245).addBox(-3.0F, -79.0F, 38.0F, 6.0F, 18.0F, 8.0F, 0.0F, false);

        waypoints_hitbox = new ModelRenderer(this);
        waypoints_hitbox.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_hitbox.addChild(waypoints_hitbox);
        waypoints_hitbox.setTextureOffset(234, 245).addBox(-19.0F, -76.0F, 45.0F, 10.0F, 10.0F, 8.0F, 0.0F, false);

        fastreturn_hitbox = new ModelRenderer(this);
        fastreturn_hitbox.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_hitbox.addChild(fastreturn_hitbox);
        fastreturn_hitbox.setTextureOffset(234, 245).addBox(-38.0F, -74.0F, 38.0F, 6.0F, 8.0F, 7.0F, 0.0F, false);

        stablizers_hitbox = new ModelRenderer(this);
        stablizers_hitbox.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_hitbox.addChild(stablizers_hitbox);
        stablizers_hitbox.setTextureOffset(234, 245).addBox(-57.0F, -74.0F, 6.0F, 6.0F, 8.0F, 7.0F, 0.0F, false);

        telepathic_hitbox = new ModelRenderer(this);
        telepathic_hitbox.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_hitbox.addChild(telepathic_hitbox);
        telepathic_hitbox.setTextureOffset(234, 245).addBox(-49.0F, -86.0F, 21.0F, 10.0F, 20.0F, 9.0F, 0.0F, false);

        door_hitbox = new ModelRenderer(this);
        door_hitbox.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_hitbox.addChild(door_hitbox);
        door_hitbox.setTextureOffset(234, 245).addBox(-43.0F, -81.0F, -26.0F, 7.0F, 15.0F, 7.0F, 0.0F, false);

        coms_hitbox = new ModelRenderer(this);
        coms_hitbox.setRotationPoint(0.0F, 0.0F, 0.0F);
        controls_hitbox.addChild(coms_hitbox);
        coms_hitbox.setTextureOffset(234, 245).addBox(-33.0F, -102.0F, -16.0F, 9.0F, 36.0F, 7.0F, 0.0F, false);
        coms_hitbox.setTextureOffset(234, 245).addBox(-30.0F, -102.0F, -23.0F, 9.0F, 36.0F, 7.0F, 0.0F, false);
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void render(NemoConsoleTile tile, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
        matrixStack.push();
        tile.getControl(IncModControl.class).ifPresent(inc -> {
            this.xyz_increment_rotate_z.rotateAngleZ = (float)Math.toRadians(inc.getAnimationProgress() * 360.0);
        });
        
        tile.getControl(RefuelerControl.class).ifPresent(refuel -> {
            this.refuel_needle_rotate_z.rotateAngleZ = (float) Math.toRadians(refuel.isRefueling() ? 15 : -15);
        });
        
        tile.getControl(ThrottleControl.class).ifPresent(throttle -> {
            this.throttle_lever_rotate_x.rotateAngleX = (float) Math.toRadians(50 - (throttle.getAmount() * 100));
        });
        
        tile.getControl(HandbrakeControl.class).ifPresent(handbrake -> {
            this.handbreak_lever2_rotate_x.rotateAngleX = (float) Math.toRadians(handbrake.isFree() ? 50 : -50);
        });
        
        tile.getControl(FacingControl.class).ifPresent(facing -> {

            float angle = WorldHelper.getAngleFromFacing(facing.getDirection()) + 45;
            
            angle -= facing.getAnimationProgress() * 360.0;
            
            this.faceing_needle_rotate_z.rotateAngleZ = (float) Math.toRadians(angle);
        });
        
        tile.getControl(RandomiserControl.class).ifPresent(rand -> {
            this.rotate_y.rotateAngleY = (float)Math.toRadians(rand.getAnimationProgress() * 720.0);
        });
        
        tile.getControl(LandingTypeControl.class).ifPresent(landing -> {
            this.rotate_bar_x.rotateAngleX = (float) Math.toRadians(landing.getLandType() == EnumLandType.UP ? 0 : 2);
        });
        
        tile.getSubsystem(EnumSubsystemType.STABILIZER).ifPresent(sys -> {
            this.stablizer_button_rotate_x2.rotateAngleX = (float)Math.toRadians(((IStabilizerSubsystem)sys).isControlActivated(tile) ? -1 : 0);
        });
        
        tile.getControl(XControl.class).ifPresent(x -> {
            this.glow_cord_x.setBright(x.getAnimationTicks() != 0 ? 1.0F : 0.0F);
        });
        
        tile.getControl(YControl.class).ifPresent(y -> {
            this.glow_cord_y.setBright(y.getAnimationTicks() != 0 ? 1.0F : 0.0F);
        });
        
        tile.getControl(ZControl.class).ifPresent(z -> {
            this.glow_cord_z.setBright(z.getAnimationTicks() != 0 ? 1.0F : 0.0F);
        });
        
        tile.getControl(CommunicatorControl.class).ifPresent(coms -> {
            this.bell_rotate_x.rotateAngleX = (float)Math.toRadians(Math.sin(coms.getAnimationTicks() * 0.6) * 20);
        });
        
        tile.getControl(DimensionControl.class).ifPresent(dim -> {
            float angle = (dim.getDimListIndex() / (float)dim.getAvailableDimensions()) * 360.0F;
            angle -= dim.getAnimationProgress() * 360;
            this.needle_rotate_z.rotateAngleZ = (float)Math.toRadians(angle);
        });
        
        tile.getControl(DoorControl.class).ifPresent(door -> {
            this.door_knob_rotate_z.rotateAngleZ = (float)Math.toRadians(door.getAnimationProgress() * 360);
        });
        
        this.glow_globe.setBright(1.0F);
        this.glow_dial.setBright(1.0F);
        this.glow_b1.setBright(1.0F);
        this.glow_barometer.setBright(1.0F);
        this.glow_baseoffset_1.setBright(1.0F);
        this.glow_baseoffset_2.setBright(1.0F);
        this.glow_baseoffset_3.setBright(1.0F);
        this.glow_c1.setBright(1.0F);
        this.glow_c2.setBright(1.0F);
        this.glow_d1.setBright(1.0F);
        this.glow_d2.setBright(1.0F);
        this.glow_dial.setBright(1.0F);
        this.glow_selector.setBright(1.0F);
        this.glow_timerotor_slide_y.setBright(1.0F);
        this.glow_telepathics.setBright(1.0F);
        this.glow_f6.setBright(1.0F);
        this.glow_f4.setBright(1.0F);
        this.glow_f2.setBright(1.0F);
        this.glow_f1.setBright(1.0F);
        this.glow_refuel.setBright(1.0F);
        
        glow_baseoffset_1.render(matrixStack, buffer, packedLight, packedOverlay);
        glow_baseoffset_2.render(matrixStack, buffer, packedLight, packedOverlay);
        glow_baseoffset_3.render(matrixStack, buffer, packedLight, packedOverlay);
        console.render(matrixStack, buffer, packedLight, packedOverlay);
        controls.render(matrixStack, buffer, packedLight, packedOverlay);
        
        matrixStack.push();
        matrixStack.translate(0, Math.cos((MathHelper.lerp(Minecraft.getInstance().getRenderPartialTicks(), tile.prevFlightTicks, tile.flightTicks)) * 0.1) * 0.75, 0);;
        time_rotor_slide_y.render(matrixStack, buffer, packedLight, packedOverlay);
        glow_timerotor_slide_y.render(matrixStack, buffer, packedLight, packedOverlay);
        matrixStack.pop();

        matrixStack.pop();
    }
}