package net.tardis.mod.client.renderers.entity;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.entity.TardisBackdoorEntity;

public class TardisBackdoorEntityRenderer extends EntityRenderer<TardisBackdoorEntity> {

    public TardisBackdoorEntityRenderer(EntityRendererManager renderManager) {
        super(renderManager);
    }

    @Override
    public void render(TardisBackdoorEntity entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn,
            IRenderTypeBuffer bufferIn, int packedLightIn) {
        super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
    }    

    @Override
    protected boolean canRenderName(TardisBackdoorEntity entity) {
        return false;
    }

    @Override
    public ResourceLocation getEntityTexture(TardisBackdoorEntity entity) {
        return null;
    }

}
