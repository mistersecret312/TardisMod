package net.tardis.mod.containers.slot;

import java.util.function.Predicate;

import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.items.ISubsystemType;
import net.tardis.mod.items.SubsystemItem;

public class EngineSlot extends SlotItemHandler{

	Direction panel;
	Predicate<EnumSubsystemType> filter;
	
	public EngineSlot(Direction panel, IItemHandler itemHandler, int index, int xPosition, int yPosition) {
		this(stack -> true, panel, itemHandler, index, xPosition, yPosition);
	}
	
	public EngineSlot(Predicate<EnumSubsystemType> filter, Direction panel, IItemHandler itemHandler, int index, int xPosition, int yPosition) {
		super(itemHandler, index, xPosition, yPosition);
		this.panel = panel;
		this.filter = filter;
	}

	public Direction getPanelSide() {
	    return this.panel;
	}
	
	public void setPanelSide(Direction dir) {
        this.panel = dir;
    }
	
	public void setFilter(Predicate<EnumSubsystemType> filter) {
		this.filter = filter;
	}
	
	@Override
	public boolean isItemValid(ItemStack stack) {
		if(stack.getItem() instanceof ISubsystemType) {
			return this.filter.test(((ISubsystemType) stack.getItem()).getType());
		} else return false;
	}


}
