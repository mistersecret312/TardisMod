package net.tardis.mod.sounds;

import javax.annotation.Nullable;

import net.minecraft.util.SoundEvent;
import net.minecraft.util.Util;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.registries.ForgeRegistryEntry;
/** A wrapped class around a {@linkplain SoundEvent} that allows for it to be looped based on a duration defined in the class*/
public class InteriorHum extends ForgeRegistryEntry<InteriorHum> {

    private SoundEvent event;
    private int duration;
    @Nullable
    private String translationKey;

    /**
     * @param event SoundEvent of the chosen hum
     * @param duration Duration of the sound in ticks
     */
    public InteriorHum(SoundEvent event, int duration) {
        this.event = event;
        this.duration = duration;
    }

    public SoundEvent getLoopedSoundEvent() {
        return event;
    }

    /**
     * @return Duration of the SoundEvent attr. in ticks
     */
    public int getDurationInTicks() {
        return duration;
    }
    
    public String getTranslationKey() {
    	if (this.translationKey == null) {
    		this.translationKey = Util.makeTranslationKey("hum", this.getRegistryName());
    	}
    	return this.translationKey;
    }

    public TranslationTextComponent getTranslatedName() {
        return new TranslationTextComponent(this.getTranslationKey());
    }
}

