package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.TardisConstants.Translations;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.items.TItems;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class RefuelerControl extends BaseControl{

	private static final TranslationTextComponent ON = new TranslationTextComponent("message." + Tardis.MODID + ".control.refuel.true");
	private static final TranslationTextComponent OFF = new TranslationTextComponent("message." + Tardis.MODID + ".control.refuel.false");

	private boolean isRefueling = false;
	
	public RefuelerControl(ControlEntry entry, ConsoleTile console, ControlEntity entity) {
		super(entry, console, entity);
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(3 / 16.0F, 3 / 16.0F);

		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.0825F, 0.0825F);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		return EntitySize.flexible(4 / 16.0F, 4 / 16.0F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote()) {
			console.getSubsystem(EnumSubsystemType.FLUIDLINK).ifPresent(link -> {
				if(link.canBeUsed(console)) {
					this.isRefueling = !this.isRefueling;
					console.updateClient();
					player.sendStatusMessage(this.isRefueling ? ON : OFF, true);
					this.startAnimation();
				}
				else {
					ItemStack name = new ItemStack(TItems.FLUID_LINK.get());
					player.sendStatusMessage(new TranslationTextComponent(Translations.NO_COMPONENT, name.getDisplayName().getString()), true);
				}
			});
		}
		return true;
	}

	@Override
	public Vector3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vector3d(4 / 16.0, 6 / 16.0, -12 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vector3d(0.813087980367591, 0.375, 0.19558907625845867);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vector3d(0.2303583465628734, 0.55, -0.4);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return new Vector3d(-0.55, 0.438, -0.442);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vector3d(0.002, 0.45, -0.754);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return new Vector3d(0.9418336762858615, 0.375, 0.015390332810151897);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vector3d(0.8955769767178283, 0.375, -0.46198242608214435);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vector3d(0.28281338270450995, 0.46875, 0.756872738705749);
		
		return new Vector3d(5.5 / 16.0, 6 / 16.0, -16 / 16.0);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return isRefueling ? TSounds.REFUEL_START.get() : TSounds.REFUEL_STOP.get();
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putBoolean("is_fueling", this.isRefueling);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.isRefueling = nbt.getBoolean("is_fueling");
	}
	
	public boolean isRefueling() {
		return this.isRefueling;
	}
	
	public void setRefueling(boolean refuel) {
		this.isRefueling = refuel;
		this.markDirty();
	}

}
