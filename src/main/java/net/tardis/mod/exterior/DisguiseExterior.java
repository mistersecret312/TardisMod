package net.tardis.mod.exterior;

import java.util.function.Supplier;

import net.minecraft.block.BlockState;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.misc.DoorSounds;
import net.tardis.mod.misc.IDoorType;
import net.tardis.mod.misc.ObjectWrapper;
import net.tardis.mod.subsystem.IChameleonSubsystem;
import net.tardis.mod.tileentities.ConsoleTile;

public class DisguiseExterior extends TwoBlockBasicExterior{

	public DisguiseExterior(Supplier<BlockState> state, boolean inChameleon, IDoorType type, ResourceLocation blueprint) {
		super(state, inChameleon, type, DoorSounds.BASE, blueprint);
	}

	@Override
	public int getWidth(ConsoleTile console) {
		ObjectWrapper<Integer> width = new ObjectWrapper<Integer>(super.getWidth(console));
		console.getSubsystem(EnumSubsystemType.CHAMELEON).ifPresent(sys -> {
			if(((IChameleonSubsystem)sys).isActiveCamo(false, console)) { //Check if this is the disguise exterior. We don't care if the chameleon subsystem is able to be used or not, so this will correctly calculate value for when the user intentionally disables the subsystem to use a particular disguise.
				if(console.getExteriorManager().getDisguise() != null)
					width.setValue(width.getValue() + console.getExteriorManager().getDisguise().getHeight());
			}
		});
		return width.getValue();
	}

	@Override
	public int getHeight(ConsoleTile console) {
		ObjectWrapper<Integer> height = new ObjectWrapper<Integer>(super.getHeight(console));
		console.getSubsystem(EnumSubsystemType.CHAMELEON).ifPresent(sys -> {
			if(((IChameleonSubsystem)sys).isActiveCamo(false, console)) { //Check if this is the disguise exterior. We don't care if the chameleon subsystem is able to be used or not, so this will correctly calculate value for when the user intentionally disables the subsystem to use a particular disguise.
				if(console.getExteriorManager().getDisguise() != null)
					height.setValue(height.getValue() + console.getExteriorManager().getDisguise().getHeight());
			}
		});
		return height.getValue();
	}

}
