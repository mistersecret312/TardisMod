package net.tardis.mod.network.packets;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Supplier;

import com.google.common.collect.Maps;

import net.minecraft.network.PacketBuffer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.ClientPacketHandler;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.helper.TardisHelper;

public class TardisNameGuiMessage {

	//Stolen from PacketBuffer#readString();
	private static final int STRING_SIZE = 32767;
	private Map<ResourceLocation, String> names = Maps.newHashMap();
	
	public TardisNameGuiMessage(Map<ResourceLocation, String> names) {
		this.names.clear();
		this.names.putAll(names);
	}
	
	public static TardisNameGuiMessage create(MinecraftServer server) {
		HashMap<ResourceLocation, String> map = Maps.newHashMap();
		for(ServerWorld world : TardisHelper.getTardises(server)) {
			TardisHelper.getConsoleInWorld(world).ifPresent(tile -> {
				world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
					tile.getSubsystem(EnumSubsystemType.ANTENNA).ifPresent(sys -> {
						if(sys.canBeUsed(tile)) {
							map.put(world.getDimensionKey().getLocation(), data.getTARDISName());
						}
				    });
				});
				
			});
		}
		return new TardisNameGuiMessage(map);
	}
	
	public static void encode(TardisNameGuiMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.names.size());
		for(Entry<ResourceLocation, String> e : mes.names.entrySet()) {
			buf.writeResourceLocation(e.getKey());
			buf.writeString(e.getValue(), STRING_SIZE);
		}
	}
	
	public static TardisNameGuiMessage decode(PacketBuffer buf) {
		int size = buf.readInt();
		Map<ResourceLocation, String> names = Maps.newHashMap();
	    for(int i = 0; i < size; ++i) {
		    names.put(buf.readResourceLocation(), buf.readString(STRING_SIZE));
        }
		
		return new TardisNameGuiMessage(names);
	}
	
	public static void handle(TardisNameGuiMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> ClientPacketHandler.handleClientSideNames(mes));
		context.get().setPacketHandled(true);
	}
	
	public Map<ResourceLocation, String> getNames(){
		return names;
	}
}
