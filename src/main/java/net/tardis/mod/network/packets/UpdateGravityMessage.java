package net.tardis.mod.network.packets;

import io.netty.buffer.ByteBuf;
import net.minecraft.util.Direction;
import net.minecraft.util.datafix.fixes.ItemRename;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.ClientPacketHandler;
import net.tardis.mod.items.TItems;
import net.tardis.mod.tileentities.inventory.PanelInventory;

import java.util.function.Supplier;

public class UpdateGravityMessage {

    final float gravPercent;
    public UpdateGravityMessage(float sliderValue) {
        this.gravPercent = sliderValue;
    }

    public static void encode(UpdateGravityMessage mes, ByteBuf buf){
        buf.writeFloat(mes.gravPercent);
    }

    public static UpdateGravityMessage decode(ByteBuf buf){
        return new UpdateGravityMessage(buf.readFloat());
    }

    public static void handle(UpdateGravityMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {
            if(context.get().getDirection() == NetworkDirection.PLAY_TO_SERVER){
                context.get().getSender().getServerWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(tardis -> {

                    boolean hasUpgrade = false;
                    final PanelInventory inv = tardis.getEngineInventoryForSide(Direction.SOUTH);
                    for(int i = 0; i < inv.getSlots(); ++i){
                        if(inv.getStackInSlot(i).getItem() == TItems.GRAVITY_CONTROL_UPGRADE.get()){
                            hasUpgrade = true;
                            break;
                        }
                    }

                    tardis.setGravPercent(hasUpgrade ? mes.gravPercent : 1);
                });
            }
            else ClientPacketHandler.handleGravityMessage(mes.gravPercent);
        });
        context.get().setPacketHandled(true);
    }
}
