package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import javax.annotation.Nullable;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.console.misc.DistressSignal;

public class SendTardisDistressSignal {

	String message;
	ResourceLocation tardis;
	
	public SendTardisDistressSignal(String message, @Nullable ResourceLocation tardis) {
		this.message = message;
		this.tardis = tardis;
	}
	
	public static void encode(SendTardisDistressSignal mes, PacketBuffer buf) {
		int size = mes.message.length();
		buf.writeInt(size);
		buf.writeString(mes.message, size);
		
		boolean hasID = mes.tardis != null;
		buf.writeBoolean(hasID);
		
		if(hasID)
			buf.writeResourceLocation(mes.tardis);
		
	}
	
	public static SendTardisDistressSignal decode(PacketBuffer buf) {
		int size = buf.readInt();
		String message = buf.readString(size);
		
		ResourceLocation id = null;
		boolean hasID = buf.readBoolean();
		if(hasID)
			id = buf.readResourceLocation();
		
		return new SendTardisDistressSignal(message, id);
	}
	
	public static void handle(SendTardisDistressSignal mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			//If in TARDIS world
			TardisHelper.getConsoleInWorld(cont.get().getSender().getServerWorld()).ifPresent(tile -> {
				
				DistressSignal signal = new DistressSignal(mes.message, tile.getPositionInFlight());
				
				//If antenna has health
				tile.getSubsystem(EnumSubsystemType.ANTENNA).ifPresent(sys -> {
					if(sys.canBeUsed(tile)) {
						ResourceLocation tardis = mes.tardis;
						if(tardis == null) {
							//If null, send to all TARDISes
							for(ServerWorld world : TardisHelper.getTardises(cont.get().getSender().getServer())) {
								TardisHelper.getConsoleInWorld(world).ifPresent(other -> {
									other.addDistressSignal(signal);
								});
							}
						}
						else {
							//if Directed at a specific TARDIS
							TardisHelper.getConsole(cont.get().getSender().getServer(), tardis).ifPresent(other -> {
								other.addDistressSignal(signal);
							});
						}
					}
				});
			});
		});
		cont.get().setPacketHandled(true);
	}
}
