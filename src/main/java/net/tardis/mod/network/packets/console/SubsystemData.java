package net.tardis.mod.network.packets.console;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent.Context;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.tileentities.ConsoleTile;

public class SubsystemData implements ConsoleData{

	private boolean canBeUsed;
	private EnumSubsystemType key;
	private boolean activated;
	
	public SubsystemData(EnumSubsystemType type, boolean canBeUsed, boolean activated) {
		this.key = type;
		this.canBeUsed = canBeUsed;
		this.activated = activated;
	}
	
	@Override
	public void applyToConsole(ConsoleTile tile, Supplier<Context> context) {
		tile.getSubsystem(key).ifPresent(sys -> {
			sys.setActive(activated);
			sys.setCanBeUsed(canBeUsed);
		});
	}

	@Override
	public void serialize(PacketBuffer buff) {
		buff.writeEnumValue(key);
		buff.writeBoolean(canBeUsed);
		buff.writeBoolean(this.activated);
	}

	@Override
	public void deserialize(PacketBuffer buff) {
		this.key = buff.readEnumValue(EnumSubsystemType.class);
		this.canBeUsed = buff.readBoolean();
		this.activated = buff.readBoolean();
	}

	@Override
	public DataType getDataType() {
		return DataTypes.SUBSYSTEM;
	}

}
