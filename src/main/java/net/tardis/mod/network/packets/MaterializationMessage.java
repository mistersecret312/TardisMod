package net.tardis.mod.network.packets;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.client.ClientPacketHandler;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.network.Network;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

import java.util.function.Supplier;

public class MaterializationMessage {

    public EnumMatterState state;
    public BlockPos pos;
    public int materializeTime;
    public int maxMaterializeTime;

    public MaterializationMessage(BlockPos pos, EnumMatterState matterState, int materializeTime, int maxMaterializeTime) {
        this.pos = pos;
        this.state = matterState;
        this.materializeTime = materializeTime;
        this.maxMaterializeTime = maxMaterializeTime;
    }

    public static void encode(MaterializationMessage mes, PacketBuffer buf){
        buf.writeBlockPos(mes.pos);
        buf.writeEnumValue(mes.state);
        buf.writeInt(mes.materializeTime);
        buf.writeInt(mes.maxMaterializeTime);
    }

    public static MaterializationMessage decode(PacketBuffer buf){
        return new MaterializationMessage(buf.readBlockPos(), buf.readEnumValue(EnumMatterState.class), buf.readInt(), buf.readInt());
    }

    public static void handle(MaterializationMessage mes, Supplier<NetworkEvent.Context> cont){
        cont.get().enqueueWork(() -> ClientPacketHandler.handleMaterializationPacket(mes.pos, mes.state, mes.materializeTime, mes.maxMaterializeTime));
        cont.get().setPacketHandled(true);
    }
}
