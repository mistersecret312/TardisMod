package net.tardis.mod.network.packets;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import com.mojang.serialization.Codec;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTDynamicOps;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.misc.TardisLike;

public class TardisLikeSyncMessage {
    
	private Map<ResourceLocation, TardisLike> likes = new HashMap<ResourceLocation, TardisLike>();
	//We use an unboundedMapCodec. However it is limited in that it can only parse objects whose keys can be serialised to a string, such as ResourceLocation
	//E.g. If you used an int as a key, the unboundedMapCodec will not parse it and will error.
	public static final Codec<Map<ResourceLocation, TardisLike>> MAPPER = Codec.unboundedMap(ResourceLocation.CODEC, TardisLike.getCodec());
	
	
	public TardisLikeSyncMessage(Map<ResourceLocation, TardisLike> likes) {
		this.likes.clear(); //Clear the client's list and readd the entries from the server
        this.likes.putAll(likes);
	}

	public static void encode(TardisLikeSyncMessage mes, PacketBuffer buf) {
		buf.writeCompoundTag((CompoundNBT)(MAPPER.encodeStart(NBTDynamicOps.INSTANCE, mes.likes).result().orElse(new CompoundNBT())));
	}
	
	public static TardisLikeSyncMessage decode(PacketBuffer buf) {
		//Parse our Map Codec and send the nbt data over. If there's any errors, populate with default Tardis Mod console rooms
		return new TardisLikeSyncMessage(MAPPER.parse(NBTDynamicOps.INSTANCE, buf.readCompoundTag()).result().orElse(TardisLike.registerCoreEntries()));
	}
	
	public static void handle(TardisLikeSyncMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			TardisLike.DATA_LOADER.setData(mes.likes); //Set the ConsoleRoom's Registry to that of the parsed in list
		});
		cont.get().setPacketHandled(true);
	}

}
