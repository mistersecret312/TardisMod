package net.tardis.mod.network.packets;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.schematics.Schematic;
import net.tardis.mod.schematics.Schematics;

public class SchematicSyncMessage {
	
    private Map<ResourceLocation, Schematic> schematics = new HashMap<ResourceLocation, Schematic>();
	
	public SchematicSyncMessage(Map<ResourceLocation, Schematic> schematics) {
        this.schematics = schematics;
	}
	
	public static void encode(SchematicSyncMessage mes, PacketBuffer buf) {
		Schematic.encodeToPacket(mes.schematics, buf);
	}
	
	public static SchematicSyncMessage decode(PacketBuffer buf) {
		return new SchematicSyncMessage(Schematic.decodeFromPacket(buf));
	}
	
	public static void handle(SchematicSyncMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			Schematics.SCHEMATIC_REGISTRY = mes.schematics;
		});
		cont.get().setPacketHandled(true);
	}

}