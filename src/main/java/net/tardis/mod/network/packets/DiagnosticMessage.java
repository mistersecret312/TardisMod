package net.tardis.mod.network.packets;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.ITardisWorldData;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.contexts.gui.GuiContextDiagnostic;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.subsystem.SubsystemInfo;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.ArtronUse;

public class DiagnosticMessage {
	
	private List<SubsystemInfo> data;
	private SpaceTimeCoord location;
	private List<ArtronUseInfo> uses;
	private ForgeEnergyInfo forgeEnergy;
	
	public DiagnosticMessage(List<SubsystemInfo> data, SpaceTimeCoord location, List<ArtronUseInfo> uses, ForgeEnergyInfo energy) {
		this.data = data;
		this.location = location;
		this.uses = uses;
		this.forgeEnergy = energy;
	}

	public static DiagnosticMessage createFromConsole(ConsoleTile console){
		ArrayList<SubsystemInfo> infos = Lists.newArrayList();
		EnumSubsystemType[] enumValues = EnumSubsystemType.values();
		for(int i = 0; i< enumValues.length-2; i++){
			console.getSubsystem(enumValues[i]).ifPresent(sys -> {
				infos.add(new SubsystemInfo(sys.getItem(console)));
			});
		}

		List<ArtronUseInfo> uses = Lists.newArrayList();
		for (ArtronUse use : console.getArtronUses().values()) {
			if (use.isActive()) {
				uses.add(new ArtronUseInfo(use));
			}
		}
		ITardisWorldData cap = console.getWorld().getCapability(Capabilities.TARDIS_DATA).orElse(null);
		int currentEnergyStored = 0;
		int maxEnergyCapacity = 0;
		if (cap != null) {
			currentEnergyStored = cap.getEnergyCap().getEnergyStored();
			maxEnergyCapacity = cap.getEnergyCap().getMaxEnergyStored();
		}
		
		ForgeEnergyInfo forgeEnergy = new ForgeEnergyInfo(currentEnergyStored, maxEnergyCapacity);
		return new DiagnosticMessage(infos, console.getPositionInFlight(), uses, forgeEnergy);
	}
	
	public static void encode(DiagnosticMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.data.size());
		
		for(SubsystemInfo info : mes.data) {
			buf.writeCompoundTag(info.serializeNBT());
		}
		
		buf.writeCompoundTag(mes.location.serialize());
		
		buf.writeInt(mes.uses.size());
		for(ArtronUseInfo use : mes.uses) {
			use.encode(buf);
		}
		
		mes.forgeEnergy.encode(buf);
		
	}
	
	public static DiagnosticMessage decode(PacketBuffer buf) {
		
		List<SubsystemInfo> list = new ArrayList<>();
		
		int size = buf.readInt();
		
		for(int i = 0; i < size; ++i) {
			list.add(new SubsystemInfo(buf.readCompoundTag()));
		}
		
		SpaceTimeCoord coord = SpaceTimeCoord.deserialize(buf.readCompoundTag());
		
		List<ArtronUseInfo> uses = Lists.newArrayList();
		int useSize = buf.readInt();
		for(int i = 0; i < useSize; ++i) {
			uses.add(ArtronUseInfo.decode(buf));
		}

		ForgeEnergyInfo info = ForgeEnergyInfo.decode(buf);
		return new DiagnosticMessage(list, coord, uses, info);
		
	}
	
	public static void handle(DiagnosticMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			ClientHelper.openGUI(TardisConstants.Gui.DIAGNOSTIC, new GuiContextDiagnostic(mes.data, mes.location, mes.uses, mes.forgeEnergy));
		});
		context.get().setPacketHandled(true);
	}
	
	public static class ArtronUseInfo{
		public TranslationTextComponent key;
		public float use;
		
		public ArtronUseInfo(ArtronUse use) {
			this(use.getType().getTranslation().getKey(), use.getArtronUsePerTick());
		}
		
		public ArtronUseInfo(String key, float use) {
			this.key = new TranslationTextComponent(key);
			this.use = use;
		}
		
		public void encode(PacketBuffer buf) {
			int size = this.key.getKey().length();
			buf.writeInt(size);
			buf.writeString(this.key.getKey(), size);
			buf.writeFloat(this.use);
		}
		
		public static ArtronUseInfo decode(PacketBuffer buf){
			int size = buf.readInt();
			String key = buf.readString(size);
			float use = buf.readFloat();
			return new ArtronUseInfo(key, use);
		}
	}
	
	public static class ForgeEnergyInfo{
		public int currentEnergyStored;
		public int maxEnergyCapacity;
		
		public ForgeEnergyInfo(int currentEnergyStored, int maxEnergyCapacity){
			this.currentEnergyStored = currentEnergyStored;
			this.maxEnergyCapacity = maxEnergyCapacity;
		}
		
		public void encode(PacketBuffer buf) {
			buf.writeInt(this.currentEnergyStored);
			buf.writeInt(this.maxEnergyCapacity);
		}
		
		public static ForgeEnergyInfo decode(PacketBuffer buf){
			int energyStored = buf.readInt();
			int maxEnergyCapacity = buf.readInt();
			return new ForgeEnergyInfo(energyStored, maxEnergyCapacity);
		}
	}

}
