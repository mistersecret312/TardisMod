package net.tardis.mod.network.packets;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.registries.SonicModeRegistry;
import net.tardis.mod.sonic.AbstractSonicMode;

import java.util.function.Supplier;

public class SonicModeChangeMessage {

    private AbstractSonicMode mode;
    private int entityID;

    public SonicModeChangeMessage(AbstractSonicMode mode, int entityID){
        this.mode = mode;
        this.entityID = entityID;
    }

    public static void encode(SonicModeChangeMessage mes, PacketBuffer buf){
        buf.writeRegistryId(mes.mode);
        buf.writeInt(mes.entityID);
    }

    public static SonicModeChangeMessage decode(PacketBuffer buf){
        return new SonicModeChangeMessage(buf.readRegistryId(), buf.readInt());
    }

    public static void handle(SonicModeChangeMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {
            Entity entity = context.get().getSender().getEntityWorld().getEntityByID(mes.entityID);
            if(entity instanceof LivingEntity){
                LivingEntity liv = (LivingEntity) entity;
                setModeToSonic(liv.getHeldItemMainhand(), mes.mode);
                setModeToSonic(liv.getHeldItemOffhand(), mes.mode);
            }
        });
        context.get().setPacketHandled(true);
    }

    public static void setModeToSonic(ItemStack stack, AbstractSonicMode entry){
        stack.getCapability(Capabilities.SONIC_CAPABILITY).ifPresent(cap -> {
            cap.setMode(entry);
            SonicItem.syncCapability(stack);
        });
    }

}
