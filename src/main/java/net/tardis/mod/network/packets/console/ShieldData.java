package net.tardis.mod.network.packets.console;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent.Context;
import net.tardis.mod.tileentities.ConsoleTile;

import java.util.function.Supplier;

public class ShieldData implements ConsoleData{

	public boolean hasShield = false;

	public ShieldData(boolean hasShield) {
		this.hasShield = hasShield;
	}
	
	@Override
	public void applyToConsole(ConsoleTile tile, Supplier<Context> context) {
		tile.setHasShield(this.hasShield);
	}

	@Override
	public void serialize(PacketBuffer buf) {
		buf.writeBoolean(hasShield);
	}

	@Override
	public void deserialize(PacketBuffer buf) {
		this.hasShield = buf.readBoolean();
	}

	@Override
	public DataType getDataType() {
		return DataTypes.NAV_COM;
	}

}
