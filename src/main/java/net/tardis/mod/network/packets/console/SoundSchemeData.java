package net.tardis.mod.network.packets.console;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent.Context;
import net.tardis.mod.registries.SoundSchemeRegistry;
import net.tardis.mod.sounds.AbstractSoundScheme;
import net.tardis.mod.tileentities.ConsoleTile;

public class SoundSchemeData implements ConsoleData{

	private AbstractSoundScheme soundScheme;
	
	public SoundSchemeData(AbstractSoundScheme soundScheme) {
		this.soundScheme = soundScheme;
	}
	
	public SoundSchemeData() {
		this(SoundSchemeRegistry.BASIC.get());
	}
	
	@Override
	public void applyToConsole(ConsoleTile tile, Supplier<Context> context) {
		tile.setSoundScheme(this.soundScheme);
	}

	@Override
	public void serialize(PacketBuffer buff) {
		buff.writeResourceLocation(this.soundScheme.getRegistryName());
	}

	@Override
	public void deserialize(PacketBuffer buff) {
		this.soundScheme = SoundSchemeRegistry.SOUND_SCHEME_REGISTRY.get().getValue(buff.readResourceLocation());
	}

	@Override
	public DataType getDataType() {
		return DataTypes.SOUND_SCHEME;
	}

}
