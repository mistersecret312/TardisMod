package net.tardis.mod.protocols;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.items.SubsystemItem;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.misc.ObjectWrapper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.network.packets.console.DataTypes;
import net.tardis.mod.network.packets.console.ForcefieldData;
import net.tardis.mod.tileentities.ConsoleTile;

public class ForcefieldProtocol extends Protocol {
	
	public static final TranslationTextComponent TRANS_BROKEN = new TranslationTextComponent("protocol.tardis.forcefield_broken");
	public static final TranslationTextComponent TRANS_ON = new TranslationTextComponent("protocol.tardis.forcefield_on");
	public static final TranslationTextComponent TRANS_OFF = new TranslationTextComponent("protocol.tardis.forcefield_off");
	public static final TranslationTextComponent TRANS_TURNED_ON = new TranslationTextComponent("protocol.tardis.forcefield_turned_on");
	public static final TranslationTextComponent TRANS_TURNED_OFF = new TranslationTextComponent("protocol.tardis.forcefield_turned_off");
	
	@Override
	public void call(World world, PlayerEntity playerIn, ConsoleTile console) {
		if (!world.isRemote) {
		    console.getSubsystem(EnumSubsystemType.SHIELD).ifPresent(shield -> {
		    	AxisAlignedBB box = new AxisAlignedBB(console.getPos()).grow(16);
				if (shield.isActive() && shield.canBeUsed(console)) {
					console.setForceFieldState(!console.getForceFieldState());
					Network.sendToAllAround(new ConsoleUpdateMessage(DataTypes.FORCEFIELD, new ForcefieldData(shield.isActive())), playerIn.world.getDimensionKey(), playerIn.getPosition(), 64);
					for(PlayerEntity player : world.getEntitiesWithinAABB(PlayerEntity.class, box)) {
						player.sendStatusMessage(console.getForceFieldState() ? TRANS_TURNED_ON : TRANS_TURNED_OFF, true);
					}
				}
				else {
					for(PlayerEntity player : world.getEntitiesWithinAABB(PlayerEntity.class, box)) {
						if (shield.getItem(console).isEmpty() && !shield.isActive()) {
							player.sendStatusMessage(TRANS_BROKEN, true);
						}
						if (!shield.getItem(console).isEmpty() && !shield.isActive()) {
							String transKey = Helper.getSubSystemNotActivatedMessage(shield.getRegistryName().getPath());
							player.sendStatusMessage(new TranslationTextComponent(transKey), true);
						}
					}
				}
			});
		}
	    else ClientHelper.openGUI(TardisConstants.Gui.NONE, new GuiContext());
	}

	@Override
	public TranslationTextComponent getDisplayName(ConsoleTile tile) {

		if (!tile.isHasShield()) {
			return TRANS_BROKEN;
		}

		if(tile.getForceFieldState()) {
			return TRANS_OFF;
		}

		return TRANS_ON;
	}

	@Override
	public String getSubmenu() {
		return TardisConstants.Strings.SECURITY_MENU;
	}

}
