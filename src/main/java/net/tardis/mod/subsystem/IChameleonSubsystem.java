package net.tardis.mod.subsystem;

import net.tardis.mod.tileentities.ConsoleTile;

public interface IChameleonSubsystem {
    boolean isActiveCamo(boolean checkDamage, ConsoleTile console);
}
