package net.tardis.mod.subsystem;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.items.SubsystemItem;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.SparkingLevel;

public class StabilizerSubsystemItem extends SubsystemItem implements IStabilizerSubsystem{
    private boolean activated;
    private int seconds = 0;

    public StabilizerSubsystemItem(Properties prop, EnumSubsystemType type, boolean required, boolean repairable) {
        super(prop, type, required, repairable);
    }

    @Override
    public void onFlightSecond(ConsoleTile console) {
        if(!console.getWorld().isRemote && this.isControlActivated(console)) {
            ++this.seconds;
            if(this.seconds >= 10) {
                this.seconds = 0;
                this.damage((ServerPlayerEntity)console.getPilot(), 1, console);
            }
        }

    }
    /** If if this stabiliser control is enabled*/
    @Override
    public boolean isControlActivated(ConsoleTile console) {
        if(!this.canBeUsed(console))
            this.activated = false;

        return this.activated;
    }
    /** Set the stabiliser to be turned on*/
    @Override
    public void setControlActivated(boolean b, ConsoleTile console) {
        if(!this.canBeUsed(console))
            this.activated = false;
        else this.activated = b;
        console.updateClient();
    }

    @Override
    public SparkingLevel getSparkState() {
        return SparkingLevel.NONE;
    }

    @Override
    public boolean stopsFlight(ConsoleTile console) {
        return false;
    }
}
