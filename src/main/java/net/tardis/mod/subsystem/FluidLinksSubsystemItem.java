package net.tardis.mod.subsystem;

import net.minecraft.entity.AreaEffectCloudEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Potion;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.items.SubsystemItem;
import net.tardis.mod.potions.TardisPotions;
import net.tardis.mod.tileentities.ConsoleTile;

public class FluidLinksSubsystemItem extends SubsystemItem {
    public FluidLinksSubsystemItem(Properties prop, EnumSubsystemType type, boolean required, boolean repairable) {
        super(prop, type, required, repairable);
    }

    @Override
    public void onTakeoff(ConsoleTile console) {
        this.damage((ServerPlayerEntity)console.getPilot(), 1, console);
    }

    @Override
    public void onLand(ConsoleTile console)                          {
        this.damage((ServerPlayerEntity)console.getPilot(), 1, console);
    }

    @Override
    public void onFlightSecond(ConsoleTile console) {}

    @Override
    public void onBreak(ConsoleTile console) {
        super.onBreak(console       );
        if(console != null && !console.getWorld().isRemote) {
            AreaEffectCloudEntity potion = new AreaEffectCloudEntity(console.getWorld(), console.getPos().getX() + 0.5, console.getPos().getY(), console.getPos().getZ() + 0.5);
            potion.setDuration(30 * 20);
            potion.setRadius(16);
            potion.setPotion(new Potion(new EffectInstance(TardisPotions.MERCURY.get(), 30 * 20, 0)));
            console.getWorld().addEntity(potion);
        }
    }
}
