package net.tardis.mod.subsystem;

import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.items.SubsystemItem;
import net.tardis.mod.tileentities.ConsoleTile;

public class ShieldGeneratorSubsystemItem extends SubsystemItem{


    public ShieldGeneratorSubsystemItem(Properties prop, EnumSubsystemType type, boolean required, boolean repairable) {
        super(prop, type, required, repairable);
    }

    @Override
    public boolean stopsFlight(ConsoleTile console) {
        return false;
    }

}
