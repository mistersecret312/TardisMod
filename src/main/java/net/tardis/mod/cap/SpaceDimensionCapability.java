package net.tardis.mod.cap;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.world.dimensions.TDimensions;

import java.util.Optional;

/**
 * Implementation of ISpaceDimProperties
 * @author 50ap5ud5
 *
 */
public class SpaceDimensionCapability implements ISpaceDimProperties{
    
    private RegistryKey<World> world;
    
    public SpaceDimensionCapability(RegistryKey<World> world) {
        this.world = world;
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.putString("world", this.world.getLocation().toString());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        this.world = RegistryKey.getOrCreateKey(Registry.WORLD_KEY, new ResourceLocation(nbt.getString("world")));
    }

    @Override
    public Vector3d modMotion(Entity ent) {
      if (this.world == TDimensions.SPACE_DIM) {
          ent.fallDistance = 0;
          if (ent instanceof ServerPlayerEntity)
              ((ServerPlayerEntity) ent).connection.floatingTickCount = 0;
      }
      if (this.world == TDimensions.MOON_DIM) {
        if(ent instanceof PlayerEntity) {
            if(((PlayerEntity)ent).abilities.isFlying)
                return ent.getMotion();
        }
        if(!ent.hasNoGravity()) {
            ent.fallDistance -= 0.25;
            return ent.getMotion().add(0, 0.05, 0);
        }
        return ent.getMotion();
      }
      if (this.world == World.THE_END) {
    	  return ent.getMotion();
      }

      //If in TARDIS
      if(WorldHelper.areDimensionTypesSame(ent.world, TDimensions.DimensionTypes.TARDIS_TYPE)){
          LazyOptional<ITardisWorldData> tardisHolder = ent.world.getCapability(Capabilities.TARDIS_DATA);
          if(tardisHolder.isPresent()){
              ITardisWorldData data = tardisHolder.orElseThrow(NullPointerException::new);
              return ent.getMotion().add(0, data.getGravMod(), 0);
          }
      }

      return ent.getMotion().add(0, 0.08, 0);
    }

    @Override
    public boolean isZeroG() {

        return world == TDimensions.SPACE_DIM || world == World.THE_END;
    }

    @Override
    public boolean hasAir() {
        if (this.world == TDimensions.SPACE_DIM || this.world == TDimensions.MOON_DIM)
            return false;
        return true;
    }

    public Optional<ConsoleTile> getConsole(World world){
        TileEntity te = world.getTileEntity(TardisHelper.TARDIS_POS);
        return te instanceof ConsoleTile ? Optional.of((ConsoleTile) te) : Optional.empty();
    }

}
