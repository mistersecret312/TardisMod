package net.tardis.mod.recipe;


import java.util.Collection;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.Tardis;
import net.tardis.mod.misc.AlembicJsonDataListener;
import net.tardis.mod.tileentities.machines.AlembicTile;
/** Custom Recipe format for recipes that are made in the {@linkplain AlembicTile}
 * <br> All recipes requires Water, a burnable item and fuel.
 * <br> The output can be either an item or fluid (Mercury at the moment)*/
public class AlembicRecipe implements IRecipe<AlembicRecipeWrapper>{

    private ResourceLocation id;
    private int requiredWaterAmount = 0;
    private int requiredNonWaterFluidAmount = 0;
    private int generatedFluidAmount = 0;
    private int progressTicks = 200;
    private Ingredient requiredBurnableIngredient = Ingredient.EMPTY;
    private Ingredient fluidCollectionIngredient = Ingredient.EMPTY;
    private int requiredIngredientCount = 1;
    private ResultType type = ResultType.ITEM;
    private ItemStack result;
    
    @Deprecated
    public static final AlembicJsonDataListener DATA_LOADER = new AlembicJsonDataListener(
            "alembic",
            AlembicRecipe.class,
            Tardis.LOGGER,
            AlembicRecipe::fromJson);
    
    public static Collection<AlembicRecipe> getAllRecipes(World world){
        return world.getRecipeManager().getRecipesForType(TardisRecipeSerialisers.ALEMBIC_RECIPE_TYPE);
    }
    
    /**
     * Custom Recipe format for recipes that are made in the {@linkplain AlembicTile}
     * <br> This constructor outputs an {@linkplain ItemStack}
     * <br> All recipes requires Water, a burnable item and fuel.
     * <br> The output can be either an item or fluid (Mercury at the moment)
     * @param requiredWater - amount of water required
     * @param requiredIngredientCount - the count of the Ingredient required for this recipe
     * @param requiredBurnableIngredient - the actual ingredient that gets burned in the burnable slot in the Alembic
     * @param result - the {@linkplain ItemStack} result
     */
    public AlembicRecipe(int requiredWater, int requiredIngredientCount, Ingredient requiredBurnableIngredient, ItemStack result) {
        this.requiredWaterAmount = requiredWater;
        this.requiredBurnableIngredient = requiredBurnableIngredient;
        this.requiredIngredientCount = requiredIngredientCount;
        this.result = result;
        this.type = ResultType.ITEM;
    }
    
    /**
     * Seperate constructor for recipes which only take fluid from the mercury (non-water) fluid tank
     * <br> Allows the alembic to not be hardcoded to only accepting glass bottles
     * @param requiredNonWaterFluidAmount
     * @param requiredIngredientCount
     * @param type
     * @param fluidCollectionIngredient
     * @param result
     */
    public AlembicRecipe(int requiredNonWaterFluidAmount, int requiredIngredientCount, ResultType type, Ingredient fluidCollectionIngredient, ItemStack result) {
        this.requiredWaterAmount = 0;
        this.requiredIngredientCount = requiredIngredientCount;
        this.requiredBurnableIngredient = Ingredient.EMPTY;
        this.type = type;
        this.fluidCollectionIngredient = fluidCollectionIngredient;
        this.requiredNonWaterFluidAmount = requiredNonWaterFluidAmount;
        this.result = result;
    }
    
    /**
     * Custom Recipe format for recipes that are made in the {@linkplain AlembicTile}
     * <br> This constructor outputs a {@linkplain ResultType#FLUID}, adds Mercury fluid to the Mercury fluid tank in the Alembic
     * <br> All recipes requires Water, a burnable item and fuel.
     * <br> The output can be either an item or fluid (Mercury at the moment)
     * @param requiredWater - amount of water required
     * @param requiredIngredientCount - the count of the Ingredient required for this recipe
     * @param requiredBurnableIngredient - the actual ingredient that gets burned in the burnable slot in the Alembic
     */
    public AlembicRecipe(int requiredWater, int requiredIngredientCount, Ingredient requiredBurnableIngredient) {
        this.requiredWaterAmount = requiredWater;
        this.requiredIngredientCount = requiredIngredientCount;
        this.requiredBurnableIngredient = requiredBurnableIngredient;
        this.type = ResultType.FLUID;
        this.result = ItemStack.EMPTY;
        this.requiredNonWaterFluidAmount = 0;
    }
    
    public AlembicRecipe(PacketBuffer buf) {
        this.decode(buf);
    }
    
    /** 
     * Get the required amount of water for this recipe
     * @return
     */
    public int getRequiredWater() {
        return requiredWaterAmount;
    }

    /** Get amount of fluid (usually mercury) that is generated
     * <br> We are not hardcoding to Mercury because in the future, if we make Mercury a fluid, we will be unhardcoding things anyway 
     * */
    public int getGeneratedFluid() {
        return generatedFluidAmount;
    }
    
    public AlembicRecipe setGeneratedFluid(int generatedFluidAmount) {
        this.generatedFluidAmount = generatedFluidAmount;
        return this;
    }
    
    public int getRequiredNonWaterFluidAmount() {
        return this.requiredNonWaterFluidAmount;
    }
    
    public AlembicRecipe setRequiredNonWaterFluidAmount(int requiredNonWaterFluidAmount) {
        this.requiredNonWaterFluidAmount = requiredNonWaterFluidAmount;
        return this;
    }
    
    /** 
     * Get the raw ingredient that will be burned in the burnable slot in the Alembic
     * <br> E.g. Plants or Cinnabar
     * @return
     */
    public Ingredient getRequiredBurnableIngredient() {
        return this.requiredBurnableIngredient;
    }

    /** 
     * Gets the required number of items needed for the recipe's burnable ingredient*/
    public int getRequiredIngredientCount() {
        return requiredIngredientCount;
    }
    
    public Ingredient getFluidCollectionIngredient() {
        return this.fluidCollectionIngredient;
    }
    
    public int getRequiredProgressTicks() {
        return this.progressTicks;
    }
    
    public AlembicRecipe setProgressTicks(int ticks) {
        this.progressTicks = ticks;
        return this;
    }

    /**
     * Get the raw result itemstack of this recipe
     * @return
     */
    public ItemStack getResult() {
        return result.copy();
    }
    
    public AlembicRecipe setResult(ItemStack stack) {
        this.result = stack.copy();
        return this;
    }
    
    public ResultType getResultType() {
        return this.type;
    }

    public boolean matches(FluidStack fluid, ItemStack stack) {
        //water
        if(fluid.getFluid().isIn(FluidTags.WATER) && fluid.getAmount() >= this.requiredWaterAmount) {
            //Ingredient
            if(this.requiredBurnableIngredient.test(stack) && stack.getCount() >= this.getRequiredIngredientCount())
                return true;
        }
        return false;
    }
    
    public boolean matches(int nonWaterFluid, ItemStack input) {
        if (nonWaterFluid >= this.requiredNonWaterFluidAmount && this.fluidCollectionIngredient.test(input)) {
            return true;
        }
        return false;
    }
    
    public boolean matches(AlembicTile tile) {
        return this.type != ResultType.FLUID_COLLECTION ? this.matches(tile.getWaterTank().getFluid(), tile.getItemStackHandler().getStackInSlot(2)) : this.matches(tile.getNonWaterFluidAmount(), tile.getItemStackHandler().getStackInSlot(4));
    }
    
    
    @Override
    public boolean matches(AlembicRecipeWrapper inv, World worldIn) {
        return matches(inv.getAlembicTile());
    }
    
    public void onCraft(AlembicTile tile) {
        if(type == ResultType.FLUID) {
        	int amount = tile.getNonWaterFluidAmount() + this.generatedFluidAmount;
            tile.setNonWaterFluidAmount(amount);
            tile.markDirty();
        }
        else {
            if (type == ResultType.ITEM) {
                boolean empty = this.getResult().isEmpty();
                if (!empty) 
                    tile.getItemStackHandler().insertItem(5, this.getResult(), false);
            }
        }
    }

    public void encode(PacketBuffer buf) {
        buf.writeResourceLocation(id);
        buf.writeInt(this.requiredIngredientCount);
        buf.writeInt(this.type.ordinal());
        buf.writeInt(this.requiredWaterAmount);
        buf.writeInt(this.generatedFluidAmount);
        buf.writeInt(this.requiredNonWaterFluidAmount);
        buf.writeInt(this.progressTicks);
        this.requiredBurnableIngredient.write(buf);
        this.fluidCollectionIngredient.write(buf);
        buf.writeItemStack(result);
    }
    
    public void decode(PacketBuffer buf) {
        this.id = buf.readResourceLocation();
        this.requiredIngredientCount = buf.readInt();
        this.type = ResultType.values()[buf.readInt()];
        this.requiredWaterAmount = buf.readInt();
        this.generatedFluidAmount = buf.readInt();
        this.requiredNonWaterFluidAmount = buf.readInt();
        this.progressTicks = buf.readInt();
        this.requiredBurnableIngredient = Ingredient.read(buf);
        this.fluidCollectionIngredient = Ingredient.read(buf);
        this.result = buf.readItemStack();
    }
    
    public JsonObject toJson() {
    	JsonObject root = new JsonObject();
        JsonObject result = new JsonObject();
        JsonObject ingred = new JsonObject();
        if (requiredBurnableIngredient != Ingredient.EMPTY) {
        	ingred.add("item", requiredBurnableIngredient.serialize());
        }
        if (this.fluidCollectionIngredient != Ingredient.EMPTY) {
        	ingred.add("item", fluidCollectionIngredient.serialize());
        }
        ingred.addProperty("count", this.requiredIngredientCount);
        
        //Result object logic
        if (this.type != ResultType.ITEM) {
        	result.addProperty("special", this.type.toString().toLowerCase());
        	if (this.type == ResultType.FLUID) {
        	    result.addProperty("generated_fluid", this.generatedFluidAmount);
        	}
        	else {
        	    result.addProperty("fluid_collection_item", this.result.getItem().getRegistryName().toString());
        	}
        }
        else {
            result.addProperty("item", this.result.getItem().getRegistryName().toString());
        }
        result.addProperty("count", this.result.getCount());
        
        root.addProperty("type", TardisRecipeSerialisers.ALEMBIC_TYPE_LOC.toString());
        //Required fluid logic
        if (this.type != ResultType.FLUID_COLLECTION)
        	root.addProperty("required_water", this.requiredWaterAmount);
        else
            root.addProperty("required_fluid", this.requiredNonWaterFluidAmount);
        
        root.add("ingredient", ingred);
        
        root.add("result", result);
        return root;
    }
    
    public static AlembicRecipe fromJson(JsonElement root) {
        try {
            JsonObject object = root.getAsJsonObject();
            JsonObject result = object.get("result").getAsJsonObject();
            JsonObject ingred = object.get("ingredient").getAsJsonObject();
            Ingredient focalPoint = Ingredient.deserialize(ingred);
            int count = ingred.get("count").getAsInt();
            
            int requiredFluid = 1000;
            if (object.has("required_water")) {
                requiredFluid = object.get("required_water").getAsInt();    
            }
            int progressTicks = object.has("progress_ticks") ? object.get("progress_ticks").getAsInt() : 200;
            
            if(result.has("item")) {
                Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(result.get("item").getAsString()));
                ItemStack resultStack = new ItemStack(item, result.get("count").getAsInt());
                return new AlembicRecipe(requiredFluid, count, focalPoint, resultStack).setProgressTicks(progressTicks);
            }
            else if(result.has("special")) {
                if (result.get("special").getAsString().toLowerCase().equals(ResultType.FLUID.toString().toLowerCase())) {
                    int generatedFluid = 500;
                    if (result.has("generated_fluid")) {
                        generatedFluid = result.get("generated_fluid").getAsInt();
                    }
                    return new AlembicRecipe(requiredFluid, count, focalPoint).setGeneratedFluid(generatedFluid).setProgressTicks(progressTicks);
                }
                else {
                    if (result.get("special").getAsString().toLowerCase().equals(ResultType.FLUID_COLLECTION.toString().toLowerCase())) {
                        String fluidCollectionKey = "fluid_collection_item";
                        if (object.has("required_fluid")) {
                            requiredFluid = object.get("required_fluid").getAsInt();    
                        }
                        if(result.has(fluidCollectionKey)) {
                            Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(result.get(fluidCollectionKey).getAsString()));
                            ItemStack resultStack = new ItemStack(item, result.get("count").getAsInt());
                            return new AlembicRecipe(requiredFluid, count, ResultType.FLUID_COLLECTION, focalPoint, resultStack).setProgressTicks(progressTicks);
                        }
                        Tardis.LOGGER.warn("No Fluid Collection Item listed! Did you misspell or omit the JSON key name?");
                    }
                    return new AlembicRecipe(requiredFluid, count, focalPoint).setProgressTicks(progressTicks);
                }
            }
            
            return null;
        }
        catch(Exception e) {
            Tardis.LOGGER.catching(e);
            return null;
        }
    }
    
    public static enum ResultType{
        ITEM(),
        FLUID(),
        FLUID_COLLECTION();
    }

    
    @Override
    public ItemStack getCraftingResult(AlembicRecipeWrapper inv) {
        return this.result;
    }

    @Override
    public boolean canFit(int width, int height) {
        return true;
    }

    @Override
    public ItemStack getRecipeOutput() {
        return this.result;
    }

    @Override
    public ResourceLocation getId() {
        return this.id;
    }
    
    public AlembicRecipe setRegistryId(ResourceLocation id) {
        this.id = id;
        return this;
    }

    @Override
    public IRecipeSerializer<?> getSerializer() {
        return TardisRecipeSerialisers.ALEMBIC_SERIALISER.get();
    }

    @Override
    public IRecipeType<?> getType() {
        return TardisRecipeSerialisers.ALEMBIC_RECIPE_TYPE;
    }
    
    @Override
    public boolean isDynamic() {
        return false; //If false, allows recipe unlocking to be available (though this doesn't actually work)
    }

    @Override
    public String getGroup() {
        return " "; //Must have a value, otherwise vanilla will complain about a null recipe category type
    }
    
    public static class AlembicRecipeSerializer extends ForgeRegistryEntry<IRecipeSerializer<?>> 
    implements IRecipeSerializer<AlembicRecipe>{

        @Override
        public AlembicRecipe read(ResourceLocation recipeId, JsonObject json) {
            AlembicRecipe recipe = fromJson(json);
            recipe.setRegistryId(recipeId); //We have to explicitly set the registry name to avoid a NPE
            return recipe;
        }

        @Override
        public AlembicRecipe read(ResourceLocation recipeId, PacketBuffer buffer) {
            ResourceLocation id = buffer.readResourceLocation();
            int requiredCount = buffer.readInt();
            ResultType type = ResultType.values()[buffer.readInt()];
            int requiredWater = buffer.readInt();
            int generatedFluid = buffer.readInt();
            int requiredNonWaterFluid = buffer.readInt();
            int progressTicks = buffer.readInt();
            Ingredient focalPoint = Ingredient.read(buffer);
            Ingredient fluidCollectionIngredient = Ingredient.read(buffer);
            ItemStack result = buffer.readItemStack();
            AlembicRecipe recipe = null;
            if(type == ResultType.ITEM) {
                Item item = result.getItem();
                ItemStack resultStack = new ItemStack(item, result.getCount());
                recipe = new AlembicRecipe(requiredWater, requiredCount, focalPoint, resultStack);
            }
            else if(type == ResultType.FLUID) {
                recipe = new AlembicRecipe(requiredWater, requiredCount, focalPoint).setGeneratedFluid(generatedFluid);
            }
            else if (type == ResultType.FLUID_COLLECTION) {
                Item item = result.getItem();
                ItemStack resultStack = new ItemStack(item, result.getCount());
                recipe = new AlembicRecipe(requiredNonWaterFluid, requiredCount, type ,fluidCollectionIngredient, resultStack);
            }
            recipe.setProgressTicks(progressTicks);
            recipe.setRegistryId(id); //We have to explicitly set the registry name to avoid a NPE
            return recipe;
        }

        @Override
        public void write(PacketBuffer buffer, AlembicRecipe recipe) {
            buffer.writeResourceLocation(recipe.id);
            buffer.writeInt(recipe.requiredIngredientCount);
            buffer.writeInt(recipe.type.ordinal());
            buffer.writeInt(recipe.requiredWaterAmount);
            buffer.writeInt(recipe.generatedFluidAmount);
            buffer.writeInt(recipe.requiredNonWaterFluidAmount);
            buffer.writeInt(recipe.progressTicks);
            recipe.requiredBurnableIngredient.write(buffer);
            recipe.fluidCollectionIngredient.write(buffer);
            buffer.writeItemStack(recipe.result);
        }
    }
}
