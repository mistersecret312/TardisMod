package net.tardis.mod.enums;

public enum EnumSubsystemType {
    DEMAT,
    FLUIDLINK,
    NAVCOM,
    STABILIZER,
    CHAMELEON,
    ANTENNA,
    GRACE,
    SHIELD,
    UPGRADE,
    CAPACITOR
}
