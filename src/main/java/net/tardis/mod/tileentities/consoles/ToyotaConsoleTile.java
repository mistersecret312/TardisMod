package net.tardis.mod.tileentities.consoles;

import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.registries.ControlRegistry;
import net.tardis.mod.texturevariants.ConsoleTextureVariants;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TTiles;

public class ToyotaConsoleTile extends ConsoleTile{


	public ToyotaConsoleTile() {
		this(TTiles.CONSOLE_TOYOTA.get());
		this.registerControlEntry(ControlRegistry.MONITOR.get());
	}
	
	public ToyotaConsoleTile(TileEntityType<?> type) {
		super(type);
		this.variants = ConsoleTextureVariants.TOYOTA;
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return new AxisAlignedBB(this.getPos()).expand(3, 4, 3);
	}
}
