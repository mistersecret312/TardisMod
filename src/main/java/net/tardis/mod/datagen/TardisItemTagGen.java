package net.tardis.mod.datagen;

import com.google.common.collect.Maps;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.minecraft.block.Blocks;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.Tardis;
import net.tardis.mod.tags.TardisBlockTags;
import net.tardis.mod.tags.TardisEntityTypeTags;
import net.tardis.mod.tags.TardisItemTags;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

public class TardisItemTagGen extends BaseDataProvider {

	public Map<ResourceLocation, List<ResourceLocation>> tags = Maps.newHashMap();

	public TardisItemTagGen(DataGenerator gen){
		super(gen);
	}

	public void registerTags(){
		this.addToTag(TardisItemTags.CONCRETE.getName(), Blocks.BLACK_CONCRETE.getRegistryName());
		this.addToTag(TardisItemTags.CONCRETE.getName(), Blocks.BLUE_CONCRETE.getRegistryName());
		this.addToTag(TardisItemTags.CONCRETE.getName(), Blocks.BROWN_CONCRETE.getRegistryName());
		this.addToTag(TardisItemTags.CONCRETE.getName(), Blocks.CYAN_CONCRETE.getRegistryName());
		this.addToTag(TardisItemTags.CONCRETE.getName(), Blocks.GRAY_CONCRETE.getRegistryName());
		this.addToTag(TardisItemTags.CONCRETE.getName(), Blocks.GREEN_CONCRETE.getRegistryName());
		this.addToTag(TardisItemTags.CONCRETE.getName(), Blocks.LIGHT_BLUE_CONCRETE.getRegistryName());
		this.addToTag(TardisItemTags.CONCRETE.getName(), Blocks.LIGHT_GRAY_CONCRETE.getRegistryName());
		this.addToTag(TardisItemTags.CONCRETE.getName(), Blocks.LIME_CONCRETE.getRegistryName());
		this.addToTag(TardisItemTags.CONCRETE.getName(), Blocks.MAGENTA_CONCRETE.getRegistryName());
		this.addToTag(TardisItemTags.CONCRETE.getName(), Blocks.ORANGE_CONCRETE.getRegistryName());
		this.addToTag(TardisItemTags.CONCRETE.getName(), Blocks.PINK_CONCRETE.getRegistryName());
		this.addToTag(TardisItemTags.CONCRETE.getName(), Blocks.PURPLE_CONCRETE.getRegistryName());
		this.addToTag(TardisItemTags.CONCRETE.getName(), Blocks.RED_CONCRETE.getRegistryName());
		this.addToTag(TardisItemTags.CONCRETE.getName(), Blocks.WHITE_CONCRETE.getRegistryName());
		this.addToTag(TardisItemTags.CONCRETE.getName(), Blocks.YELLOW_CONCRETE.getRegistryName());
		

		this.addToTag(TardisItemTags.CORAL.getName(), Blocks.BRAIN_CORAL_BLOCK);
		this.addToTag(TardisItemTags.CORAL.getName(), Blocks.BRAIN_CORAL_FAN);
		this.addToTag(TardisItemTags.CORAL.getName(), Blocks.BUBBLE_CORAL_BLOCK);
		this.addToTag(TardisItemTags.CORAL.getName(), Blocks.BUBBLE_CORAL_FAN);
		this.addToTag(TardisItemTags.CORAL.getName(), Blocks.FIRE_CORAL_BLOCK);
		this.addToTag(TardisItemTags.CORAL.getName(), Blocks.FIRE_CORAL_FAN);
		this.addToTag(TardisItemTags.CORAL.getName(), Blocks.HORN_CORAL_BLOCK);
		this.addToTag(TardisItemTags.CORAL.getName(), Blocks.HORN_CORAL_FAN);
		this.addToTag(TardisItemTags.CORAL.getName(), Blocks.TUBE_CORAL_BLOCK);
		this.addToTag(TardisItemTags.CORAL.getName(), Blocks.TUBE_CORAL_FAN);
		
		this.addToTag(TardisItemTags.DEAD_CORAL.getName(), Blocks.DEAD_BRAIN_CORAL_BLOCK);
		this.addToTag(TardisItemTags.DEAD_CORAL.getName(), Blocks.DEAD_BRAIN_CORAL_FAN);
		this.addToTag(TardisItemTags.DEAD_CORAL.getName(), Blocks.DEAD_BUBBLE_CORAL_BLOCK);
		this.addToTag(TardisItemTags.DEAD_CORAL.getName(), Blocks.DEAD_BUBBLE_CORAL_FAN);
		this.addToTag(TardisItemTags.DEAD_CORAL.getName(), Blocks.DEAD_FIRE_CORAL_BLOCK);
		this.addToTag(TardisItemTags.DEAD_CORAL.getName(), Blocks.DEAD_FIRE_CORAL_FAN);
		this.addToTag(TardisItemTags.DEAD_CORAL.getName(), Blocks.DEAD_HORN_CORAL_BLOCK);
		this.addToTag(TardisItemTags.DEAD_CORAL.getName(), Blocks.DEAD_HORN_CORAL_FAN);
		this.addToTag(TardisItemTags.DEAD_CORAL.getName(), Blocks.DEAD_TUBE_CORAL_BLOCK);
		this.addToTag(TardisItemTags.DEAD_CORAL.getName(), Blocks.DEAD_TUBE_CORAL_FAN);

	}

	@Override
	public void act(DirectoryCache cache, Path base) throws IOException {
		tags.clear();
		this.registerTags();
		for(Map.Entry<ResourceLocation, List<ResourceLocation>> tag : tags.entrySet()){

			this.generateTag(base, cache, tag.getKey(), tag.getValue());

		}

	}

	public void addToTag(ResourceLocation tag, ResourceLocation item){
		if(!tags.containsKey(tag)){
			tags.put(tag, new ArrayList<>());
		}

		this.tags.get(tag).add(item);
	}

	public void addToTag(ResourceLocation tag, ForgeRegistryEntry<?> entry){
		this.addToTag(tag, entry.getRegistryName());
	}

	public void generateTag(Path path, DirectoryCache cache, ResourceLocation name, List<ResourceLocation> items){
		this.generateTable(cache, getPath(path, name), () -> serialize(items));
	}
	
	public void generateTable(DirectoryCache cache, Path path, Supplier<JsonElement> element) {
        try {
			IDataProvider.save(DataGen.GSON, cache, element.get(), path);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
	
	public Path getPath(Path base, ResourceLocation path) {
		return base.resolve("data/" + path.getNamespace() + "/tags/items/" + path.getPath() + ".json");
	}
	
	public JsonElement serialize(List<ResourceLocation> items) {
		JsonObject root = new JsonObject();
		
		root.add("replace", new JsonPrimitive(false));
		
		JsonArray itemBlock = new JsonArray();
		for(ResourceLocation s : items) {
			itemBlock.add(s.toString());
		}
		root.add("values", itemBlock);
		
		return root;
	}

	@Override
	public String getName() {
		return "TARDIS Item Tag Gen";
	}

}