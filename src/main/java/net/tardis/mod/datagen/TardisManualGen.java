package net.tardis.mod.datagen;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import net.minecraft.client.Minecraft;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.datagen.migration.ManualNamesLookup;
import net.tardis.mod.datagen.migration.ManualNamesLookup.ManualMigrationWrapper;

public class TardisManualGen implements IDataProvider{
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private final DataGenerator generator;
    private List<ResourceLocation> chapters = new ArrayList<>();
    private String locale;
    
    public TardisManualGen(DataGenerator generator, String locale) {
        this.generator = generator;
        this.locale = locale;
    }
    
    public TardisManualGen(DataGenerator generator) {
    	this(generator, "en_us");
    }

	@Override
	public void act(DirectoryCache cache) throws IOException {
		final Path path = this.generator.getOutputFolder();
		
		//Migration of old Pages
//	    ManualNamesLookup.createMigrationObjects();
//	    for (ManualMigrationWrapper obj : ManualNamesLookup.getMigrationObjects()) {
//	    	if (obj.getConvertedObjectType().equals(ManualObjectType.PAGE)){
//	    		if (obj.getPageType().equals(PageType.NORMAL)) {
//	    			if (obj.requiresMerging()) {
//	    				convertAndMergeToNormalPage(path, cache, obj.getPathsToMerge(), obj.getNewLoc());
//	    			}
//	    			else {
//	    				this.convertToNormalPage(path, cache, obj.getOldLoc(), obj.getNewLoc());
//	    			}
//	    		}
//	    	}
//	    }
		
		
	}
	
	public String getLocale() {
		return this.locale;
	}
	
	public String getOldManualPath(ResourceLocation inputLoc) {
		return "resources/assets/" + inputLoc.getNamespace() + "/" + inputLoc.getPath();
	}
	
	public Path getPathRoot(Path base, ResourceLocation out) {
		return base.resolve("assets/" + out.getNamespace() + "/manual/" + this.locale + "/" + out.getPath() + ".json");
	}
	
	public Path getPathPage(Path base, ResourceLocation out) {
		return base.resolve("assets/" + out.getNamespace() + "/manual/" + this.locale + "/page/" + out.getPath() + ".json");
	}
	
	public Path getPathChapter(Path base, ResourceLocation out) {
		return base.resolve("assets/" + out.getNamespace() + "/manual/" + this.locale + "/chapter/" + out.getPath() + ".json");
	}
	
	public void createMainManualIndex(Path base, DirectoryCache cache) throws IOException {
		IDataProvider.save(GSON, cache, createMainManualIndex(), getPathRoot(base, new ResourceLocation(Tardis.MODID, "manual")));
	}
	
	public void createChapter(Path base, DirectoryCache cache, String displayName, List<ResourceLocation> pages, ResourceLocation newLoc) throws JsonIOException, JsonSyntaxException, IOException {
		IDataProvider.save(GSON, cache, this.addChapter(displayName, pages), getPathChapter(base, newLoc));
		chapters.add(newLoc);
	}
	
	public void createCoverPage(Path base, DirectoryCache cache, String title, ResourceLocation imageLoc, ResourceLocation newLoc) throws JsonIOException, JsonSyntaxException, IOException {
		IDataProvider.save(GSON, cache, this.addCoverPage(title), getPathPage(base, newLoc));
		chapters.add(newLoc);
	}
	
	public void createNormalPage(Path base, DirectoryCache cache, String text, ResourceLocation newLoc) throws JsonIOException, JsonSyntaxException, IOException {
		IDataProvider.save(GSON, cache, this.createNormalPage(text), getPathPage(base, newLoc));
		chapters.add(newLoc);
	}
	
	public void convertToNormalPage(Path base, DirectoryCache cache, ResourceLocation oldPath, ResourceLocation newLoc) throws JsonIOException, JsonSyntaxException, IOException {
		Path newPath = base.getParent().resolve(getOldManualPath(oldPath));
		File file = newPath.toFile();
		FileInputStream stream = new FileInputStream(file);
		JsonObject old = new JsonParser().parse(new InputStreamReader(stream)).getAsJsonObject();
		IDataProvider.save(GSON, cache, this.convertToNormalPage(old), getPathPage(base, newLoc));
	}
	
	public void convertAndMergeToNormalPage(Path base, DirectoryCache cache, List<ResourceLocation> oldPaths, ResourceLocation newLoc) throws JsonIOException, JsonSyntaxException, IOException {
		List<JsonObject> oldJsons = new ArrayList<>();
		for (ResourceLocation path : oldPaths){
			Path newPath = base.getParent().resolve(getOldManualPath(path));
			File file = newPath.toFile();
			FileInputStream stream = new FileInputStream(file);
			JsonObject old = new JsonParser().parse(new InputStreamReader(stream)).getAsJsonObject();
			oldJsons.add(old);
		}
		IDataProvider.save(GSON, cache, this.convertAndMergeToNormalPage(oldJsons), getPathPage(base, newLoc));
	}
	
	protected JsonObject addChapter(String displayName, List<ResourceLocation> pages) {
		JsonObject root = new JsonObject();
		root.addProperty("display_name", displayName);
		JsonArray pageList = new JsonArray();
		for (ResourceLocation page : pages) {
			pageList.add(page.toString());
		}
		root.add("pages", pageList);
		return root;
	}
	
	protected JsonObject addCoverPage(String title) {
		JsonObject root = new JsonObject();
		root.addProperty("type", PageType.COVER.toString().toLowerCase());
		root.addProperty("title", title);
		return root;
	}
	
	protected JsonObject createNormalPage(String text) {
		JsonObject root = new JsonObject();
		root.addProperty("type", PageType.NORMAL.toString().toLowerCase());
		root.addProperty("text", text);
		return root;
	}
	
	protected JsonObject convertAndMergeToNormalPage(List<JsonObject> oldPages) {
		JsonObject root = new JsonObject();
		root.addProperty("type", PageType.NORMAL.toString().toLowerCase());
		String text = "";
		for (JsonObject oldPage : oldPages) {
			for (JsonElement ele : oldPage.get("lines").getAsJsonArray()) {
				text = text.concat(ele.getAsString());
			}
		}
		root.addProperty("text", text);
		return root;
	}
	
	protected JsonObject convertToNormalPage(JsonObject old) {
		JsonObject root = new JsonObject();
		root.addProperty("type", PageType.NORMAL.toString().toLowerCase());
		String text = "";
		for (JsonElement ele : old.get("lines").getAsJsonArray()) {
			text = text.concat(ele.getAsString());
		}
		root.addProperty("text", text);
		return root;
	}
	
	private JsonObject createMainManualIndex() {
		JsonObject root = new JsonObject();
		JsonArray chapterList = new JsonArray();
		for(ResourceLocation chapter : this.chapters) {
			chapterList.add(chapter.toString());
		}
		root.add("chapters", chapterList);
		return root;
	}

	@Override
	public String getName() {
		return "Tardis Manual Migration Generator";
	}
	
	public enum ManualObjectType{
		MAIN_PAGE,
		CHAPTER,
		PAGE
	}
	
	public enum PageType{
		NORMAL,
		COVER
	}
}
