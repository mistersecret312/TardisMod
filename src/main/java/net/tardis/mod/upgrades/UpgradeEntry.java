package net.tardis.mod.upgrades;

import javax.annotation.Nullable;

import net.minecraft.item.Item;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.tileentities.ConsoleTile;

public class UpgradeEntry extends ForgeRegistryEntry<UpgradeEntry>{

	private IConsoleSpawner<Upgrade> spawn;
	private Item item;
	private EnumSubsystemType system;
	
	/**
	 * 
	 * @param spawn - Functional Interface to create your Upgrade
	 * @param item - Item key
	 * @param type - Parent system type, for those that depend on it. Can be null
	 */
	public UpgradeEntry(IConsoleSpawner<Upgrade> spawn, Item item, @Nullable EnumSubsystemType type) {
		this.spawn = spawn;
		this.item = item;
		this.system = type;
	}
	
	public Item getItem() {
		return this.item;
	}
	
	public Upgrade create(ConsoleTile tile) {
		Upgrade upgrade = spawn.create(this, tile, system);
		return upgrade;
	}
	
	public static interface IConsoleSpawner<T>{
		T create(UpgradeEntry entry, ConsoleTile console, @Nullable EnumSubsystemType sys);
	}

}
