package net.tardis.mod.upgrades;

import net.minecraft.item.Item;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.items.TimeLinkUpgradeItem;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.tileentities.ConsoleTile;

import java.util.Optional;

public class TimeLinkUpgrade extends Upgrade{

	private Optional<ConsoleTile> slave = Optional.empty();
	
	public TimeLinkUpgrade(UpgradeEntry entry, ConsoleTile tile, EnumSubsystemType clazz) {
		super(entry, tile, clazz);
	}

	@Override
	public void onLand() {
		

		if(this.isUsable() && this.isActivated()) {
			if(!slave.isPresent())
				findSlave();
			this.slave.ifPresent(tile -> {
				BlockPos pos;
				//If the parent Tardis has already preparing to land, use its current position
				//By this time, the current location will already be set, and this is where the exterior blocks are going to be placed.
            	if (this.getConsole().getLandTime() <= 0) {
            	    pos = this.getConsole().getCurrentLocation().offset(this.getConsole().getExteriorFacingDirection(), 3);
            	}
            	else {
                	pos = this.getConsole().getPositionInFlight().getPos().offset(this.getConsole().getExteriorFacingDirection(), 3);
                }
            	
            	//Only update the current location during landing, don't call setDestination as it will cause recalculation of flight, but reachedDestinationTicks won't decrease, so the slaved Tardis will land multiple times
				tile.setForcedLandCoord(Optional.of(new SpaceTimeCoord(this.getConsole().getDestinationDimension(), pos)));
				tile.setCurrentLocation(this.getConsole().getDestinationDimension(), pos);
				if(!tile.isLanding())
					tile.land();
				
			});
		}
		else {
			if(slave.isPresent())
		        this.unlink();
		}
		
	}

	@Override
	public void onTakeoff() {
		
		if(this.isUsable() && this.isActivated()) {
			if(!slave.isPresent())
				findSlave();

			this.slave.ifPresent(tile -> {
				tile.takeoffTowed();
				//Set the Landing Type to match that of the parent Tardis. This reduces issues of the slaved Tardis landing at a different set of y coordinates.
				this.getConsole().getControl(LandingTypeControl.class).ifPresent(parent -> {
					tile.getControl(LandingTypeControl.class).ifPresent(landType -> {
						landType.setLandType(parent.getLandType());
					});
				});
			});
		}
		else {
			if(slave.isPresent())
		        this.unlink();
		}
	}

	@Override
	public void onFlightSecond() {
		if(this.isUsable() && this.isActivated()) {
			this.slave.ifPresent(tile -> {
				//Don't setDestination every second, because this recalculates flight and exponentially increases the reachedDestinationTicks, which makes the Tardis land multiple times.
//				SpaceTimeCoord coord = new SpaceTimeCoord(this.getConsole().getDestinationDimension(), this.getConsole().getDestinationPosition().offset(this.getConsole().getExteriorFacingDirection(), 3));
//				tile.setDestination(coord);
//				System.out.println("Changing towed coordinates to: " + this.getConsole().getDestinationPosition().toString());
//				System.out.println("Towed dest ticks: " + tile.getReachDestinationTick());
			});
		}
	}
	
	public Optional<ConsoleTile> getSlave() {
		return this.slave;
	}
	
	public void setSlave(Optional<ConsoleTile> tile) {
		this.slave = tile;
	}
	
	public void unlink() {
		this.slave = Optional.empty();
	}
	
	private void findSlave() {
		Item item = this.getStack().getItem();
		if (item != null && item instanceof TimeLinkUpgradeItem) {
			TimeLinkUpgradeItem link = (TimeLinkUpgradeItem)item;
			RegistryKey<World> type = link.getConsoleWorldKey(getStack());
			if(type != null) {
				ServerWorld world = this.getConsole().getWorld().getServer().getWorld(type);
				TardisHelper.getConsoleInWorld(world).ifPresent(slave -> this.setSlave(Optional.of(slave)));
			}
		}
		
	}


}
