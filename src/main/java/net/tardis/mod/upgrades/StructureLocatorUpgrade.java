package net.tardis.mod.upgrades;

import javax.annotation.Nullable;

import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.tileentities.ConsoleTile;

public class StructureLocatorUpgrade extends Upgrade{

	public StructureLocatorUpgrade(UpgradeEntry entry, ConsoleTile tile, @Nullable EnumSubsystemType clazz) {
		super(entry, tile, clazz);
	}

	@Override
	public void onLand() {
		
	}

	@Override
	public void onTakeoff() {
		
	}

	@Override
	public void onFlightSecond() {

	}

}
