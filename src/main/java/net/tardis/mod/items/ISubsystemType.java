package net.tardis.mod.items;

import net.tardis.mod.enums.EnumSubsystemType;

public interface ISubsystemType {
    EnumSubsystemType getType();
}
