package net.tardis.mod.items;

import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.AirLockBlock;
import net.tardis.mod.blocks.BrokenExteriorBlock.BlockItemBrokenExterior;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.blocks.WaypointBankBlock.WaypointBankItem;
import net.tardis.mod.blocks.multiblock.MultiblockPatterns;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.constants.TardisConstants.Part.PartType;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.items.sonicparts.SonicBasePart;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.registries.DalekTypeRegistry;
import net.tardis.mod.sonic.ISonicPart;
import net.tardis.mod.subsystem.*;

public class TItems {
	public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, Tardis.MODID);

    public static final RegistryObject<Item> CIRCUITS = ITEMS.register("circuits", () -> createItem(new BaseItem(Prop.Items.SIXTY_FOUR.get().group(TItemGroups.MAINTENANCE))));
    public static final RegistryObject<Item> INT_DOOR = ITEMS.register("int_door", () -> createItem(new SpawnerItem(() -> TEntities.DOOR.get())));

    public static final RegistryObject<Item> KEY = ITEMS.register("tardis_key", () -> createItem(new KeyItem()));
    public static final RegistryObject<Item> KEY_01 = ITEMS.register("key_gallifreyan", () -> createItem(new KeyItem()));
    public static final RegistryObject<Item> KEY_PIRATE = ITEMS.register("key_pirate", () -> createItem(new KeyItem()));
    
    public static final RegistryObject<Item> CIRCUIT_PASTE = ITEMS.register("circuit_paste", () -> createItem(new BaseItem(Prop.Items.SIXTY_FOUR.get().group(TItemGroups.MAINTENANCE))));
    
    public static final RegistryObject<Item> EARTHSHOCK_GUN = ITEMS.register("earthshock_gun", () -> createItem(new LaserGunItem(9)));


    //Component parts - register the subsystems before upgrades so the upgrades can use the subsystem display name
    public static final RegistryObject<Item> DEMAT_CIRCUIT = ITEMS.register("subsystem/dematerialisation_circuit", () -> createItem(new FlightSubsystemItem(Prop.Items.ONE.get().maxDamage(1000).group(TItemGroups.MAINTENANCE), EnumSubsystemType.DEMAT, true, true)));
    //	public static final RegistryObject<Item> THERMOCOUPLING = ITEMS.register("thermocoupling", () -> createItem(new TardisPartItem(PartType.SUBSYSTEM, false, true)));
    public static final RegistryObject<Item> FLUID_LINK = ITEMS.register("subsystem/fluid_link", () -> createItem(new FluidLinksSubsystemItem(Prop.Items.ONE.get().maxDamage(250).group(TItemGroups.MAINTENANCE), EnumSubsystemType.FLUIDLINK, true, true)));
    public static final RegistryObject<Item> CHAMELEON_CIRCUIT = ITEMS.register("subsystem/chameleon_circuit", () -> createItem(new ChameleonSubsystemItem(Prop.Items.ONE.get().maxDamage(250).group(TItemGroups.MAINTENANCE), EnumSubsystemType.CHAMELEON,false, true)));
    public static final RegistryObject<Item> INTERSTITIAL_ANTENNA = ITEMS.register("subsystem/interstitial_antenna", () -> createItem(new AntennaSubsystemItem(Prop.Items.ONE.get().maxDamage(250).group(TItemGroups.MAINTENANCE), EnumSubsystemType.ANTENNA, false, true)));
    public static final RegistryObject<Item> TEMPORAL_GRACE = ITEMS.register("subsystem/temporal_grace", () -> createItem(new TemporalGraceSubsystemItem(Prop.Items.ONE.get().maxDamage(250).group(TItemGroups.MAINTENANCE), EnumSubsystemType.GRACE,false, true)));
    public static final RegistryObject<Item> SHEILD_GENERATOR = ITEMS.register("subsystem/shield_generator", () -> createItem(new ShieldGeneratorSubsystemItem(Prop.Items.ONE.get().maxDamage(250).group(TItemGroups.MAINTENANCE), EnumSubsystemType.SHIELD, false, true)));
    public static final RegistryObject<Item> STABILIZERS = ITEMS.register("subsystem/stabilizer", () -> createItem(new StabilizerSubsystemItem(Prop.Items.ONE.get().maxDamage(250).group(TItemGroups.MAINTENANCE), EnumSubsystemType.STABILIZER, false, true)));
    public static final RegistryObject<Item> NAV_COM = ITEMS.register("subsystem/nav_com", () -> createItem(new NavComSubsystemItem(Prop.Items.ONE.get().maxDamage(250).group(TItemGroups.MAINTENANCE), EnumSubsystemType.NAVCOM,false, true)));

    public static final RegistryObject<Item> BLANK_UPGRADE = ITEMS.register("blank_upgrade", () -> createItem(new TardisPartItem(PartType.UPGRADE, false, false)));
    public static final RegistryObject<Item> ELECTRO_CONVERT_UPGRADE = ITEMS.register("upgrades/electro_convert", () -> createItem(new TardisPartItem(PartType.UPGRADE, false, false, TardisConstants.Translations.TEMPORAL_GRACE)));
    public static final RegistryObject<Item> KEY_FOB_UPGRADE = ITEMS.register("upgrades/key_fob", () -> createItem(new TardisPartItem(PartType.UPGRADE, false, false, TardisConstants.Translations.SHIELD_GENERATOR)));
    public static final RegistryObject<Item> TELE_STRUCTURE_UPGRADE = ITEMS.register("upgrades/structure", () -> createItem(new TardisPartItem(PartType.UPGRADE, false, false)));
    public static final RegistryObject<Item> TIME_LINK_UPGRADE = ITEMS.register("upgrades/time_link", () -> createItem(new TimeLinkUpgradeItem()));
    public static final RegistryObject<Item> ZERO_ROOM_UPGRADE = ITEMS.register("upgrades/zero_room", () -> createItem(new TardisPartItem(PartType.UPGRADE, false, false)));
    public static final RegistryObject<Item> GRAVITY_CONTROL_UPGRADE = ITEMS.register("upgrades/grav_control", () -> createItem(new TardisPartItem(PartType.UPGRADE, false, false)));
    
    public static final RegistryObject<Item> VORTEX_MANIP = ITEMS.register("vm", () -> createItem(new VortexManipItem()));
    public static final RegistryObject<Item> VM_MODULE = ITEMS.register("vm_module", () -> createItem(new BaseItem()));
    public static final RegistryObject<Item> VM_STRAP = ITEMS.register("vm_strap", () -> createItem(new BaseItem()));

    //Machine items

    public static final RegistryObject<Item> STATTENHEIM_REMOTE = ITEMS.register("stattenheim_remote", () -> createItem(new StatRemoteItem(Prop.Items.ONE.get().group(TItemGroups.MAIN))));
    public static final RegistryObject<Item> DIAGNOSTIC_TOOL = ITEMS.register("diagnostic_tool", () -> createItem(new TardisDiagnosticItem(Prop.Items.ONE.get())));
    public static final RegistryObject<Item> MONITOR_REMOTE = ITEMS.register("monitor_remote", () -> createItem(new MonitorRemoteItem()));
    public static final RegistryObject<Item> DATA_CRYSTAL = ITEMS.register("data_crystal", () -> createItem(new DataCrystalItem(Prop.Items.ONE.get().group(TItemGroups.MAIN))));
    public static final RegistryObject<Item> ARTIFACT_MAP = ITEMS.register("artifact_map", () -> createItem(new ArtifactMap(Prop.Items.ONE.get())));
    public static final RegistryObject<Item> POCKET_WATCH = ITEMS.register("pocket_watch", () -> createItem(new PocketWatchItem()));
    public static final RegistryObject<Item> ARS_TABLET = ITEMS.register("ars_tablet", () -> createItem(new ARSTabletItem()));
    public static final RegistryObject<Item> TARDIS_BACKDOOR = ITEMS.register("timeship_backdoor", () -> createItem(new TardisBackdoorItem()));

    public static final RegistryObject<Item> LEAKY_ARTRON_CAPACITOR = ITEMS.register("leaky_capacitor", () -> createItem(new ArtronCapacitorItem(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE),32, 0.2F)));
    public static final RegistryObject<Item> ARTRON_CAPACITOR = ITEMS.register("artron_capacitor", () -> createItem(new ArtronCapacitorItem(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE), 64, 0.5F)));
    public static final RegistryObject<Item> ARTRON_CAPACITOR_MEDIUM = ITEMS.register("artron_capacitor_mid", () -> createItem(new ArtronCapacitorItem(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE),128, 1F)));
    public static final RegistryObject<Item> ARTRON_CAPACITOR_HIGH = ITEMS.register("artron_capacitor_high", () -> createItem(new ArtronCapacitorItem(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE),256, 1.5F)));

    public static final RegistryObject<Item> MERCURY_BOTTLE = ITEMS.register("mercury_bottle", () -> createItem(new BaseItem(Prop.Items.SIXTY_FOUR.get().group(TItemGroups.MAINTENANCE))));
    public static final RegistryObject<Item> CINNABAR = ITEMS.register("cinnabar", () -> createItem(new BaseItem(Prop.Items.SIXTY_FOUR.get().group(TItemGroups.MAINTENANCE))));

    public static final RegistryObject<Item> ARTRON_BATTERY = ITEMS.register("artron_battery", () -> createItem(new ArtronItemStackBatteryItem(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE),0.5F, 0.75F, 250F, false)));
    public static final RegistryObject<Item> ARTRON_BATTERY_MED = ITEMS.register("artron_battery_medium", () -> createItem(new ArtronItemStackBatteryItem(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE),1.25F, 0.5F, 750F, false)));
    public static final RegistryObject<Item> ARTRON_BATTERY_HIGH = ITEMS.register("artron_battery_high", () -> createItem(new ArtronItemStackBatteryItem(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE),2F, 0.25F, 1500F, false)));
    public static final RegistryObject<Item> ARTRON_BATTERY_CREATIVE = ITEMS.register("artron_battery_creative", () -> createItem(new ArtronItemStackBatteryItem(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE),Float.MAX_VALUE, 0F, Float.MAX_VALUE, true)));

    public static final RegistryObject<Item> ANTIDEPRESSANT = ITEMS.register("antidepressant", () -> createItem(new MoodChangeItem(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE), 100)));
    public static final RegistryObject<Item> DEPRESSANT = ITEMS.register("depressant", () -> createItem(new MoodChangeItem(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE), -100)));
    public static final RegistryObject<Item> SONIC_EMITTER = ITEMS.register("sonic_emitter", () -> createItem(new SonicBasePart(ISonicPart.SonicPart.EMITTER)));
    public static final RegistryObject<Item> SONIC_ACTIVATOR = ITEMS.register("sonic_activator", () -> createItem(new SonicBasePart(ISonicPart.SonicPart.ACTIVATOR)));
    public static final RegistryObject<Item> SONIC_HANDLE = ITEMS.register("sonic_handle", () -> createItem(new SonicBasePart(ISonicPart.SonicPart.HANDLE)));
    public static final RegistryObject<Item> SONIC_END = ITEMS.register("sonic_end", () -> createItem(new SonicBasePart(ISonicPart.SonicPart.END)));
    public static final RegistryObject<Item> SONIC = ITEMS.register("sonic", () -> createItem(new SonicItem()));

    public static final RegistryObject<Item> MANUAL = ITEMS.register("manual", () -> createItem(new ManualItem()));
    public static final RegistryObject<Item> SQUARENESS_GUN = ITEMS.register("squareness_gun", () -> createItem(new SquarenessGunItem()));

    public static final RegistryObject<Item> DEBUG = ITEMS.register("debug", () -> createItem(new DebugItem()));
    public static final RegistryObject<Item> TAPE_MESURE = ITEMS.register("tape_measure", () -> createItem(new TapeMeasureItem()));
    public static final RegistryObject<Item> PLASMIC_SHELL_GENERATOR = ITEMS.register("plasmic_shell", () -> createItem(new PlasmicShellItem()));

    //Clothes
    public static final RegistryObject<Item> SPACE_HELM = ITEMS.register("space_helm", () -> createItem(new SpaceSuitItem(EquipmentSlotType.HEAD)));
    public static final RegistryObject<Item> SPACE_CHEST = ITEMS.register("space_chest", () -> createItem(new SpaceSuitItem(EquipmentSlotType.CHEST)));
    public static final RegistryObject<Item> SPACE_LEGS = ITEMS.register("space_legs", () -> createItem(new SpaceSuitItem(EquipmentSlotType.LEGS)));
    public static final RegistryObject<Item> SPACE_BOOTS = ITEMS.register("space_boots", () -> createItem(new SpaceSuitItem(EquipmentSlotType.FEET)));

    //Block Items
    public static final RegistryObject<Item> BROKEN_EXTERIOR = ITEMS.register("broken_exterior", () -> createItem(new BlockItemBrokenExterior(TBlocks.broken_exterior.get())));
    public static final RegistryObject<Item> AIR_LOCK = ITEMS.register("air_lock", () -> createItem(new AirLockBlock.AirLockBlockItem(TBlocks.air_lock.get(), Prop.Items.SIXTY_FOUR.get().group(TItemGroups.FUTURE))));
    public static final RegistryObject<Item> SPINNY_THING = ITEMS.register("spinny_thing", () -> createItem(new MultiblockBlockItem(TBlocks.spinny_block.get(), MultiblockPatterns.SPINNY_BOI, Prop.Items.SIXTY_FOUR.get())));
    public static final RegistryObject<Item> WAYPOINT_BANK = ITEMS.register("waypoint_bank", () -> createItem(new WaypointBankItem(TBlocks.waypoint_bank.get(), MultiblockPatterns.COMPUTER, Prop.Items.SIXTY_FOUR.get().group(TItemGroups.MAINTENANCE))));
    
    //Dalek Spawn Eggs
    public static final RegistryObject<Item> SPAWN_EGG_DALEK_DEFAULT = ITEMS.register("spawn_egg/dalek_default", () -> createItem(new DalekSpawnItem(() -> DalekTypeRegistry.DEFAULT.get())));
    public static final RegistryObject<Item> SPAWN_EGG_DALEK_SPECIAL_WEAPONS = ITEMS.register("spawn_egg/dalek_special_weapons", () -> createItem(new DalekSpawnItem(() -> DalekTypeRegistry.SPECIAL.get())));
    public static final RegistryObject<Item> SPAWN_EGG_DALEK_RUSTY = ITEMS.register("spawn_egg/dalek_rusty", () -> createItem(new DalekSpawnItem(() -> DalekTypeRegistry.RUSTY.get())));

    private static <T extends Item> T createItem(T item) {
        return item;
    }
                
}
