package net.tardis.mod.items.misc;

import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.tileentities.ConsoleTile;
/** Generic item class that allows any item that subclasses this to be able to receive nbt tags containing Tardis Names and Tardis World Keys 
 * <br> If you do not want behaviours of either {@linkplain IAttunable} or {@linkplain IConsoleBound}, such as turning the input item into a different item, use the {@linkplain AttunableRecipeGen} to make a recipe json that does this*/
public class ConsoleBoundItem extends AttunableItem implements IConsoleBound{
	
	public ConsoleBoundItem(Properties properties) {
		super(properties);
	}
	
	/** In this implementation, add NBT tags to the item stack that sets the correct Tardis World Key and Tardis Name.
	 * <br> This may seem redundant, but compared to the implementation in {@link AttunableItem} we are using our inherited methods {@linkplain IConsoleBound#setTardis(ItemStack, ResourceLocation)} and {@linkplain IConsoleBound#setTardisName(ItemStack, String)} 
     * <br> This allows for less code to be used, and can be used to allow tooltips to have the correct values*/
	@Override
	public ItemStack onAttuned(ItemStack stack, ConsoleTile tile) {
		this.setTardis(stack, tile.getWorld().getDimensionKey().getLocation());
		tile.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> this.setTardisName(stack, data.getTARDISName()));
		return stack;
	}

	@Override
	public ResourceLocation getTardis(ItemStack stack) {
		if(stack.getTag() != null && stack.getTag().contains(TardisConstants.CONSOLE_ATTUNEMENT_NBT_KEY)) {
			return new ResourceLocation(stack.getTag().getString(TardisConstants.CONSOLE_ATTUNEMENT_NBT_KEY));
		}
		else {
			return null;
		} 
	}

	@Override
	public void setTardis(ItemStack stack, ResourceLocation world) {
		stack.getOrCreateTag().putString(TardisConstants.CONSOLE_ATTUNEMENT_NBT_KEY, world.toString());
	}

	@Override
	public String getTardisName(ItemStack stack) {
		if(stack.getTag() != null && stack.getTag().contains(TardisConstants.TARDIS_NAME_ATTUNMENT_NBT_KEY)) {
			return stack.getTag().getString(TardisConstants.TARDIS_NAME_ATTUNMENT_NBT_KEY);
		}
		else {
			return null;
		}
	}

	@Override
	public void setTardisName(ItemStack stack, String name) {
		stack.getOrCreateTag().putString(TardisConstants.TARDIS_NAME_ATTUNMENT_NBT_KEY, name);
	}
	
}
