package net.tardis.mod.items.misc;

import java.util.List;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.items.BaseItem;
import net.tardis.mod.misc.IItemTooltipProvider;
/** Template item class to allow for tooltips to be handled more consistently
 * <br> The tooltip logic is opt-in by default
 * <p> To enable this logic, call shouldShowTooltips and set to true
 * <br> If we want to allow for statistical tooltips to be shown, call setHasStatisticsTooltips and set to true
 * <br> If we want to allow for description tooltips to be shown, call setHasDescriptionTooltips and set to true*/
public class TooltipProviderItem extends BaseItem implements IItemTooltipProvider{
    
    protected boolean shouldShowTooltips = false;
    protected boolean hasStatisticsTooltips = false;
    protected boolean hasDescriptionTooltips = false;

    public TooltipProviderItem(Properties properties, boolean showTooltips) {
        super(properties);
        this.shouldShowTooltips = showTooltips;
    }
    
    public TooltipProviderItem(Properties properties) {
        this(properties, true);
    }
    
    public TooltipProviderItem() {
        super(new Properties().group(TItemGroups.MAIN));
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        super.addInformation(stack, worldIn, tooltip, flagIn);
        if (this.shouldShowTooltips()) {
            this.createDefaultTooltips(stack, worldIn, tooltip, flagIn);
            
            if (this.hasStatisticsTooltips()) {
                tooltip.add(TardisConstants.Translations.TOOLTIP_HOLD_SHIFT);
                if (Screen.hasShiftDown()) {
                    tooltip.clear();
                    tooltip.add(0, this.getDisplayName(stack));
                    createStatisticTooltips(stack, worldIn, tooltip, flagIn);
                }   
            }
            if (this.hasDescriptionTooltips()) {
                tooltip.add(TardisConstants.Translations.TOOLTIP_CONTROL);
                if (Screen.hasControlDown()) {
                    tooltip.clear();
                    tooltip.add(0, this.getDisplayName(stack));
                    createDescriptionTooltips(stack, worldIn, tooltip, flagIn);
                }
            }
        }
        
    }

    @Override
    public void createStatisticTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        
    }

    @Override
    public void createDescriptionTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        
    }

    @Override
    public void createDefaultTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        
    }
    
    @Override
    public boolean shouldShowTooltips() {
        return this.shouldShowTooltips;
    }

    @Override
    public void setShowTooltips(boolean showTooltips) {
        this.shouldShowTooltips = showTooltips;
    }

    @Override
    public boolean hasStatisticsTooltips() {
        return this.hasStatisticsTooltips;
    }

    @Override
    public void setHasStatisticsTooltips(boolean hasStatisticsTooltips) {
        this.hasStatisticsTooltips = hasStatisticsTooltips;
    }

    @Override
    public boolean hasDescriptionTooltips() {
        return this.hasDescriptionTooltips;
    }

    @Override
    public void setHasDescriptionTooltips(boolean hasDescriptionTooltips) {
        this.hasDescriptionTooltips = hasDescriptionTooltips;
    }

}
