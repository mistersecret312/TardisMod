package net.tardis.mod.flight;

import java.util.ArrayList;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.tileentities.ConsoleTile;

public class TimeWindFlightEvent extends FlightEvent{

	public TimeWindFlightEvent(FlightEventFactory entry, ArrayList<ResourceLocation> loc) {
		super(entry, loc);
	}

	@Override
	public void onMiss(ConsoleTile tile) {
		super.onMiss(tile);
		tile.setDestinationReachedTick(tile.getReachDestinationTick() + 200);
	}
}
